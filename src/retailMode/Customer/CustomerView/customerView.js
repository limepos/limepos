import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Typography,
  Upload,
  message,
  Switch,
  Tabs,
  Timeline,
  Modal,
  Table,
  Radio, Checkbox,
} from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import dummy from "../../../images/products/retail/flour-wheat.png";
import { SketchPicker } from 'react-color';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import TuneIcon from '@material-ui/icons/Tune';
import DeleteIcon from '@material-ui/icons/Delete';
import AccountActivity from "../accoutnActivity/accountActivity"

const { TabPane } = Tabs;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
const { confirm } = Modal;
const { Search } = Input;
const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    orderNumber: '#' + i,
    amount: '₹50.00',
    date: `Feb 14, 2020, 5:00 PM`,
    customer: 'Customer Name',
    items: `5`,
    status: `Pending`,
  });
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{`Person `+i}</Option>);
}


const options = [
  { label: 'Receivables', value: 'receivable' },
  { label: 'On Account ', value: 'onaccount' }
];

class EditProduct extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
    displayColorPicker: false,
    background: '#66fecb',
    current_tab: "1",
    visible: false,
    confirmLoading: false,
    columns: [
      {
        title: 'Order No',
        dataIndex: 'orderNumber',
        key: 'orderNumber',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.orderNumber} </a>
          </span>
        )
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.amount} </a>
          </span>
        )
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.date} </a>
          </span>
        )
      },
      {
        title: 'Customer',
        dataIndex: 'customer',
        key: 'customer',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.customer} </a>
          </span>
        )
      },
      {
        title: 'Items',
        dataIndex: 'items',
        key: 'items',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.items} </a>
          </span>
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.status} </a>
          </span>
        )
      }

    ],
    bookdata: data,
    displaybookdata: data,
  };



  constructor(props) {
    super(props);
  }

  viewOrder =(id) => {
    console.log('+++++')
    this.props.history.push(`/app/retail/order/detail/${id}`);
  }

  goback = () => {
    this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  handleChange = (value) => {
    console.log(`selected ${value}`);
  }
  tabcallback = (key) => {
    this.setState({
      current_tab: key,

    });
  }


  numberFormat = (value) =>
    new Intl.NumberFormat('en-us', {
      style: 'currency',
      currency: 'USD'
    }).format(value);

  renderItemlistReceipt() {
    return this.state.order.map((items, index) => {
      const { id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value } = items;
      return (
        <tr key={id} className={this.state.clickitem === id ? 'blink' : ''}>
          <td className="pname"> <span>{name}</span> </td>
          <td>  {qty} X {price}</td>

          <td> <span className="textprice">  {this.numberFormat(discount)} </span> </td>
          <td> <span className="textprice">  {this.numberFormat(updatePrice)} </span> </td>
        </tr>
      )
    })

  }

  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };

  render() {
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;


    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>

        <Row gutter={[25, 25]}>
          <Col span={24}>
            <Title level={4}>  <span onClick={this.goback}> Customer </span> /  Customer Name </Title>
          </Col>
        </Row>

        <Row gutter={[50, 50]}>
          <Col span={24} className="pepar">

       
              <Row gutter={[20, 20]}>
                <Col span={24}>
                   <Col span={24}>
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.tabcallback}>
                      <TabPane tab="Info " key="1">
                        <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">

                          <Form.Item label="Name" {...formItemLayout}>
                            {getFieldDecorator('name', {
                              rules: [
                                {
                                  required: true,
                                  message: 'Please enter name',
                                },
                              ],
                            })(<Input placeholder=" Name" />)}
                          </Form.Item>
                          <Form.Item label="Mobile" {...formItemLayout}>
                            {getFieldDecorator('mobile', {
                              rules: [
                                {
                                  required: true,
                                  message: 'Please enter mobile',
                                },
                              ],
                            })(<Input type="text" placeholder="Mobile" />)}
                          </Form.Item>
                    
                           <Form.Item label="Address " {...formItemLayout}>
                            <Input type="text" placeholder="Address" />
                          </Form.Item>

                          <Form.Item label="Email " {...formItemLayout}>
                            <Input type="text" placeholder="Email" />
                          </Form.Item>

                          <Form.Item label="Work Phone " {...formItemLayout}>
                            <Input type="text" placeholder="Phone" />
                          </Form.Item>

                       

                          <Form.Item label="Allow customer to pay later " {...formItemLayout}>
                            <Switch defaultChecked onChange={this.manageStock} />
                          </Form.Item>

                          <Form.Item >
                            <Button type="default" onClick={this.goback}>
                              Go Back
                                </Button>
                            <Button type="primary" htmlType="submit">
                              Save
                              </Button>


                          </Form.Item>
                          </Form>    
                        </TabPane>
                        <TabPane tab="Sales " key="2">
                             <div class="searchBoxw">
                             
                               <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />
                             </div>
                              <div class="searchBoxw">
                               <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />
                               </div>
                        </TabPane>
                          <TabPane tab="Account" key="3">
                             
                             <AccountActivity/>

                        </TabPane>

                      </Tabs>
                    </Col>
                  </Col>
                  <Col>
                  </Col>

                </Row>
            
          </Col>

        </Row>

        <Modal
          title="Filter Orders"
          visible={this.state.visible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
          okText	="Apply Filter"
         >
      <Radio.Group defaultValue="a" buttonStyle="solid" size="large" className="fiterButtons">
        <Radio.Button value="a">Last 30 days </Radio.Button>
        <Radio.Button value="b">Today</Radio.Button>
        <Radio.Button value="c">Yesterday</Radio.Button>
        <Radio.Button value="d">This Week </Radio.Button>
        <Radio.Button value="e">Last Week </Radio.Button>
        <Radio.Button value="f">This Month </Radio.Button>
        <Radio.Button value="g">Last Month </Radio.Button>
        <Radio.Button value="h">This Year </Radio.Button>
        <Radio.Button value="i">Last Year </Radio.Button>
      </Radio.Group>
       <br />
       <br />
         <h3> Type </h3>
      <Checkbox.Group
      options={options}
      defaultValue={['Apple']}
      onChange={this.onChangepaymentMethod}
      className="paymentmethod"
     />
       <br />
       <br />
       <h3> Sales Person </h3>
       <Select
          mode="tags"
          placeholder="Please select"
          defaultValue={['Person 1', 'Person 2']}
          onChange={this.handleChange}
          style={{ width: '100%' }}
        >
          {children}
        </Select>
          <br />
        </Modal>
      </div>
    );
  }
}
export default Form.create()(EditProduct);