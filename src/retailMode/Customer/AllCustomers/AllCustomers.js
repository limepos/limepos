import React from 'react';
import { Table, Input, DatePicker, Modal, Icon, Radio, Checkbox, Select  } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import TuneIcon from '@material-ui/icons/Tune';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { Search } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    customer: "Customer Name",
    contact : "9856231470",
    sales: "5",
    balance :"$50.00",
    action : ''
  });
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{`Person `+i}</Option>);
}

class AllExpenses extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
   
   columns: [
      {
        title: 'Name',
        dataIndex: 'customer',
        key: 'customer',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.customer} </a>
          </span>
        )
      },
      {
        title: 'Contact Number',
        dataIndex: 'contact',
        key: 'contact',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.contact} </a>
          </span>
        )
      },
      {
        title: 'Sales',
        dataIndex: 'sales',
        key: 'sales',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.sales} </a>
          </span>
        )
      },
      {
        title: 'Balance',
        dataIndex: 'balance',
        key: 'balance',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.balance} </a>
          </span>
        )
      },
     {
       title: 'Action',
          dataIndex: 'action',
          key: 'action',
          render: (text, record) => (
            <span>
                <a onClick={() => this.viewOrder(record.key)}> <BorderColorIcon /> </a> &nbsp; &nbsp; 
                <a onClick={() => this.viewOrder(record.key)}> <VisibilityIcon /> </a> &nbsp; &nbsp; 
                <a onClick={() => this.showDeleteConfirm(record.key)}> <DeleteIcon /> </a>
              </span>
          ),
      }
    ],
    bookdata: data,
    displaybookdata: data,
    sortName: '',
    sortValue: ''
  };


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  changeDate = (date, dateString) => {
    console.log(date, dateString);
  }

  viewOrder = (id) => {
    console.log(id);
    this.props.history.history.push(`/app/retail/customer/detail/${id}`);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };
  
  onChangepaymentMethod(checkedValues) {
    console.log('checked = ', checkedValues);
  }

  handleChange = (value) =>{
    console.log(`selected ${value}`);
  }
  showDeleteConfirm = () => {
    confirm({
      title: 'Are you sure delete this Product.',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  render() {
    return (
      <div>
        <div style={{ marginBottom: 16,overflow: `auto` }}>
          <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />
        </div>
        <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />
      </div>

    )
  }
}
export default (AllExpenses);