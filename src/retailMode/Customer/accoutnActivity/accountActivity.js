import React from 'react';
import { Table, Input, DatePicker, Modal, Icon, Radio, Checkbox, Select, Button,Row ,Col  } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import TuneIcon from '@material-ui/icons/Tune';
import { ArrowDownOutlined } from '@ant-design/icons';
const { Search } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    orderNumber: '#' + i,
    amount: '$50.00',
    transaction : 'Update Balance',
    date: `Feb 14, 2020, 5:00 PM`
  });
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{`Person `+i}</Option>);
}

class AccountActivity extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle: "Action",
    ModalText: 'Content of the modal',
    visible: false,
    creditpopup : false,
    confirmLoading: false,
    credittype : 0,
    columns: [
      {
        title:  'No',
        dataIndex: 'orderNumber',
        key: 'orderNumber',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.orderNumber} </a>
          </span>
        )
      },
      {
        title: 'Transaction',
        dataIndex: 'transaction',
        key: 'transaction',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.amount} </a>
          </span>
        )
      },
      {
        title: 'Account Balance',
        dataIndex: 'amount',
        key: 'amount',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.amount}  <ArrowDownOutlined /></a>
          </span>
        )
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.date} </a>
          </span>
        )
      }
    ],
    bookdata: data,
    displaybookdata: data,
    sortName: '',
    sortValue: ''
  };


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  changeDate = (date, dateString) => {
    console.log(date, dateString);
  }

  viewOrder = (id) => {
    this.props.history.history.push(`/app/retail/order/detail/${id}`);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };
  
  onChangepaymentMethod(checkedValues) {
    console.log('checked = ', checkedValues);
  }

  handleChange = (value) =>{
    console.log(`selected ${value}`);
  }

  
  showModalCredit = (t) => {
    this.setState({
      creditpopup: true,
      credittype : t
    });
  };

  handleOkCredit = e => {
    console.log(e);
    this.setState({
      creditpopup: false,
    });
  };

  handleCancelCredit = e => {
    console.log(e);
    this.setState({
      creditpopup: false,
    });
  };


   
  render() {

    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
 
    const options = [
      { label: 'Cash', value: 'cash' },
      { label: 'Credit Card', value: 'creditcard' },
      { label: 'Debit Card', value: 'debitcard' },
      { label: 'Check', value: 'check' },
      { label: 'Other', value: 'other' },
      { label: 'Store Credit', value: 'storecredit' },
      { label: 'Pay Later', value: 'paylater' },
      { label: 'Coupon', value: 'Orange' }
    ];

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
          <div class="searchBoxw_item">
          <p className="center"> Current Account Balance </p>
                            <h1 className="center"> $ 4500.00 </h1>
                                 <div class="center">
                                        <Button type="primary" onClick={() => this.showModalCredit(1)}> Add Credit </Button>
                                        &nbsp; &nbsp; 
                                        <Button type="primary" onClick={() =>this.showModalCredit(0)}> Remove Credit </Button>
                                 </div>
          
                                 
                                 </div>
          <TuneIcon className="searchBox filterextra" onClick={this.showModal} />
          <RangePicker onChange={this.changeDate} className="searchBox " />
          <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />
            
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
          </Col>
        </Row>
        <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />

        <Modal
          title="Filter Orders"
          visible={this.state.visible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
          okText	="Apply Filter"
         >
      <Radio.Group defaultValue="a" buttonStyle="solid" size="large" className="fiterButtons">
        <Radio.Button value="a">Last 30 days </Radio.Button>
        <Radio.Button value="b">Today</Radio.Button>
        <Radio.Button value="c">Yesterday</Radio.Button>
        <Radio.Button value="d">This Week </Radio.Button>
        <Radio.Button value="e">Last Week </Radio.Button>
        <Radio.Button value="f">This Month </Radio.Button>
        <Radio.Button value="g">Last Month </Radio.Button>
        <Radio.Button value="h">This Year </Radio.Button>
        <Radio.Button value="i">Last Year </Radio.Button>
      </Radio.Group>
       <br />
       <br />
         <h3> Type </h3>
      <Checkbox.Group
      options={options}
      defaultValue={['Apple']}
      onChange={this.onChangepaymentMethod}
      className="paymentmethod"
     />
       <br />
       <br />
       <h3> Sales Person </h3>
       <Select
          mode="tags"
          placeholder="Please select"
          defaultValue={['Person 1', 'Person 2']}
          onChange={this.handleChange}
          style={{ width: '100%' }}
        >
          {children}
        </Select>
          <br />
        </Modal>
        <Modal
          title={this.state.credittype == 1 ? "Add Credit" : "Remove Credit" }
          visible={this.state.creditpopup}
          onOk={this.handleOkCredit}
          onCancel={this.handleCancelCredit}
        >
                 <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Amount</label>
                      <Input type="number" placeholder="Credit Amount" />
                    </div>
                  
        </Modal>
      </div>

    )
  }
}
export default (AccountActivity);