import React from 'react';
import ReactDOM from 'react-dom';
import { Menu, Dropdown, DatePicker, Typography, Button, Tabs, Row, Col, Input, Icon, Modal, Radio, Checkbox, TimePicker, List, Avatar, Select,Form,Switch } from 'antd';
import Grid from '@material-ui/core/Grid';
import data from './data.json';
import product from './product.json';
import Currency from "../../components/currency";
import tableSearch from '../../components/TableSearch/tableSearch';
import AddIcon from '@material-ui/icons/Add';
import Bottle from '../../images/products/retail/Water-bottles.jpg';
import SoyaOil from '../../images/products/retail/fortune-soya-oil.jpg';
import Handwash from '../../images/products/retail/handwash.webp';
import Pasta from '../../images/products/retail/pasta.jpg';
import flourWheat from '../../images/products/retail/flour-wheat.png';
import Chocolate from '../../images/products/retail/chocolate.jpg';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import CardMembershipIcon from '@material-ui/icons/CardMembership';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import CalendarViewDayIcon from '@material-ui/icons/CalendarViewDay';
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import MoneyIcon from '@material-ui/icons/Money';
import SubtitlesIcon from '@material-ui/icons/Subtitles';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import ReceiptIcon from '@material-ui/icons/Receipt';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { UserOutlined } from '@ant-design/icons';
import logo from "../../images/logo.png";
import CancelIcon from '@material-ui/icons/Cancel';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import SendIcon from '@material-ui/icons/Send';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';

import {
  toggleSidebarClose
} from "../../context/LayoutContext";
import { Item } from 'react-sidemenu';
const { Search } = Input;
const { Title, Text } = Typography;
const { TabPane } = Tabs;
const { confirm } = Modal;
const { Option } = Select;

const datacustomer = [
  {
    title: 'Customer Name',
  },
  {
    title: 'Customer Name',
  },
  {
    title: 'Customer Name',
  },
  {
    title: 'Customer Name',
  },
];
var windowWidth = window.innerWidth;

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

class Checkout extends React.Component {

  constructor(props) {
    super(props);
    this.newTabIndex = 0;
    const panes = [
      { title: 'All', content: 'Content of Tab Pane 1', key: '0' },
      { title: 'Grocery', content: 'Content of Tab Pane 2', key: '10' },
    ];

    console.log("product", product);


    this.state = {
      activeKey: panes[0].key,
      panes,
      currentTypeProduct: '0',
      count: product.length,
      current_tab: '1',
      activeorder: 0,
      activetablename: 'RetailOrder',
      currenttype: 'all',
      inProgress: [],
      sortName: '',
      sortValue: '',
      order: [],
      visible: false,
      columns: [
        {
          title: 'Name',
          dataIndex: 'name',
        },
        {
          title: 'status',
          dataIndex: 'status',
        },
        {
          title: 'type',
          dataIndex: 'type',
        }
      ],
      productcolumns: [
        {
          title: 'Name',
          dataIndex: 'name',
        },
        {
          title: 'type',
          dataIndex: 'type',
        },
        {
          title: 'price',
          dataIndex: 'price',
        },
        {
          title: 'category',
          dataIndex: 'category',
        }
      ],
      bookdata: data,
      displaybookdata: data,
      productList: product,
      Product: product,
      qty: 0,
      total: 0,  // total of  minus discount.
      discountTotal: 0,    // total of discount.
      subtotal: 0,     //  total of all product price. 
      bulkdiscount: 0,   //  bulk discount calculation total.
      coupnediscount: 0,   // coupne discunt calculation total.
      BulkdiscountType: '',
      bulkdiscountValue: 0,  // bulk discount vlaue in cash or parcentage.
      viewItem: [{
        id: 0,
        name: '',
        price: 0,
        type: 'type',
        updatePrice: 0,
        qty: 1,
        discount: 0,
        note: '',
        discountby: '',
        unit: '0',
        unit_value: ''
      }],
      discountType: '',
      showbulkdiscount: false,
      showCoupme: false,
      error: false,
      orderticketshow: false,
      startValue: null,
      endValue: null,
      endOpen: false,
      ordertyepe: 'sale',
      chargeshow: false,
      customerForm: false,
      swaptableshow: false,
      swapTableName: '',
      clickitem: 0,
      move: false,
      movefix: false,
      viewbill: false,
      unitQty: false,
      qtymaneger: 0.000,
      qtymaneger1: 0.000,
      unitType: "",
      itemName: '',
      complete: false,
      product: true,
      remove: false,
      saveOrder: false,
      receipt: false,
      paymentType: '',
      totalQ: 0,
      customermodle: false,
      customermodlenew: false,
      currentCustomer : "",




    }
    this.renderTableProdudctthum();


    toggleSidebarClose()
  }

  componentDidMount() {

    this.setState({
      order: JSON.parse(localStorage.getItem(this.state.activetablename))
    }, () => this.subtotalCalc());

    // this.props.history
    if (localStorage.getItem("inProgress")) {
      this.setState({ inProgress: JSON.parse(localStorage.getItem("inProgress")) })
    } else {
      console.log("noe")
      this.setState({ inProgress: [] })
    }
    this.subtotalCalc();
    //   localStorage.setItem('inProgress', )

    if (windowWidth > 991) {

      this.setState({
        viewbill: true,
      })

    }

  }

  numberFormat = (value) =>
    new Intl.NumberFormat('en-us', {
      style: 'currency',
      currency: 'USD'
    }).format(value);

  finderProduct = (e) => {

    let searchtext = e.target.value
    let columns = this.state.productcolumns
    let bookdata = this.state.productList

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      Product: r
    })
  }


  addnewRegister = (tab) => {


    if (tab === 1) {
      this.props.history.push("/app/setup/product-categorie/new-category");
    }
    else if (tab === 2) {
      this.props.history.push("/app/setup/new-ticket-group");
    }
  }


  currentTypeProduct = (type) => {
    console.log("type", type);
    this.setState({
      currentTypeProduct: type
    })
  }


  showModalCustomer = () => {
    this.setState({
      customermodle: true,
    });
  };

  handleOkCustomer = e => {
    console.log(e);
    this.setState({
      customermodle: false,
    });
  };

  handleCancelCustomer = e => {
    console.log(e);
    this.setState({
      customermodle: false,
    });
  };



  showModalCustomernew = () => {
    this.setState({
      customermodle: false,
      customermodlenew: true
      
    });
  };

  handleOkCustomernew = e => {
    console.log(e);
    this.setState({
      customermodlenew: false,
    });
  };

  handleCancelCustomernew = e => {
    console.log(e);
    this.setState({
      customermodlenew: false,
    });
  };

  /* -***** Tab 2 *****  -- */

  addProduct = (id, name, price, type, unit, unit_value, e) => {

    console.log("unit", unit);

    let updatePrice = price
    let discount = 0
    let note = ""
    let discountby = ''
    let discoutnumber = 0
    let qty = 1


    var n = ReactDOM.findDOMNode(this);
    console.log(n);

    let getproductData = {
      "id": id,
      "name": name,
      "price": price,
      "type": type,
      "updatePrice": price,
      "qty": 1,
      "discount": 0,
      "note": '',
      "discountby": '',
      "discoutnumber": 0,
      "unit": unit,
      "unit_value": unit_value,
    };

    console.log(getproductData);

    if (this.state.order.some((item) => item.id === getproductData.id)) {
      var data = [...this.state.order];
      var index = data.findIndex(obj => obj.id === getproductData.id);
      data[index].qty = data[index].qty + 1;
      data[index].updatePrice = data[index].price * (data[index].qty);
      this.setState({ order: data, clickitem: id },
        () =>
          console.log("qty update", this.state.order));
      this.subtotalCalc();
      localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
      this.billcalcaulatore()

    } else {

      console.log(getproductData)
      console.log("product", getproductData)
      let prod = this.state.order.concat(getproductData);

      console.log("new Prouduct", prod);

      this.setState({
        order: prod,
        clickitem: id
      }, () => {
        console.log("add Product", this.state.order);
        this.subtotalCalc();
        localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
        this.billcalcaulatore()
      }
      )
    }

    setTimeout(
      function () {
        this.setState({
          move: true
        })
      }
        .bind(this),
      100
    );
    setTimeout(
      function () {
        this.setState({
          clickitem: 0,
          move: false,
        })
      }
        .bind(this),
      500
    );

    if (unit === "1") {

      this.showModal(id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value)
      this.setState({
        remove: true
        //  unitQty :true,
        //   unitType : unit_value,
        // itemName : name
      })
    }

  }

  renderTableProdudct() {

    console.log("this.state.Product", this.state.Product)

    return this.state.Product.map((table, index) => {
      const { id, name, price, type, color, image, unit, unit_value } = table;
      console.log("index", unit_value);
      return (
        <>
          {this.state.currentTypeProduct === type || this.state.currentTypeProduct === '0' || name === 'placeholder' ? <li onClick={() => { name == "placeholder" ? this.addNewProduct() : this.addProduct(id, name, price, type, unit, unit_value, this) }}>
            {name !== 'placeholder' ? <div className="table prod" style={image === '' ? { background: color } : { background: `url(${name === 'Bottle' ? Bottle : name === 'Soya Oil' ? SoyaOil : name === 'Handwash' ? Handwash : name === 'Pasta' ? Pasta : name === 'Flour Wheat' ? flourWheat : name === 'Chocolate' ? Chocolate : ''})` }}>
              {image === '' ? <Text className="pdetailsName"> {name} </Text> : ''}
            </div> : type == "plus0" ? <AddIcon className="plushicon" /> : ''}
            {name !== 'placeholder' ? <div className="pdetails" style={{ background: color }}>
              <div className="tablename">
                {name}
              </div>
              <div className="price">
                <span></span> {this.numberFormat(price)}
              </div>
            </div> : ""}
            <>{this.state.clickitem === id ? <img className={`b-flying-img ${this.state.move ? 'move' : this.state.movefix ? 'movefix' : ''} `} src={name === 'Bottle' ? Bottle : name === 'Soya Oil' ? SoyaOil : name === 'Handwash' ? Handwash : name === 'Pasta' ? Pasta : name === 'Flour Wheat' ? flourWheat : name === 'Chocolate' ? Chocolate : ''} /> : ''}</>
          </li> : ''}
        </>
      )
    })
  }

  quntyPlus = (id) => {
    var data = this.state.order;
    var index = data.findIndex(obj => obj.id === id);
    data[index].qty = data[index].qty + 1;
    data[index].updatePrice = data[index].price * data[index].qty;
    this.setState({ order: data },
      () =>
        console.log(" + qty update", this.state.order));
    this.subtotalCalc();
    localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

  }

  quntyMinus = (id) => {

    var data = this.state.order;

    var index = data.findIndex(obj => obj.id === id);
    if (data[index].qty > 0) {
      data[index].qty = data[index].qty - 1;
      data[index].updatePrice = data[index].price * data[index].qty;
    }
    this.setState({ order: data },
      () =>
        console.log("- qty update", this.state.order));
    this.subtotalCalc();
    localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

  }

  /* Edit quntity */

  editQuntyPlus = () => {

    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber,
      unit: this.state.viewItem[0].unit,
      unit_value: this.state.viewItem[0].unit_value

    }]


    items[0].qty = this.state.viewItem[0].qty + 1

    items[0].updatePrice = items[0].price * (this.state.viewItem[0].qty + 1);

    this.setState({
      viewItem: items
    })

  }

  editQuntyMinus = () => {


    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber,
      unit: this.state.viewItem[0].unit,
      unit_value: this.state.viewItem[0].unit_value

    }]

    if (items[0].qty > 0) {
      items[0].qty = this.state.viewItem[0].qty - 1
      items[0].updatePrice = items[0].price * (this.state.viewItem[0].qty - 1);
      this.setState({
        viewItem: items
      })
    }

  }


  subtotalCalc = () => {

    let subtotalSum = 0;
    let discountSum = 0;
    let qq = 0;

    this.state.order.forEach(element => {
      discountSum += parseInt(element.discount);
      subtotalSum += parseInt(element.updatePrice);
      qq += parseInt(element.qty);
    })

    console.log("Total ", parseInt(subtotalSum));


    let bulkdis = this.state.bulkdiscount;



    this.setState({
      total: subtotalSum - discountSum - bulkdis,
      discountTotal: discountSum,
      subtotal: subtotalSum,
      totalQ: qq
    }, () => this.billcalcaulatore())

  }

  showModal = (id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value) => {

    // console.log("data == > ", id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber)

    let items = [{ id: id, name: name, price: price, type: type, qty: qty, updatePrice: updatePrice, discount: discount, note: note, discountby: discountby, discoutnumber: discoutnumber, unit: unit, unit_value: unit_value }]

    this.setState({
      visible: true,
      viewItem: items
    }, () => console.log("viewItem", this.state.viewItem));
  };

  handleOk = e => {
    console.log(e);
    if (this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice) { } else {
      this.setState({
        visible: false,
        remove: false,
      });
    }


    if (this.state.order.some((item) => item.id === this.state.viewItem[0].id)) {
      var data = [...this.state.order];
      //id, name, price, type, qty, updatePrice, discount, note, discountby

      var index = data.findIndex(obj => obj.id === this.state.viewItem[0].id);
      data[index].qty = this.state.viewItem[0].qty;
      data[index].updatePrice = this.state.viewItem[0].updatePrice;
      data[index].discount = this.state.viewItem[0].discount;
      data[index].note = this.state.viewItem[0].note;
      data[index].discountby = this.state.viewItem[0].discountby;
      data[index].discoutnumber = this.state.viewItem[0].discoutnumber;

      this.setState({ order: data },
        () =>
          console.log("qty update", this.state.order));
      this.subtotalCalc();
      localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

    }

  };

  handleCancel = e => {

    if (this.state.remove) {
      this.removeItem(e);
    }

    this.setState({
      visible: false,
      customerForm: false,
      swaptableshow: false,
      remove: false,
    });
  };

  changeNot = (event) => {

    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber,
      unit: this.state.viewItem[0].unit,
      unit_value: this.state.viewItem[0].unit_value

    }]

    if (event.target.name === "discount") {

      let price = items[0].updatePrice;


      items[0].discountby = this.state.discountType;
      items[0].discoutnumber = event.target.value;

      if (this.state.discountType === 'percentage') {

        let parsent = (parseInt(price) * parseInt(event.target.value)) / 100;
        console.log("parcentage", parsent);
        items[0].discount = parsent;
        this.setState({
          viewItem: items
        })

      } else {

        items[0].discount = parseInt(event.target.value);
        this.setState({
          viewItem: items
        })

      }

    }
    if (event.target.name === "note") {
      items[0].note = event.target.value;
      this.setState({
        viewItem: items
      })
    }
    if (event.target.name === "qty") {


      items[0].qty = parseInt(event.target.value);

      console.log(items[0].price, event.target.value);

      items[0].updatePrice = parseInt(items[0].price) * parseInt(event.target.value);

      console.log("price update", items)

      this.setState({
        viewItem: items
      })
    }
  }

  checkDiscount = (e) => {
    console.log("discount", e.target.value)
    this.setState({
      discountType: e.target.value,
    })
  }



  renderItemlist() {
    return this.state.order.map((items, index) => {
      const { id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value } = items;
      return (
        <tr key={id} className={this.state.clickitem === id ? 'blink' : ''}>
          <td className="pname"> <div className="pimage" style={{ backgroundImage: `url(${name === 'Bottle' ? Bottle : name === 'Soya Oil' ? SoyaOil : name === 'Handwash' ? Handwash : name === 'Pasta' ? Pasta : name === 'Flour Wheat' ? flourWheat : name === 'Chocolate' ? Chocolate : ''})` }}></div> <span>{name}</span> </td>
          <td> <span onClick={() => this.quntyMinus(id)} className="circle"> <Icon type="minus" /> </span>  <span className="qty"> {qty} </span> <span className="circle" onClick={() => this.quntyPlus(id)}> <Icon type="plus" /> </span> </td>
          <td> <span className="textprice">  {this.numberFormat(updatePrice)} </span> </td>
          <td> <span className="textprice">  {this.numberFormat(discount)} </span> </td>
          <td> <Icon type="edit" onClick={() => this.showModal(id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value)} /> </td>
        </tr>
      )
    })

  }
  renderItemlistReceipt() {
    console.log("-->> ", this.state.order);
    return this.state.order.map((items, index) => {
      const { id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value } = items;
      return (
        <tr key={id} className={this.state.clickitem === id ? 'blink' : ''}>
          <td className="pname"> <span>{name}</span> </td>
          <td>  {this.numberFormat(price)} </td>
          <td>  {qty} </td>
          <td> <span className="textprice">  {this.numberFormat(discount)} </span> </td>
          <td> <span className="textprice">  {this.numberFormat(updatePrice)} </span> </td>
        </tr>
      )
    })

  }

  bulkdiscountshow = () => {

    this.setState({
      showbulkdiscount: !this.state.showbulkdiscount,
      showCoupme: false

    })

  }

  coupnshow = () => {
    this.setState({
      showCoupme: !this.state.showCoupme,
      showbulkdiscount: false
    })
  }

  bulkdiscounttype = (e) => {

    this.setState({
      BulkdiscountType: e.target.value
    })
  }

  bulkdiscounCount = (e) => {

    this.setState({
      bulkdiscountValue: e.target.value
    })

  }

  bulkdiscountCalculate = () => {

    let bulkdiscountvalue = this.state.bulkdiscountValue;



    let price = this.state.subtotal - this.state.discountTotal;

    console.log("Total Discount ", this.state.discountTotal);

    console.log("value", price)

    if (this.state.BulkdiscountType === 'percentage') {

      let parsent = (parseInt(price) * parseInt(bulkdiscountvalue)) / 100;
      console.log("parcentage", parsent);



      console.log("Parsent", this.state.total)

      if (price > parsent) {

        this.setState({
          bulkdiscount: parsent,
          total: price - parsent,
          showbulkdiscount: false,
          error: false
        }, () => this.billcalcaulatore())

      } else {
        console.log("Parsent is greater")
        this.setState({

          error: true
        }, () => console.log(this.state.error))

      }
    } else {
      let total = parseInt(price) - bulkdiscountvalue;

      console.log("Total ", price, bulkdiscountvalue)

      if (price > bulkdiscountvalue) {

        this.setState({
          bulkdiscount: bulkdiscountvalue,
          total: total,
          showbulkdiscount: false,
          error: false
        }, () => this.billcalcaulatore())
      } else {
        console.log("Parsent is greater")
        this.setState({
          error: true
        }, () => console.log(this.state.error))

      }



    }
  }

  billcalcaulatore = () => {
    let bill = {
      subtotal: this.state.subtotal,
      discountTotal: this.state.discountTotal,
      bulkdiscount: this.state.bulkdiscount,   //  bulk discount calculation total.
      coupnediscount: this.state.coupnediscount,   // coupne discunt calculation total.
      BulkdiscountType: this.state.BulkdiscountType,
      bulkdiscountValue: this.state.bulkdiscountValue,
      total: this.state.total,

    }

    // let prod = this.state.order.concat(bill);

    localStorage.setItem("bill" + this.state.activetablename, JSON.stringify(bill));
  }

  clearall = () => {

    let tabelid = this.state.activeorder;
    console.log(tabelid)

    let talels = this.state.inProgress;
    console.log(talels)

    var filteredAry = talels.filter(e => e !== tabelid)

    console.log(filteredAry)

    let inpro = JSON.stringify(filteredAry);

    localStorage.setItem('inProgress', inpro);

    localStorage.removeItem("bill" + this.state.activetablename);
    localStorage.removeItem(this.state.activetablename);

    this.setState({
      current_tab: '1',
      inProgress: filteredAry,
      activeorder: 0,
      order: []

    })
  }

  showDeleteConfirm = (e) => {
    confirm({
      title: 'Clear Order',
      content: 'Are you sure you want to clear this order?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {

        e.clearall()

      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  removeItem = (id) => {
    console.log("Remove id = ", id)

    if (this.state.order.some((item) => item.id === id)) {
      var data = [...this.state.order];

      var i = data.findIndex(obj => obj.id === id);

      if (i > -1) {
        data.splice(i, 1);
      }

      console.log("remaining items", data);

      this.setState({ order: data, visible: false },
        () => {
          console.log("qty update", this.state.order)
          this.subtotalCalc();
          localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
          this.billcalcaulatore()
        });
    }
  }

  orderticket = () => {
    this.setState({
      orderticketshow: true
    })
  }

  orderItems() {
    return this.state.order.map((items, index) => {
      const { id, name, qty } = items;
      return (
        <li key={id} >
          {qty} X {name}
        </li>
      )
    })

  }

  generateOrderTicketPrint = () => {

  }

  onCancelorderTicket = () => {

    this.setState({
      orderticketshow: false
    })

  }

  disabledStartDate = startValue => {
    const { endValue } = this.state;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledStartDate = startValue => {
    const { endValue } = this.state;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = endValue => {
    const { startValue } = this.state;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  }

  onStartChange = value => {
    this.onChange('startValue', value);
  }

  handleStartOpenChange = open => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }
  checkorderfor = (e) => {
    this.setState({
      ordertyepe: e.target.value
    })
  }



  changebill = () => {

    this.setState({
      chargeshow: true,
      product: false
    })

  }
  closechargeview = () => {
    this.setState({
      chargeshow: false
    })
  }

  addCustomer = () => {
    this.setState({
      customerForm: true
    })
  }
  swaptable = () => {

    this.setState({
      activetablename: this.state.activetablename,
      swaptableshow: false
    })

  }
  showSwapform = () => {
    this.setState({
      swaptableshow: true
    })
  }
  tabelselect = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      activetablename: value
    })
  }
  viewProduct = () => {
    this.setState({
      viewbill: false,
      product: true
    })
  }
  billShow = () => {
    this.setState({
      viewbill: true,
      /* product:false*/
    })
  }

  newSale = () => {
    this.setState({
      complete: false,
      product: true,
      saveOrder: false,
      order: [],
      currentCustomer: ''
    })
  }

  renderTableProdudctthum = () => {

    console.log(this.state.product);

    console.log(this.state.count);


    for (let i = 0; i < 20 - this.state.count; i++) {
      this.state.Product.push({
        "id": 0,
        "type": 'plus' + i,
        "name": "placeholder",
        "price": "null",
        "Taxgroup": "null",
        "category": "null",
        "unit": "1",
        "unit_value": ""

      })
    }
  }

  onChangeTab = activeKey => {

    console.log("activeKey", activeKey);
    this.setState({
      activeKey,
      currentTypeProduct: activeKey
    });
  };

  qtymaneger = (e) => {

    const str = e.target.value;

    //console.log("---****---", str)

    if (str.charAt(str.length - 1) === '.') {
      return
    }

    let q = e.target.value;
    console.log("==<<", q);
    let c = e.target.value;

    if (c == "") {
      c = 0
    }
    // this.setState({
    //   qtymaneger1: q,
    //   qtymaneger: e.target.value
    // })

    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber,
      unit: this.state.viewItem[0].unit,
      unit_value: this.state.viewItem[0].unit_value

    }]

    items[0].qty = parseFloat(c);

    items[0].updatePrice = parseFloat(items[0].price) * parseFloat(c);

    console.log("price update", items)

    this.setState({
      viewItem: items
    })

  }

  setQty = () => {
    this.setState({
      unitQty: false
    })
  }

  onCancel_addtocart = () => {
    this.setState({
      unitQty: false
    })
  }
  addNewProduct = () => {

    alert("Add Proudct Module in Under construction.")

  }

  addNewCustomer = () => {
    alert("Add Customer Module in Under construction.")
  }

  complete = () => {
    this.setState(
      {
        complete: true,
        chargeshow: false,
      }
    )
  }

  fhandleButtonClick = (e) => {
    // message.info('Click on left button.');
    console.log('click left button', e);
  }

  handleMenuClick = (e) => {
    //  message.info('Click on menu item.');
    console.log('click', e);
  }

  menu = () => {
    return (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item key="1">
          Pending
      </Menu.Item>
        <Menu.Item key="2">
          Confirmed
      </Menu.Item>
      </Menu>
    )
  }

  saveOrder = () => {
    this.setState(
      {
        complete: false,
        chargeshow: false,
        saveOrder: true

      })
  }
  backTocart = () => {
    this.setState(
      {
        complete: false,
        chargeshow: false,
        saveOrder: false,
        product: true

      })
  }

  paymentType = (t) => {
    this.setState({
      paymentType: t
    })
  }

  receipt = () => {
    this.setState({
      receipt: true
    })
  }

  closereceipt = () => {
    this.setState({
      receipt: false
    })
  }

  addcustomers = () => {
    this.props.history.push("/app/retail/customer/new");
  }

  addcustomer = () => {
    
     this.setState({
      currentCustomer : "Customer Name",
      customermodle: false
     })
     
    
  }

  render() {

    const { getFieldDecorator } = this.props.form;


    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div hi="s">
        <Grid container spacing={3}>
          <Grid item xs={6} className="pageTitle">
            {this.state.product ? <Title level={4}> Checkout </Title> : this.state.chargeshow ?
              <Title level={4} onClick={this.backTocart}> <KeyboardArrowLeftIcon className="backArrow" />  Payment </Title> : this.state.paymentType !== '' ? <Title level={4}> Checkout / {this.state.paymentType}</Title> : ''
            }
          </Grid>
         
          { this.state.product || this.state.chargeshow ? <Grid item xs={6} className="rightButton">
            {/* <PersonAddIcon onClick={this.addCustomer} /> */}
           { this.state.currentCustomer == "" ? <Button type="primary" className="primarybutton" onClick={this.showModalCustomer} > Add Customer </Button>: <Button  onClick={this.showModalCustomer} level={4}>{ this.state.currentCustomer } </Button>}
          </Grid> : ''}
        </Grid>
        <Row gutter={[30, 30]}>
          <Col span={24}>
            <Col span={24} className="pepar">
              <div>
                {this.state.product ? <>
                  <div className="productSide">
                    <Tabs
                      hideAdd
                      onChange={this.onChangeTab}
                      activeKey={this.state.activeKey}
                    >
                      {this.state.panes.map(pane => (
                        <TabPane tab={pane.title} key={pane.key}>

                        </TabPane>
                      ))}
                    </Tabs>

                    <div className="orderTables">
                      <div style={{ marginBottom: 16, overflow: 'auto' }}>
                        <Search placeholder="Search Product" onChange={e => this.finderProduct(e)} className="" />
                      </div>
                      <ul className="retails product" >
                        {this.renderTableProdudct()}
                      </ul>
                      <button className="retailOrder productbitton" onClick={this.billShow}>  {this.state.order.length} {this.state.order.length > 1 ? 'Items' : 'Item'} = {this.numberFormat(this.state.total)}</button>
                    </div>
                  </div>
                  {this.state.viewbill ?
                    <div className="orderside">
                      <div className="orderItems">
                        <div className="ordemeta">
                          <a className="rightitem" onClick={() => this.showDeleteConfirm(this)}> Clear </a> <a className="rightitem viewProducts" onClick={this.viewProduct}> View Products </a>

                        </div>

                        {this.state.order.length > 0 ?
                          <>
                            <table className="orders">
                              <thead>
                                <tr key="1">
                                  <th> Item </th>
                                  <th> Quantity </th>
                                  <th> Amount</th>
                                  <th> Discount</th>
                                  <th> </th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.renderItemlist()}
                              </tbody>
                            </table>


                            <div className="bulk_discount">

                              {this.state.showCoupme ? <div className="discountbox coupn">

                                <div style={{ marginBottom: 16, marginTop: 16 }}>
                                  <Input type="text" name="discount" value={this.state.viewItem[0].discoutnumber} onChange={this.changeNot} />
                                </div>
                                <button className="smallbutton"> Apply </button>
                                <span className="discountboxarrow"></span>
                              </div> : ''}

                              {this.state.showbulkdiscount ? <div className="discountbox">
                                <Radio.Group name="radiogroup" onChange={this.bulkdiscounttype} defaultValue={this.state.BulkdiscountType}>
                                  <Radio value={`cash`}> Cash</Radio>
                                  <Radio value={`percentage`}> Percentage </Radio>
                                </Radio.Group>
                                <div style={{ marginBottom: 16, marginTop: 16 }}>
                                  <Input type="number" name="discount" value={this.state.bulkdiscountValue} onChange={this.bulkdiscounCount} />
                                </div>
                                <button className="smallbutton" onClick={this.bulkdiscountCalculate}> Done </button>
                                <span className="discountboxarrow"></span>
                              </div> : ''}
                              <span> {}</span>

                              <a onClick={this.bulkdiscountshow}> {this.state.bulkdiscount > 0 && this.state.error === false ? <span> Bulk Discount {this.numberFormat(this.state.bulkdiscount)}</span> : this.state.error ? <small style={{ color: "red" }}> Discount more than total </small> : <span> Bulk Discount </span>}  </a> &nbsp; &nbsp; &nbsp;  <a onClick={this.coupnshow}> Coupon Code  </a>
                            </div>

                            <button className="retailOrder" onClick={this.changebill}>  {this.state.order.length} {this.state.order.length > 1 ? 'Items' : 'Item'} = {this.numberFormat(this.state.total)} </button>
                          </>
                          : <>
                            <p className="viewProductemptytitle">Your Cart is Empty Please add Proudct</p>
                            <Button onClick={this.viewProduct} className="viewProductempty"> Add Products </Button>
                          </>
                        }
                      </div>

                    </div>
                    : ''}
                </> : ''}

                {this.state.chargeshow ? <Col span={24} className="chargeOptions">


                  <Title level={2} className="center mgb-50"> {this.numberFormat(this.state.total)}</Title>

                  <Col span={24} className="center mgb-50">
                    <Button onClick={this.saveOrder}>Save Order </Button> <Button> Pay Later </Button>
                  </Col>

                  <Col xs={24} className="center paymentoptions">
                    <div className={`Paymentoption one ${this.state.paymentType == 'Cash' ? 'active' : ''}`} onClick={() => this.paymentType('Cash')}>
                      <LocalAtmIcon />
                      <br />
                      <Text className="paymenttype"> Cash </Text>
                    </div>
                    <div className={`Paymentoption two ${this.state.paymentType == 'Debit Card' ? 'active' : ''}`} onClick={() => this.paymentType('Debit Card')}>
                      <CreditCardIcon />
                      <br />
                      <Text className="paymenttype"> Debit Card </Text>
                    </div>
                    <div className={`Paymentoption three ${this.state.paymentType == 'Credit Card' ? 'active' : ''}`} onClick={() => this.paymentType('Credit Card')}>
                      <HorizontalSplitIcon />
                      <br />
                      <Text className="paymenttype"> Credit Card </Text>
                    </div>
                    <div className={`Paymentoption four ${this.state.paymentType == 'Store Credit' ? 'active' : ''}`} onClick={() => this.paymentType('Store Credit')}>
                      <AccountBalanceWalletIcon />
                      <br />
                      <Text className="paymenttype"> Store Credit </Text>
                    </div>
                    <div className={`Paymentoption five ${this.state.paymentType == 'Check' ? 'active' : ''}`} onClick={() => this.paymentType('Check')}>
                      <MoneyIcon />
                      <br />
                      <Text className="paymenttype"> Check </Text>
                    </div>
                    <div className={`Paymentoption six ${this.state.paymentType == 'Other' ? 'active' : ''}`} onClick={() => this.paymentType('Other')}>
                      <SubtitlesIcon />
                      <br />
                      <Text className="paymenttype"> Other </Text>
                    </div>
                    <div className="mgb-50" />
                    <button className="retailOrder" onClick={this.complete}> Next </button>
                  </Col>

                </Col> : ''}

                {this.state.complete ? <Col span={24} className="chargeOptions">




                  <Col span={24} className="center completeMasr">
                    <CheckCircleOutlineIcon className="completemark" />
                  </Col>

                  <Title level={2} className="center mgb-50"> {this.numberFormat(this.state.total)}</Title>

                  <Col xs={24} className="center paymentoptions">
                    <button className="receiptOrder retailOrder" onClick={() => this.receipt()}> <ReceiptIcon className="receipticonb" /> Receipt </button>
                    <button className="retailOrder" onClick={() => this.newSale()}> Start New Sale </button>
                  </Col>

                </Col> : ''}

                {this.state.saveOrder ? <Col span={24} className="chargeOptions" style={{ textAlign: 'center' }}>




                  <Col span={24} className="center completeMasr">
                    <AccessTimeIcon className="completemark" />
                  </Col>
                  <Title level={2} className="center mgb-50">Order Placed! </Title>
                  <Title level={2} className="center mgb-50"> {this.numberFormat(this.state.total)}</Title>
                  <Select defaultValue="pending" style={{ width: 120, marginBottom: '50px' }} >
                    <Option value="pending">Pending</Option>
                    <Option value="confirmed">Confirmed</Option>
                  </Select>
                  <Col xs={24} className="center paymentoptions">
                    <button className="receiptOrder retailOrder" onClick={() => this.receipt()}> <ReceiptIcon className="receipticonb" /> Share </button>
                    <button className="retailOrder" onClick={() => this.newSale()}> Start New Sale </button>
                  </Col>

                </Col> : ''}

                <Col span={24} className={`receipt ${this.state.receipt ? 'show' : 'hide'}`}>

                  <Col span={12} className="relogo">
                    <img src={logo} />
                  </Col>
                  <Col span={12} className="renumber">
                    <CancelIcon className="closerb" onClick={this.closereceipt} />
                               #13 <br />
                               Customer Name <br />
                               Address  <br />
                               Contact No.  <br />
                               Date


                           </Col>
                  <Col span={24} className="details">
                    <Title>Test Store </Title>
                    <Text>Address City State Country | 9856321470</Text>
                  </Col>
                  <Col span={24} className="orderItems">
                    <Text className="strong">Total Items: {this.state.totalQ} </Text>
                    <table className="orders">
                      <thead>
                        <tr key="2">
                          <th> Item </th>
                          <th> Price </th>
                          <th > Quantity </th>
                          <th> Discount</th>
                          <th> Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.renderItemlistReceipt()}
                        <tr key="44" style={{ background: "#ccc" }}>
                          <th> </th>
                          <th></th>
                          <th> </th>
                          <th> {this.numberFormat(this.state.discountTotal)}</th>
                          <th> {this.numberFormat(this.state.subtotal)}</th>
                        </tr>
                      </tbody>
                    </table>

                    <div className="ttotal">
                      <table className="orderstotal">
                        <tr key="2">
                          <th> Subtotal : &nbsp; </th>
                          <th>&nbsp;{this.numberFormat(this.state.subtotal)}</th>
                        </tr>
                        <tr key="2">
                          <th>  Product Discount  : &nbsp; </th>
                          <th>&nbsp;{this.numberFormat(this.state.discountTotal)}</th>
                        </tr>
                        <tr key="2">
                          <th> Bulk Discount  : &nbsp; </th>
                          <th> &nbsp; {this.numberFormat(this.state.bulkdiscount)}</th>
                        </tr>
                        <tr key="2">
                          <th> Total :  &nbsp; </th>
                          <th>&nbsp;{this.numberFormat(this.state.total)}</th>
                        </tr>
                      </table>
                    </div>
                  </Col>
                  <Col md={12} xs={24} className="center shate">

                    <Col span={4}>
                      <PictureAsPdfIcon />

                    </Col>
                    <Col span={4}>

                      <SendIcon />

                    </Col>
                    <Col span={4}>

                      <PrintIcon />

                    </Col>
                    <Col span={4}>

                      <ShareIcon />
                    </Col>
                    <Col span={4} onClick={this.closereceipt}>

                      <CancelPresentationIcon />
                    </Col>

                  </Col>

                </Col>
              </div>

              <Modal
                title={`${this.state.viewItem[0].name}`}
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={() => this.handleCancel(this.state.viewItem[0].id)}
              >
                {this.state.viewItem[0].unit === "1" ? <span><Title level={3}> {this.state.viewItem[0].qty} {this.state.viewItem[0].unit_value} </Title>
                  <Input type='number' min='1' step='1' onChange={this.qtymaneger} value={this.state.viewItem[0].qty} />
                </span> : <span>
                    <Input type="number" onChange={this.changeNot} name="qty" value={this.state.viewItem[0].qty} />
                    <div style={{ marginBottom: 16, marginTop: 16, textAlign: `center` }}>

                      <span onClick={() => this.editQuntyMinus()} className="circle"> <Icon type="minus" /> </span>  <span className="qty"> {this.state.viewItem[0].qty} </span> <span className="circle" onClick={() => this.editQuntyPlus()}> <Icon type="plus" /> </span>
                    </div>
                  </span>}
                <p><strong> Price : {this.numberFormat(this.state.viewItem[0].price)}  |  Total Amount {this.numberFormat(this.state.viewItem[0].updatePrice)} </strong></p>

                <Radio.Group name="radiogroup" onChange={this.checkDiscount} defaultValue={this.state.viewItem[0].discountby}>
                  <Radio value={`cash`}> Cash Discount</Radio>
                  <Radio value={`percentage`}> Percentage Discount</Radio>
                </Radio.Group>
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <Input type="number" name="discount" value={this.state.viewItem[0].discoutnumber} onChange={this.changeNot} />

                  <span> {this.state.viewItem[0].discountby === "percentage" && this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice ? <small style={{ color: "red" }}> Discount cannot be more than 100%. </small> : ''}</span>
                  <span> {this.state.viewItem[0].discountby === "cash" && this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice ? <small style={{ color: "red" }}> Discount cannot be greater than item price.  </small> : ''}</span>
                </div>
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label>Order Ticket Notes</label>
                  <Input type="text" placeholder="Order Note..." value={this.state.viewItem[0].note} name="note" onChange={this.changeNot} />
                </div>
                {this.state.remove ? '' : <Button className="removeitem" onClick={() => this.removeItem(this.state.viewItem[0].id)}> Remove </Button>}
              </Modal>

              <Modal
                title="Modal"
                visible={this.state.orderticketshow}
                onOk={this.generateOrderTicketPrint}
                onCancel={this.onCancelorderTicket}
                okText="Create Order Ticket"
                cancelText="Cancel"
              >

                <Input type="text" readOnly value={this.state.activetablename} />
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label>Order Ticket Notes</label>
                  <Input type="text" placeholder="Order Note..." />
                </div>

                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label> Main Kitchen - Updated Now </label>
                  <hr />
                  <span className="active_status"></span> Added Items
                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                    {this.orderItems()}
                  </div>
                </div>

              </Modal>

              <Modal
                title="Customer"
                visible={this.state.customerForm}
                onOk={this.addCustomer}
                onCancel={this.handleCancel}
              >
                <Tabs defaultActiveKey="1" onChange={this.innerTabCallback}>
                  <TabPane tab="General" key="1">

                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Phone</label>
                      <Input type="text" placeholder="Customer mobile number" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Name</label>
                      <Input type="text" placeholder="Customer Name" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Email</label>
                      <Input type="text" placeholder="Customer email" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Occupants</label>
                      <Input type="text" placeholder="Number of seats occupied" />
                    </div>

                  </TabPane>

                </Tabs>

              </Modal>
              <Modal
                title="Customers"
                visible={this.state.customermodle}
                onOk={this.handleOkCustomer}
                onCancel={this.handleCancelCustomer}
              >
                <Search className="customerSearch_retail" placeholder="Search customer" /> <Button type="primary" className="primarybutton" onClick={this.showModalCustomernew} > New Customer </Button>

                <List
                  itemLayout="horizontal"
                  dataSource={datacustomer}
                  renderItem={item => (
                    <List.Item>
                      <List.Item.Meta
                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                        title={<a onClick={this.addcustomer}>{item.title}</a>}
                        description={ <a onClick={this.addcustomer} > Account Balance : +500 | Contact No. : 971365777 </a>}
                      />
                    </List.Item>
                  )}
                />

              </Modal>

              <Modal
                title="New Customers"
                visible={this.state.customermodlenew}
                onOk={this.handleOkCustomernew}
                onCancel={this.handleCancelCustomernew}
              >
                <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">

                  <Form.Item label="Name" {...formItemLayout}>
                    {getFieldDecorator('name', {
                      rules: [
                        {
                          required: true,
                          message: 'Please enter name',
                        },
                      ],
                    })(<Input placeholder=" Name" />)}
                  </Form.Item>
                  <Form.Item label="Mobile" {...formItemLayout}>
                    {getFieldDecorator('mobile', {
                      rules: [
                        {
                          required: true,
                          message: 'Please enter mobile',
                        },
                      ],
                    })(<Input type="text" placeholder="Mobile" />)}
                  </Form.Item>

                  <Form.Item label="Address " {...formItemLayout}>
                    <Input type="text" placeholder="Address" />
                  </Form.Item>

                  <Form.Item label="Email " {...formItemLayout}>
                    <Input type="text" placeholder="Email" />
                  </Form.Item>

                  <Form.Item label="Work Phone " {...formItemLayout}>
                    <Input type="text" placeholder="Phone" />
                  </Form.Item>



                  <Form.Item label="Allow customer to pay later " {...formItemLayout}>
                    <Switch defaultChecked onChange={this.manageStock} />
                  </Form.Item>

                </Form>
              </Modal>

            </Col>
          </Col>
        </Row>
      </div>
    )
  }
}
export default Form.create()(Checkout);