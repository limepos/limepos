import React from 'react';
import { Row, Button, Tabs, Col, Typography,Input } from 'antd';
import Grid from '@material-ui/core/Grid';
import product from './product.json';
import burger from '../../images/products/burger.jpg';
import coffee from '../../images/products/coffee.jpg';
import coldcoffee from '../../images/products/coldcoffee.jpg';
import noodles from '../../images/products/noodles.jpg';
import pizza from '../../images/products/pizza.jpeg';
import tea from '../../images/products/tea.webp';
import Currency from "../../components/currency";
import AddIcon from '@material-ui/icons/Add';
import tableSearch from '../../components/TableSearch/tableSearch';
const { Title, Text } = Typography
const { TabPane } = Tabs;
const { Search } = Input;
class Checkout extends React.Component {

  constructor(props) {
    super(props);

    this.newTabIndex = 0;
    const panes = [
      { title: 'All', content: 'Content of Tab Pane 1', key: '0' },
      { title: 'Grosary', content: 'Content of Tab Pane 2', key: '10' },
    ];
    this.state = {
      activeKey: panes[0].key,
      panes,
      currentTypeProduct: '0',
      count : product.length,
      productcolumns: [
        {
          title: 'Name',
          dataIndex: 'name',
        },
        {
          title: 'type',
          dataIndex: 'type',
        },
        {
          title: 'price',
          dataIndex: 'price',
        },
        {
          title: 'category',
          dataIndex: 'category',
        }
      ],
      productList: product,
      Product: product,
    };

    this.renderTableProdudctthum();
  }

  addNewProduct = () =>{
     
    // console.log("addNewProduct");
  }

  onChangeTab = activeKey => {

    console.log("activeKey", activeKey);
    this.setState({ activeKey,
      currentTypeProduct : activeKey });
  };

  renderTableProdudct() {

    return this.state.Product.map((table, index) => {
      const { id, name, price, type, color, image } = table;
     // console.log("index", index);
      return (
        <>
          {this.state.currentTypeProduct === type || this.state.currentTypeProduct === '0'|| name === 'placeholder' ? <li onClick={() => { name == "placeholder" ? this.addNewProduct() : this.addProduct(id, name, price, type, this) }}>
          { name !== 'placeholder'? <div className="table prod"  style={ image === '' ? {background : color} : { background: `url(${name === 'Burger' ? burger : name === 'Pizza' ? pizza : name === 'Tea' ? tea : name === 'Cold Coffee' ? coldcoffee : name === 'Coffee' ? coffee : name === 'Noodles' ? noodles : '' })` }}>
                         { image === '' ? <Text className="pdetailsName"> {name} </Text> : '' }
            </div> : type == "plus0" ? <AddIcon className="plushicon"/> : '' }
              { name !== 'placeholder'? <div className="pdetails" style={{ background : color }}>
              <div className="tablename">
                {name}
              </div>
              <div className="price">
                <span></span> <Currency />{price}
              </div>
            </div> : "" }
            <>{this.state.clickitem === id ? <img className={`b-flying-img ${this.state.move ? 'move' : this.state.movefix ? 'movefix' : ''} `} src={name === 'Burger' ? burger : name === 'Pizza' ? pizza : name === 'Tea' ? tea : name === 'cold Coffee' ? coldcoffee : name === 'Coffee' ? coffee : name === 'Noodles' ? noodles : ''} /> : ''}</>
          </li> : ''}
        </>
      )
    })
  }

  renderTableProdudctthum = () =>{

       console.log(this.state.count);
    
    
    for (let i = 0; i < 20 - this.state.count; i++) {
      this.state.Product.push({
         "id" : 0,
        "type" : 'plus' + i,
        "name" : "placeholder",
        "price" : "null",
        "Taxgroup" : "null",
        "category" : "null"
        
      })
    }
  }

  finderProduct = (e) => {

    let searchtext = e.target.value
    let columns = this.state.productcolumns
    let bookdata = this.state.productList

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      Product: r
    })
  //  this.renderTableProdudctthum();
  }

  render() {
    return (
      <div hi="s">
        <Grid container spacing={3}>
          <Grid item xs={6} className="pageTitle">
            <Title level={4}> Checkout </Title>
          </Grid>
          <Grid item xs={6} className="rightButton">
            <Button type="" onClick={this.import} className=""> Add Customer </Button> <Button type="primary" onClick={() => this.openexpenseform()} className="primarybutton">Add Product </Button>
          </Grid>
        </Grid>
        <Row gutter={[50, 50]}>
          <Col span={24}>
            <Col span={24} className="pepar">
              <Col span={16} >

                <Tabs
                  hideAdd
                  onChange={this.onChangeTab}
                  activeKey={this.state.activeKey}
                >
                  {this.state.panes.map(pane => (
                    <TabPane tab={pane.title} key={pane.key}>

                    </TabPane>
                  ))}
                </Tabs>
             
                <div className="orderTables">
                 <div style={{ marginBottom: 16, overflow: 'auto' }}>
                   <Search placeholder="Search Order" onChange={e => this.finderProduct(e)} className="" />
                 </div>
                  <ul className="retails product" >
                     {this.renderTableProdudct()}
                  </ul>
                
               </div>
               
              </Col>
              <Col span={8}>
                <Text>dsdsdsdsd</Text>
              </Col>
            </Col>
          </Col>
        </Row>
      </div>
    )
  }
}
export default (Checkout);