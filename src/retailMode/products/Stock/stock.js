import React from 'react';
import { Table, Input, DatePicker, Modal } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import burger from '../../../images/products/retail/flour-wheat.png';
const { Search } = Input;
const { RangePicker} = DatePicker;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    name: `Water-bottles`,
    variantgroups : '-',
    addongroups: `-`,
    productcategory: "General",
    taxgroup : "Zero Tax Group ",
    productprice : '₹100.00'
  });
}



class Stock extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
       {
        title: 'Product',
        dataIndex: 'pic',
        key: 'pic',
        render: (text, record) => (
          <span>
            <img src={ burger } className="table_image" onClick={() => this.editProudct(record.key) }/>  &nbsp;  <a  onClick={() => this.editProudct(record.key) }> {record.name} </a>
          </span>
        ),
      }
  
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
     

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

   changeDate = (date, dateString) =>{
    console.log(date, dateString);
  }

  deleteexpences =() =>{

    confirm({
      title: 'Confirm Cancellation',
      content: 'Are you sure you want to cancel this expense?',
      okText: 'Cancel Expenses',
      okType: 'danger',
      cancelText: 'Back',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });

  }
  
  editProudct = (id) => {
    this.props.history.history.push(`/app/retail/product/edit/${id}?stock=0`);
  }
  


  render() {
    
    const { selectedRowKeys } = this.state;
 
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                     <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" /> 
                  
                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table  columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (Stock);