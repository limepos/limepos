import React from 'react';
import { Modal, Typography, Button, Tabs, Row, Col, Input,Checkbox } from 'antd';
import Grid from '@material-ui/core/Grid';
import AllProduct from './AllProduct/allProducts';
import Stock from './Stock/stock';
import Category from './category/category';
import AddIcon from '@material-ui/icons/Add';
const { Title } = Typography;
const { TabPane } = Tabs;
class Receipts extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-categories/1'){
     
    this.state.current_tab = '1';
  
     console.log("current tab", this.state.current_tab)

  } 
  if( currantbackurl === '/app/setup/product-categories/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
    
  }
  }

  state = {
     current_tab: '1',
     visible: false,
     showExpenseform : false,
     sendonEmail : false,
     mailcheck: false,
     download : false
  }

  componentDidMount(){

   // this.props.history
    console.log("did" )  
  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,
    })

  }
   
  addnewRegister = (tab) => {

  }

  import = () => {
  
    this.props.history.push("/app/retail/product/import");

  }

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
      showExpenseform :false
    });
  }
  openexpenseform = () => {
    this.setState({
      showExpenseform :true
    })
    }

    addProuducts = () =>{
      this.props.history.push("/app/retail/add-product");
    }

   addExpense =() =>{
     this.setState({
      showExpenseform :false,
      sendonEmail : false,
      mailcheck: false,
      download : false
     })
   } 

   newProudct = () => {
    this.props.history.push(`/app/retail/add-product`);
   }
   ondownload=(e)=>{
    this.setState({
      download : !this.state.download
    })
  }
 
   sendOnemail=(e)=>{
         this.setState({
           sendonEmail : e.target.checked,
           mailcheck: !this.state.mailcheck
         })
   }

  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={3} className="pageTitle">
                    <Title level={4}> Products </Title>  
                </Grid>
                <Grid item xs={9} className="rightButton">
    { this.state.current_tab == 3 ? '':<><Button type="" onClick={ this.openexpenseform }  className=""> Export </Button> <Button type="primary" onClick={() =>  this.newProudct()}  className="primarybutton">Add Product </Button></> }
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                        <TabPane tab="Items"  key="1">
                              <AllProduct history={this.props}/>
                       </TabPane>
                     
                       <TabPane tab="Categories"  key="3">
                              <Category history={this.props}/>
                       </TabPane>
                      </Tabs>
                  </Col> 
                </Col>
                  <Modal
                  title="Export Products"
                  visible={this.state.showExpenseform }
                  onOk={this.addExpense}
                  onCancel={this.handleCancel}
                  okText = "Submit"
                  >
                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                       <Checkbox  onChange={this.ondownload}  checked={ this.state.download } >Download report on local drive </Checkbox>
                    </div>
                     <div style={{ marginBottom: 16, marginTop: 16 }}>
                     <Checkbox onChange={this.sendOnemail} checked={ this.state.mailcheck } value="email">Send To Email Address  </Checkbox>
                    
                      {this.state.sendonEmail ? <Input type="email" placeholder="Email*" /> : '' }
                    </div>
                  </Modal>
              </Row>     
            </div>
    )
  }
}
export default (Receipts);