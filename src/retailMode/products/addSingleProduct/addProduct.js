import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Typography,
  Upload,
  message, 
  Switch
} from 'antd';
import dummy from "../../../images/products/retail/flour-wheat.png";
import { SketchPicker } from 'react-color';
import reactCSS from 'reactcss'
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
 

class EditProduct extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
    displayColorPicker: false,
    background: '#66fecb',
    saleBy : ''
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

   handleChange = (value) => {
    console.log(`selected ${value}`);
  }


  handleClickc = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClosec = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChangec = (color) => {
    this.setState({ background: color.hex });
  };
  manageStock = (checked) => {
    console.log(`switch to ${checked}`);
  }
  saleby = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      saleby:value
    })
  }
  render() {
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;
    

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
    const styles = reactCSS({
      'default': {
        color: {
          width: '36px',
          height: '14px',
          borderRadius: '2px',
          background: `${ this.state.background }`,
        },
        swatch: {
          padding: '2px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}>  <span onClick={ this.goback }> Products  </span> / New </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col xs={24} md={8}>
          
    <Title level={4}>Your Product Details </Title>
           <Text>Edit your product details here. Product name should be unique.</Text>
        </Col>
        <Col xs={24} md={16} className="pepar">
        <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={this.handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : <img src={dummy} alt="avatar" style={{ width: '100%' }} />}
      </Upload>
        
       <Text> Upload Product Image. It should be a square image.</Text>
       <br/>
       <Text> &nbsp; </Text>
      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
      <Form.Item label="Product Color" {...formItemLayout}>
        <div style={ styles.swatch } onClick={ this.handleClickc }>
          <div style={ styles.color } />
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClosec }/>
          <SketchPicker color={ this.state.background } onChange={ this.handleChangec } />
        </div> : null }
        </Form.Item>
        <Form.Item label="Product Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter name',
              },
            ],
          })(<Input  placeholder="Product  Name" />)}
        </Form.Item>
        <Form.Item label="Product Price" {...formItemLayout}>
            {getFieldDecorator('price', {
            rules: [
              {
                required: true,
                message: 'Please enter price',
              },
            ],
          })(<Input type="number" placeholder="Price" />)}
        </Form.Item>
        <Form.Item label="Product Category" {...formItemLayout}>
        { getFieldDecorator('category', {
            rules: [
              {
                required: true,
                message: 'Please Select Category.',
              },
            ],
          })(<Select nstyle={{ width: '100%' }}  placeholder="Product Category">
            <Option value="general"> General </Option>
          </Select>) }
        </Form.Item>
        <Form.Item label="Description " {...formItemLayout}>
              <Input type="text" placeholder="Description" />
        </Form.Item>
        
        <Form.Item label="Code " {...formItemLayout}>
              <Input type="text" placeholder="Code" />
        </Form.Item>

        <Form.Item label="Cost " {...formItemLayout}>
              <Input type="text" placeholder="Cost" />
        </Form.Item>

        <Form.Item label="Sell By" {...formItemLayout}>
        { getFieldDecorator('sellby', {
            rules: [
              {
                required: true,
                message: 'Please Select.',
              },
            ],
          })(<Select nstyle={{ width: '100%' }}  placeholder="Sell By" onChange={this.saleby}>
            <Option value="unit"> Unit </Option>
            <Option value="fraction"> Fraction (Kilo, Pound, liter, etc) </Option>
          </Select>) }
        </Form.Item>
        
        { this.state.saleby == 'fraction' ? <Form.Item label="Unit of Measure" {...formItemLayout}>
          <Select nstyle={{ width: '100%' }}  placeholder="Unit of Measure">
            <Option value="Kg"> Kg </Option>
            <Option value="Pound"> Pound </Option>
            <Option value="Liter"> Liter </Option>
            <Option value="Meter"> Meter </Option>
          </Select>
        </Form.Item> : '' }
        <Form.Item label="Manage stock for this product " {...formItemLayout}>
             <Switch defaultChecked onChange={this.manageStock} />
        </Form.Item>
       
        <Form.Item label="Stock" {...formItemLayout}>
              <Input type="number" placeholder="Stock" />
        </Form.Item>
       
        <Form.Item label="Minimum " {...formItemLayout}>
              <Input type="number" placeholder="Minimum" />
        </Form.Item>

        <Form.Item >
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>

      </Row>
    </div>
    );
  }
}
export default Form.create()(EditProduct);