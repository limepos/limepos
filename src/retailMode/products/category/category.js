import React from 'react';
import { Table, Input, DatePicker, Modal,Button,Icon } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import burger from '../../../images/products/burger.jpg';
import AddIcon from '@material-ui/icons/Add';
const { Search } = Input;
const { RangePicker} = DatePicker;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    name: `Category `+i
  });
}



class Category extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [

      {
        title: 'Category Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a onClick={() => this.showModal(record.key) }> {record.name} </a>
          </span>
        ),
      }
  
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: '',
    visible: false,
    addcategory :false
  };
     

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

   changeDate = (date, dateString) =>{
    console.log(date, dateString);
  }

  deleteexpences =() =>{

    confirm({
      title: 'Confirm Cancellation',
      content: 'Are you sure you want to cancel this expense?',
      okText: 'Cancel Expenses',
      okType: 'danger',
      cancelText: 'Back',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });

  }
  
  editProudct = (id) => {
    this.props.history.history.push(`/app/retail/product/edit/${id}`);
  }
  
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  showModalcategory = () => {
    this.setState({
      addcategory: true,
    });
  };

  handleOkcategoty = e => {
    console.log(e);
    this.setState({
      addcategory: false,
    });
  };

  handleCancelcategoty = e => {
    console.log(e);
    this.setState({
      addcategory: false,
    });
  };

  render() {
    
    const { selectedRowKeys } = this.state;
 
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
           
                <div style={{ marginBottom: 16 }}>
                <Button type="" onClick={this.showModalcategory}>
                 Add Category 
            </Button>  <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" /> 
                  
                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table  columns={this.state.columns} dataSource={this.state.displaybookdata} />

                <Modal
                    title="Add Category"
                    visible={this.state.addcategory}
                    onOk={this.handleOkcategoty}
                    onCancel={this.handleCancelcategoty}
                    >
                   <div style={{ marginBottom: 16, marginTop: 16 }}>
                    <label>Category Name</label>
                      <Input type="text" placeholder="Category Name" />
                  </div>
                </Modal>

                <Modal
                    title="Update Category"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    >
                   <div style={{ marginBottom: 16, marginTop: 16 }}>
                    <label>Category Name</label>
                      <Input type="text" placeholder="Category Name" />
                  </div>
                  <Button className="removeitem"> Remove </Button>
                </Modal>
            </div>
            
    )
  }
}
export default (Category);