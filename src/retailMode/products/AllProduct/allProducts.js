import React from 'react';
import { Table, Input, DatePicker, Modal } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import burger from '../../../images/products/retail/flour-wheat.png';
import DeleteIcon from '@material-ui/icons/Delete';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { Search } = Input;
const { RangePicker } = DatePicker;
const { confirm } = Modal;



const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    pic : '',
    name: `Water-bottles`,
    productcategory: "General",
    stock: "5",
    productprice: '$100.00',
    action : ''
  });
}



class Allproducts extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle: "Action",
    columns: [
      {
        title: 'Product',
        dataIndex: 'pic',
        key: 'pic',
        render: (text, record) => (
          <span>
            <img src={burger} className="table_image" onClick={() => this.editProudct(record.key)} />
          </span>
        ),
      },
      {
        title: 'Product Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a onClick={() => this.editProudct(record.key)}> {record.name} </a>
          </span>
        ),
      },
      {
        title: 'Stock',
        dataIndex: 'stock',
        key: 'stock',
      },
      {
        title: 'Product Category',
        dataIndex: 'productcategory',
        key: 'productcategory',
      },
      {
        title: 'Product Price',
        dataIndex: 'productprice',
        key: 'productprice',
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: (text, record) => (
          <span>
             <a onClick={() => this.editProudct(record.key)}> <BorderColorIcon /> </a> &nbsp; &nbsp; 
              <a onClick={() => this.editProudct(record.key)}> <VisibilityIcon /> </a> &nbsp; &nbsp; 
             <a onClick={() => this.showDeleteConfirm(record.key)}> <DeleteIcon /> </a>
          </span>
        ),
      }
    ],
    bookdata: data,
    displaybookdata: data,
    sortName: '',
    sortValue: ''
  };


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  changeDate = (date, dateString) => {
    console.log(date, dateString);
  }

   showDeleteConfirm = () => {
    confirm({
      title: 'Are you sure delete this Product.',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  editProudct = (id) => {
    this.props.history.history.push(`/app/retail/product/edit/${id}`);
  }



  render() {

    const { selectedRowKeys } = this.state;

    const hasSelected = selectedRowKeys.length > 0;


    return (
      <div>
        <div style={{ marginBottom: 16 }}>
          <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />

          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />
      </div>
    )
  }
}
export default (Allproducts);