import React from 'react';
import {
    Form,
    Input,
    Select,
    Row,
    Col,
    Button,
    Typography,
    Upload,
    message,
    Switch,
    Tabs,
    Timeline,
    Modal,
    Table,
    Radio, Checkbox 
} from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import country from './countory';
const { TextArea } = Input;
const { TabPane } = Tabs;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
const { confirm } = Modal;
const { Search } = Input;
const data = [];


const children = [];
country.map((c) => {
    children.push(<Option key={ c.name }>{c.name}</Option>);
})

class Addcustomer extends React.Component {

    state = {
        confirmDirty: false

    };

    constructor(props) {
        super(props);
    }

    goback = () => {
        this.props.history.goBack();
    };


    tabcallback = (key) => {
        this.setState({
            current_tab: key,

        });
    }


    numberFormat = (value) =>
        new Intl.NumberFormat('en-us', {
            style: 'currency',
            currency: 'USD'
        }).format(value);


    handleChange(value) {
        console.log(`selected ${value}`);
    }

    onChange(checked) {
        console.log(`switch to ${checked}`);
      }



    render() {
        const { imageUrl } = this.state;
        const { getFieldDecorator } = this.props.form;


        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
        };

        return (
            <div>

                <Row gutter={[25, 25]}>
                    <Col span={24}>
                        <Title level={4}>  <span onClick={this.goback}> Settings </span> / Business information </Title>
                    </Col>
                </Row>

                <Row gutter={[50, 50]}>
                    <Col span={24} className="pepar">

                        <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">
                             
`                            <Form.Item label="Business Name" {...formItemLayout}>
                                    { getFieldDecorator('businessname', {
                                    rules: [
                                    {
                                    required: true,
                                    message: 'Please enter business name.',
                                    },
                                    ],
                                    })(<Input placeholder=" Business Name" />)}
                            </Form.Item>

                              <Form.Item label="Country" {...formItemLayout}>
                                <Select
                                    style={{ width: '100%' }}
                                    placeholder="Please select Country"
                                    defaultValue="India"
                                    onChange={this.handleChange}
                                  >
                                    {children}
                                  </Select>
                            </Form.Item>
                            <Form.Item label="Mobile" {...formItemLayout}>
                               <Input type="text" placeholder="Mobile" />
                            </Form.Item>

                            <Form.Item label="Whats App" {...formItemLayout}>
                                {getFieldDecorator('whatsapp', {
                                rules: [
                                {
                                required: true,
                                message: 'Please enter Whats App',
                                },
                                ],
                                })(<Input type="text" placeholder="Whats App" />)}
                            </Form.Item>

                            <Form.Item label="Address" {...formItemLayout}>
                                <Input type="text" placeholder="Address" />
                            </Form.Item>

                            <Form.Item label="Email" {...formItemLayout}>
                                <Input type="text" placeholder="Email" />
                            </Form.Item>

                            <Form.Item  {...formItemLayout}>
                                 <Title level={4}> Receipt</Title>
                            </Form.Item>

                            <Form.Item label="Header" {...formItemLayout}>
                                    <TextArea rows={4} placeholder="Header" />
                            </Form.Item>

                            <Form.Item label="Footer" {...formItemLayout}>
                                    <TextArea rows={4} placeholder="Footer" />
                            </Form.Item>
                            <Form.Item {...formItemLayout}>
                            <lable> Display customer information  </lable>  <span className="rightd"><Switch defaultChecked onChange={this.onChange} /> </span>
                            </Form.Item>

                            <Form.Item >
                                <Button type="default" onClick={this.goback}>
                                    Go Back
                                  </Button>
                                 <Button type="primary" htmlType="submit">
                                    Save
                                     </Button>


                            </Form.Item>
                        </Form>
                    </Col>

                </Row>

            </div>
        );
    }
}
export default Form.create()(Addcustomer);