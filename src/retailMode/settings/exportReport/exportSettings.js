import React from 'react';
import {
    Form,
    DatePicker,
    Row,
    Col,
    Typography,
    Input,
    Button,
    Checkbox 
} from 'antd';

const { RangePicker } = DatePicker;
const { Text } = Typography;
const { Title } = Typography;

const plainOptions = ['Sales', 'Products', 'Customers'];

class Addcustomer extends React.Component {

    state = {
        confirmDirty: false

    };

    constructor(props) {
        super(props);
    }

    goback = () => {
        this.props.history.goBack();
    };

    onChange(checkedValues) {
      console.log('checked = ', checkedValues);
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
        };

        return (
            <div>

                <Row gutter={[25, 25]}>
                    <Col span={24}>
                        <Title level={4}>  <span onClick={this.goback}> Settings </span> / Export Reports </Title>
                    </Col>
                </Row>

                <Row gutter={[50, 50]}>
                    <Col span={24} className="pepar">

                        <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">
                             
                            <Form.Item label="Date Range" {...formItemLayout}>
                                 <RangePicker />
                            </Form.Item>
                            <Form.Item {...formItemLayout}>
                              
                            </Form.Item>
                          
                            <Form.Item label="which report would you like to export ?" {...formItemLayout}>
                                 
                               <Checkbox.Group options={plainOptions} defaultValue={['Apple']} onChange={this.onChange} />
                            </Form.Item>  
                            <Form.Item {...formItemLayout}>
                                
                                </Form.Item>
                            <Form.Item >
                                <Button type="default" onClick={this.goback}>
                                    Go Back
                                  </Button>
                                 <Button type="primary" htmlType="submit">
                                    Generate  Report
                                     </Button>


                            </Form.Item>
                        </Form>
                    </Col>

                </Row>

            </div>
        );
    }
}
export default Form.create()(Addcustomer);