import React from 'react';
import {
    Form,
    Input,
    Select,
    Row,
    Col,
    Button,
    Typography,
    Upload,
    message,
    Switch,
    Tabs,
    Timeline,
    Modal,
    Table,
    Radio, Checkbox 
} from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';

const { TextArea } = Input;
const { TabPane } = Tabs;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
const { confirm } = Modal;
const { Search } = Input;
const data = [];

class Addcustomer extends React.Component {

    state = {
        confirmDirty: false

    };

    constructor(props) {
        super(props);
    }

    goback = () => {
        this.props.history.goBack();
    };


    tabcallback = (key) => {
        this.setState({
            current_tab: key,

        });
    }


    numberFormat = (value) =>
        new Intl.NumberFormat('en-us', {
            style: 'currency',
            currency: 'USD'
        }).format(value);


    handleChange(value) {
        console.log(`selected ${value}`);
    }

    onChange(checked) {
        console.log(`switch to ${checked}`);
      }



    render() {
        const { imageUrl } = this.state;
        const { getFieldDecorator } = this.props.form;


        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
        };

        return (
            <div>

                <Row gutter={[25, 25]}>
                    <Col span={24}>
                        <Title level={4}>  <span onClick={this.goback}> Settings </span> / Sales Tax </Title>
                    </Col>
                </Row>

                <Row gutter={[50, 50]}>
                    <Col span={24} className="pepar">

                        <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">
                             

                            <Form.Item {...formItemLayout}>
                              <lable> Collect sales tax</lable>  <span className="rightd"><Switch defaultChecked onChange={this.onChange} /> </span>
                            </Form.Item>

                           <Form.Item label="Tax Type" {...formItemLayout}>
                                <Radio.Group defaultValue="a" buttonStyle="solid">
                                  <Radio.Button value="a">Percentage</Radio.Button>
                                  <Radio.Button value="b">Fixed</Radio.Button>
                                </Radio.Group>
                            </Form.Item>
                            <Form.Item label="Description" {...formItemLayout}>
                               <Input type="text" placeholder="Description" />
                            </Form.Item>

                            <Form.Item label="Sales Tax " {...formItemLayout}>
                               <Input type="text" placeholder="Sales Tax" />
                            </Form.Item>

                            <Form.Item label="Tax On" {...formItemLayout}>
                                <Radio.Group defaultValue="a" buttonStyle="solid">
                                  <Radio.Button value="a">Added To the price</Radio.Button>
                                  <Radio.Button value="b">Included in price (VAT)</Radio.Button>
                                </Radio.Group>
                            </Form.Item>
                            
                            <Form.Item {...formItemLayout}>
                              <lable> Allow exemption </lable>  <span className="rightd"><Switch defaultChecked onChange={this.onChange} /> </span>
                               <p>You'll be able to disable it during checkout</p>
                            </Form.Item>

                            <Form.Item >
                                <Button type="default" onClick={this.goback}>
                                    Go Back
                                  </Button>
                                 <Button type="primary" htmlType="submit">
                                    Save
                                     </Button>


                            </Form.Item>
                        </Form>
                    </Col>

                </Row>

            </div>
        );
    }
}
export default Form.create()(Addcustomer);