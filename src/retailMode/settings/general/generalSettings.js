import React from 'react';
import {
    Form,
    Input,
    Select,
    Row,
    Col,
    Button,
    Typography,
    Upload,
    message,
    Switch,
    Tabs,
    Timeline,
    Modal,
    Table,
    Radio, Checkbox 
} from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import currency from './currency.json';

const { TabPane } = Tabs;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
const { confirm } = Modal;
const { Search } = Input;
const data = [];


const children = [];
currency.map((c) => {
    children.push(<Option value={c.cc} label={c.name}>
        <div className="demo-option-label-item">
            <span role="img" aria-label={c.name}>
                {c.symbol}
            </span>
            {c.name}
        </div>
    </Option>);
})

class Addcustomer extends React.Component {

    state = {
        confirmDirty: false

    };

    constructor(props) {
        super(props);
    }

    goback = () => {
        this.props.history.goBack();
    };


    tabcallback = (key) => {
        this.setState({
            current_tab: key,

        });
    }


    numberFormat = (value) =>
        new Intl.NumberFormat('en-us', {
            style: 'currency',
            currency: 'USD'
        }).format(value);


    handleChange(value) {
        console.log(`selected ${value}`);
    }

    onChange(checked) {
        console.log(`switch to ${checked}`);
      }



    render() {
        const { imageUrl } = this.state;
        const { getFieldDecorator } = this.props.form;


        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
            },
        };

        return (
            <div>

                <Row gutter={[25, 25]}>
                    <Col span={24}>
                        <Title level={4}>  <span onClick={this.goback}> Settings </span> / General </Title>
                    </Col>
                </Row>

                <Row gutter={[50, 50]}>
                    <Col span={24} className="pepar">

                        <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} className="centerform">

                            <Form.Item label="Currency" {...formItemLayout}>
                                {getFieldDecorator('name', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Please select currency',
                                        },
                                    ],
                                })(<Select
                                    style={{ width: '100%' }}
                                    placeholder="Please select"
                                    defaultValue="lucy"
                                    onChange={this.handleChange}
                                    optionLabelProp="label"
                                >
                                    {children}
                                </Select>)}
                            </Form.Item>
                           

                            <Form.Item {...formItemLayout}>
                            <lable> Decimal Parts  </lable>  <span className="rightd"><Switch defaultChecked onChange={this.onChange} /> </span>
                            </Form.Item>

                            <Form.Item label="View Canceled Transaction / Order as" {...formItemLayout}>
                                <Radio.Group defaultValue="a" buttonStyle="solid">
                                  <Radio.Button value="a">Cross Out</Radio.Button>
                                  <Radio.Button value="b">Hide</Radio.Button>
                                </Radio.Group>
                            </Form.Item>

                            <Form.Item label="Checkout Product Sorting" {...formItemLayout}>
                                <Radio.Group defaultValue="a" buttonStyle="solid">
                                  <Radio.Button value="a">Creating date </Radio.Button>
                                  <Radio.Button value="b">A-Z</Radio.Button>
                                </Radio.Group>
                            </Form.Item>



                            <Form.Item {...formItemLayout}>
                            <lable> Allow customer to pay later </lable>   <span className="rightd"><Switch defaultChecked onChange={this.manageStock} /></span>
                              <p>
                                  Disable this feature if you do not wish to use this payment method.
                              </p>
                            </Form.Item>

                            <Form.Item >
                                <Button type="default" onClick={this.goback}>
                                    Go Back
                                  </Button>
                                 <Button type="primary" htmlType="submit">
                                    Clear data
                                     </Button>


                            </Form.Item>
                        </Form>
                    </Col>

                </Row>

            </div>
        );
    }
}
export default Form.create()(Addcustomer);