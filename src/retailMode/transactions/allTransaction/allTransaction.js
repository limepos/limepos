import React from 'react';
import { Table, Input, DatePicker, Modal, Icon, Radio, Checkbox, Select  } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import TuneIcon from '@material-ui/icons/Tune';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { Search } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    type : 'Credit Card',
    amount : "$500.00",
    customer: "Customer Name",
    date: 'Feb 14, 2020, 5:00 PM',
    
  });
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{`Person `+i}</Option>);
}

class AllTransactions extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
   
   columns: [
     {
        title: '#id',
        dataIndex: 'id',
        key: 'id',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > # {record.key} </a>
          </span>
        )
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.type} </a>
          </span>
        )
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.amount} </a>
          </span>
        )
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.date} </a>
          </span>
        )
      },
      {
        title: 'Customer',
        dataIndex: 'customer',
        key: 'customer',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.customer} </a>
          </span>
        )
      }
     
     
    ],
    bookdata: data,
    displaybookdata: data,
    sortName: '',
    sortValue: ''
  };


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  changeDate = (date, dateString) => {
    console.log(date, dateString);
  }

  viewOrder = (id) => {
    console.log(id);
    this.props.history.history.push(`/app/retail/transaction/detail/${id}`);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };
  
  onChangepaymentMethod(checkedValues) {
    console.log('checked = ', checkedValues);
  }

  handleChange = (value) =>{
    console.log(`selected ${value}`);
  }
  showDeleteConfirm = () => {
    confirm({
      title: 'Are you sure delete this Product.',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  
  handleChange = (value) =>{
    console.log(`selected ${value}`);
  }

  render() {

    const options = [
        { label: 'Cash', value: 'cash' },
        { label: 'Credit Card', value: 'creditcard' },
        { label: 'Debit Card', value: 'debitcard' },
        { label: 'Check', value: 'check' },
        { label: 'Other', value: 'other' },
        { label: 'Store Credit', value: 'storecredit' },
        { label: 'Pay Later', value: 'paylater' },
        { label: 'Coupon', value: 'Orange' }
      ];

    return (
      <div>
        <div style={{ marginBottom: 16,overflow: `auto` }}>
          <TuneIcon className="searchBox filterextra" onClick={this.showModal} />
          <RangePicker onChange={this.changeDate} className="searchBox " />
          <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />
        </div>
        <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />

        <Modal
          title="Fiter Orders"
          visible={this.state.visible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
          okText	="Apply Filter"
         >
      <Radio.Group defaultValue="a" buttonStyle="solid" size="large" className="fiterButtons">
        <Radio.Button value="a">Last 30 days </Radio.Button>
        <Radio.Button value="b">Today</Radio.Button>
        <Radio.Button value="c">Yesterday</Radio.Button>
        <Radio.Button value="d">This Week </Radio.Button>
        <Radio.Button value="e">Last Week </Radio.Button>
        <Radio.Button value="f">This Month </Radio.Button>
        <Radio.Button value="g">Last Month </Radio.Button>
        <Radio.Button value="h">This Year </Radio.Button>
        <Radio.Button value="i">Last Year </Radio.Button>
      </Radio.Group>
       <br />
       <br />
         <h3> Type </h3>
      <Checkbox.Group
      options={options}
      defaultValue={['Apple']}
      onChange={this.onChangepaymentMethod}
      className="paymentmethod"
     />
       <br />
       <br />
       <h3> Sales Person </h3>
       <Select
          mode="tags"
          placeholder="Please select"
          defaultValue={['Person 1', 'Person 2']}
          onChange={this.handleChange}
          style={{ width: '100%' }}
        >
          {children}
        </Select>
          <br />
          <br />
          <Checkbox onChange={this.onChange}> Show canceled order only </Checkbox>
          
        </Modal> 

      </div>
  )
  }
}
export default (AllTransactions);