import React from 'react';
import { Table, Input, DatePicker, Modal, Icon, Radio, Checkbox, Select,Tag   } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import TuneIcon from '@material-ui/icons/Tune';
const { Search } = Input;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { confirm } = Modal;
const { CheckableTag } = Tag;
const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    orderNumber: '#' + i,
    amount: '₹50.00',
    date: `Feb 14, 2020, 5:00 PM`,
    items: `5`,
    status: `Pending`,
  });
}

const tagsFromServer = ['Confirm', 'Pending'];

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{`Person `+i}</Option>);
}

class AllExpenses extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle: "Action",
    ModalText: 'Content of the modal',
    visible: false,
    confirmLoading: false,
    columns: [
      {
        title: 'Order No',
        dataIndex: 'orderNumber',
        key: 'orderNumber',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.orderNumber} </a>
          </span>
        )
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.amount} </a>
          </span>
        )
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.date} </a>
          </span>
        )
      },
      {
        title: 'Items',
        dataIndex: 'items',
        key: 'items',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.items} </a>
          </span>
        )
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        render: (text, record) => (
          <span>
            <a onClick={() => this.viewOrder(record.key)} > {record.status} </a>
          </span>
        )
      }

    ],
    bookdata: data,
    displaybookdata: data,
    sortName: '',
    sortValue: '',
    selectedTags: []
  };


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  changeDate = (date, dateString) => {
    console.log(date, dateString);
  }

  viewOrder = (id) => {

    console.log(id);
    this.props.history.history.push(`/app/retail/order/detail/${id}`);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };
  
  onChangepaymentMethod(checkedValues) {
    console.log('checked = ', checkedValues);
  }

  handleChange = (value) =>{
    console.log(`selected ${value}`);
  }

  handleChangetag(tag, checked) {
    const { selectedTags } = this.state;
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag);
    console.log('You are interested in: ', nextSelectedTags);
    this.setState({ selectedTags: nextSelectedTags });
  }

  render() {
    const { selectedTags } = this.state;
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
 
    const options = [
      { label: 'Cash', value: 'cash' },
      { label: 'Credit Card', value: 'creditcard' },
      { label: 'Debit Card', value: 'debitcard' },
      { label: 'Check', value: 'check' },
      { label: 'Other', value: 'other' },
      { label: 'Store Credit', value: 'storecredit' },
      { label: 'Pay Later', value: 'paylater' },
      { label: 'Coupon', value: 'Orange' }
    ];

    return (
      <div>
        <div style={{ marginBottom: 16 }}>
          <TuneIcon className="searchBox filterextra" onClick={this.showModal} />
            {tagsFromServer.map(tag => (
          <CheckableTag
            key={tag}
            checked={selectedTags.indexOf(tag) > -1}
            className="rightfloat"
            onChange={checked => this.handleChangetag(tag, checked)}
          >
            {tag}
          </CheckableTag>
            ))}
          <RangePicker onChange={this.changeDate} className="searchBox " />
          <Search placeholder="input search text" onChange={e => this.finder(e)} className="searchBox" />

          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Table columns={this.state.columns} dataSource={this.state.displaybookdata} />

        <Modal
          title="Filter Orders"
          visible={this.state.visible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
          okText	="Apply Filter"
         >
      <Radio.Group defaultValue="a" buttonStyle="solid" size="large" className="fiterButtons">
        <Radio.Button value="a">Last 30 days </Radio.Button>
        <Radio.Button value="b">Today</Radio.Button>
        <Radio.Button value="c">Yesterday</Radio.Button>
        <Radio.Button value="d">This Week </Radio.Button>
        <Radio.Button value="e">Last Week </Radio.Button>
        <Radio.Button value="f">This Month </Radio.Button>
        <Radio.Button value="g">Last Month </Radio.Button>
        <Radio.Button value="h">This Year </Radio.Button>
        <Radio.Button value="i">Last Year </Radio.Button>
      </Radio.Group>
       <br />
       <br />
       <h3> Order Status </h3>
        
          {tagsFromServer.map(tag => (
          <CheckableTag
          key={tag}
          checked={selectedTags.indexOf(tag) > -1}
          
          onChange={checked => this.handleChangetag(tag, checked)}
          >
          {tag}
          </CheckableTag>
          ))}
                                <br />
       <br />
         <h3> Payment method </h3>
      <Checkbox.Group
      options={options}
      defaultValue={['Apple']}
      onChange={this.onChangepaymentMethod}
      className="paymentmethod"
     />
       <br />
       <br />
       <h3> Sales Person </h3>
       <Select
          mode="tags"
          placeholder="Please select"
          defaultValue={['Person 1', 'Person 2']}
          onChange={this.handleChange}
          style={{ width: '100%' }}
        >
          {children}
        </Select>
          <br />
          <br />
                <Checkbox onChange={this.onChange}> Show canceled order only </Checkbox>
        </Modal>
      </div>

    )
  }
}
export default (AllExpenses);