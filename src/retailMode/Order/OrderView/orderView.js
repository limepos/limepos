import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Typography,
  Upload,
  message,
  Switch,
  Tabs,
  Timeline,
  Modal
} from 'antd';
import dummy from "../../../images/products/retail/flour-wheat.png";
import { SketchPicker } from 'react-color';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import logo from "../../../images/logo.png";
import CancelIcon from '@material-ui/icons/Cancel';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import SendIcon from '@material-ui/icons/Send';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';
import HorizontalSplitIcon from '@material-ui/icons/HorizontalSplit';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import MoneyIcon from '@material-ui/icons/Money';
import SubtitlesIcon from '@material-ui/icons/Subtitles';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import ReceiptIcon from '@material-ui/icons/Receipt';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import CreditCardIcon from '@material-ui/icons/CreditCard';
const { TabPane } = Tabs;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
const { confirm } = Modal;
function getBase6tab4(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}


class EditProduct extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
    displayColorPicker: false,
    background: '#66fecb',
    current_tab: "1",
    chargeshow : false,
    complete : false,
    orderview :true, 
    paymentType : '',
    order: [
      {
        id: 1,
        name: "Bottle",
        price: "150",
        type: "10",
        updatePrice: "150",
        qty: 1,
        discount: 0,
        note: "",
        discountby: "",
        discoutnumber: 0,
        unit: "1",
        unit_value: "kg"
      },
      {
        id: 2,
        name: "Soya Oil",
        price: "200",
        type: "10",
        updatePrice: "200",
        qty: 1,
        discount: 0,
        note: "",
        discountby: "",
        discoutnumber: 0,
        unit: "1",
        unit_value: "Ltr"
      },
      {
        id: 3,
        name: "Handwash",
        price: "120",
        type: "10",
        updatePrice: "120",
        qty: 1,
        discount: 0,
        note: "",
        discountby: "",
        discoutnumber: 0,
        unit: "1",
        unit_value: "Ltr"
      },
      {
        id: 4,
        name: "Pasta",
        price: "130",
        type: "10",
        updatePrice: "130",
        qty: 1,
        discount: 0,
        note: "",
        discountby: "",
        discoutnumber: 0,
        unit: "1",
        unit_value: "kg"
      },
      {
        id: 5,
        name: "Flour Wheat",
        price: "170",
        type: "12",
        updatePrice: "170",
        qty: 1,
        discount: 0,
        note: "",
        discountby: "",
        discoutnumber: 0,
        unit: "1",
        unit_value: "kg"
      }
    ]
  };

  constructor(props) {
    super(props);
  }

  goback = () => {
    this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  handleChange = (value) => {
    console.log(`selected ${value}`);
  }
  tabcallback = (key) => {
    this.setState({
      current_tab: key,

    });
  }


  numberFormat = (value) =>
    new Intl.NumberFormat('en-us', {
      style: 'currency',
      currency: 'USD'
    }).format(value);

  renderItemlistReceipt() {
    return this.state.order.map((items, index) => {
      const { id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber, unit, unit_value } = items;
      return (
        <tr key={id} className={this.state.clickitem === id ? 'blink' : ''}>
          <td className="pname"> <span>{name}</span> </td>
          <td>  {  this.numberFormat(price) } </td>
          <td>   { qty } </td>
          <td> <span className="textprice">  {this.numberFormat(discount)} </span> </td>
          <td> <span className="textprice">  {this.numberFormat(updatePrice)} </span> </td>
        </tr>
      )
    })
  }
  gotoPayament = () => {
    
       this.setState({
        chargeshow :true,
        orderview : false 
      })
  } 

  complete = () => {
    this.setState(
      {
         complete : true,
         chargeshow :false,
         orderview : false,
      }
    )
  }

  editOrder = () => {
    this.props.history.push(`/app/retail/checkout`);
  }

  cancelOrder = () => {
     let ee =  this
    confirm({
      title: 'Are you sure cancel this Order.',
      icon: <ExclamationCircleOutlined />,
      content: '',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        ee.props.history.goBack();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  paymentType = (t) => {
    this.setState({
      paymentType : t
    })
}

receipt = () => {
  this.setState({
    receipt : true
  })
}

closereceipt = () =>{
  this.setState({
     receipt : false
    }) 
}


newSale =() => {
  this.setState({
    complete: false,
    orderview:true,
    chargeshow :false
  })
}

addcustomers =() => {
  this.props.history.push("/app/retail/customer/new");
 }

  render() {
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;


    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>

        <Row gutter={[25, 25]}>
          <Col span={12}>
            <Title level={4}>  <span onClick={this.goback}> Order </span> /  Feb 14, 2020, 5:00 PM </Title>
          </Col>
          <Col span={12} className="rightButton">
             <Button type="primary" className="primarybutton" onClick={this.addcustomers} > Add Customer </Button>
          </Col>
        </Row>

        <Row gutter={[50, 50]}>
         {this.state.orderview ?  <Col span={24} className="pepar">

            <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >

              <Row gutter={[20, 20]}>
                <Col span={24}>
                  <Col span={12}>
                    <Title level={4} className="margin2">#20</Title>
                    <Title level={4} className="margin2">confirmed</Title>
                  </Col>
                  <Col span={12}>
                    <Title level={4} className="margin2">$500.00</Title>
                    <Title level={4} className="margin2">By Seller Name</Title>
                    <Title level={4} className="margin2">Feb 14, 2020, 5:00 PM</Title>
                  </Col>
                  <Col span={24}>
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.tabcallback}>
                      <TabPane tab="Items " key="1">
                        <Text className="strong">  Total Items: 5 </Text>
                        <table className="orders">
                          <thead>
                            <tr key="2">
                              <th> Item </th>
                              <th> Price </th>
                              <th> Quantity </th>
                              <th> Discount</th>
                              <th> Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.renderItemlistReceipt()}
                            <tr key="44" style={{ background: "#ccc" }}>
                              <th> </th>
                              <th></th>
                              <th >  </th>
                             <th> {this.numberFormat(0)}</th>
                              <th> {this.numberFormat(670)}</th>
                            </tr>
                          </tbody>
                        </table>


                        <div className="ttotal">
                            <table className="orderstotal">
                                 <tr key="2">
                                    <th> Subtotal : &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(760)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th>  Product Discount  : &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(0)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th> Bulk Discount  : &nbsp; </th>
                                    <th> &nbsp; {this.numberFormat(0)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th> Total :  &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(760)}</th>
                                   </tr>
                                </table>  
                            </div>
                      </TabPane>
                      <TabPane tab="Details" key="2">
                        <div className="centerbox">
                          <Title level={2} className=""> Customer Name </Title>

                          <Title level={4} className="margin2">  <PhoneIphoneIcon />  Contact Number </Title>
                          <Title level={4} className="margin2"> <LocationOnIcon />  Address </Title>
                          <Title level={4} className="margin2"> &nbsp; </Title>
                          <Timeline >
                            <Timeline.Item> Pending 2015-09-01</Timeline.Item>
                            <Timeline.Item>Confirm  2015-09-01</Timeline.Item>
                            <Timeline.Item> <p> Subtotal : 670 </p></Timeline.Item>
                            <Timeline.Item>Total : 670</Timeline.Item>
                          </Timeline>
                        </div>
                      </TabPane>

                    </Tabs>
                  </Col>
                </Col>
                <Col>
                </Col> 
                    

              </Row>

              <Form.Item >
                <Button type="default" onClick={this.goback}>
                  Go Back
                  </Button>
                  &nbsp;
                  <Button type="primary" onClick={this.gotoPayament}>
                  Go To Payment
               </Button>
               &nbsp;
               <Button type="primary" onClick={this.editOrder}>
                  Edit Order
               </Button>
               &nbsp;
                  <Button type="primary" onClick={this.cancelOrder} >
                  Cancel Order
               </Button>
              </Form.Item>
            </Form>
          </Col>
          : ''}
       

        {this.state.chargeshow ? <Col span={24} className="chargeOptions">
                     
              
                     <Title level={2} className="center mgb-50"> {this.numberFormat(670)}</Title> 
                             
                                {/* <Col  span={24} className="center mgb-50">
                                         <Button onClick={this.saveOrder}>Save Order </Button> <Button> Pay Later </Button>
                                </Col> */}
     
                                <Col xs={24} className="center paymentoptions">
                                       <div className={`Paymentoption one ${ this.state.paymentType == 'Cash'? 'active':''}`} onClick={()=>this.paymentType('Cash')}>
                                                       <LocalAtmIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Cash </Text>
                                       </div>
                                       <div className={`Paymentoption two ${ this.state.paymentType == 'Debit Card'? 'active':''}`} onClick={()=>this.paymentType('Debit Card')}>
                                                       <CreditCardIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Debit Card </Text>
                                       </div>
                                       <div className={`Paymentoption three ${ this.state.paymentType == 'Credit Card'? 'active':''}`} onClick={()=>this.paymentType('Credit Card')}>
                                                       <HorizontalSplitIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Credit Card </Text>
                                       </div>
                                       <div className={`Paymentoption four ${ this.state.paymentType == 'Store Credit'? 'active':''}`} onClick={()=>this.paymentType('Store Credit')}>
                                                       <AccountBalanceWalletIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Store Credit </Text>
                                       </div>
                                       <div className={`Paymentoption five ${ this.state.paymentType == 'Check'? 'active':''}`} onClick={()=>this.paymentType('Check')}>
                                                       <MoneyIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Check </Text>
                                       </div>
                                       <div className={`Paymentoption six ${ this.state.paymentType == 'Other'? 'active':''}`} onClick={()=>this.paymentType('Other')}>
                                                       <SubtitlesIcon/> 
                                                       <br/>
                                                       <Text className="paymenttype"> Other </Text>
                                       </div>
                                       <div className="mgb-50"/>
                                       <button className="retailOrder" onClick={this.complete}> Next </button>
                                </Col>
     
                     </Col> : ''}

                     {this.state.complete ? <Col span={24} className="chargeOptions">
                     
              
              
                        
                     <Col span={24} className="center completeMasr">
                          <CheckCircleOutlineIcon className="completemark"/>
                     </Col>

                     <Title level={2} className="center mgb-50"> {this.numberFormat(670)}</Title> 
                    
                     <Col xs={24} className="center paymentoptions">
                            <button className="receiptOrder retailOrder" onClick={()=>this.receipt()}> <ReceiptIcon className="receipticonb"/> Receipt </button>
                            <button className="retailOrder" onClick={()=> this.goback()}> Start New Order </button>
                     </Col>

          </Col> : ''}

          <Col span={24} className={`receipt ${this.state.receipt ? 'show':'hide'}`}>
                     
                            <Col span={12} className="relogo">
                              <img src={logo}/>
                           </Col>
                           <Col span={12} className="renumber">
                           <CancelIcon className="closerb" onClick={this.closereceipt}/>
                               #13 <br/>
                               Customer Name <br/>
                               Address  <br/>
                               Contact No.  <br />
                               Date   

                          
                           </Col>
                            <Col span={24} className="details">
                               <Title>Test Store </Title>
                               <Text>Address City State Country | 9856321470</Text>
                           </Col>
                            <Col span={24} className="orderItems">
                              <Text className="strong">  Total Items: 5 </Text>
                            <table className="orders">
                              <thead>
                                <tr key="2">
                                  <th> Item </th>
                                  <th> Price </th>
                                  <th> Quantity </th>
                                   <th> Discount</th>
                                  <th> Amount</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.renderItemlistReceipt()}
                                <tr key="44" style={{background:"#ccc"}}>
                                  <th> </th>
                                  <th>  </th>
                                  <th> </th>
                                  <th> { this.numberFormat(0)}</th>
                                  <th> { this.numberFormat(670)}</th>
                                </tr>
                              </tbody>
                            </table>
                            

                            <div className="ttotal">
                            <table className="orderstotal">
                                 <tr key="2">
                                    <th> Subtotal : &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(760)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th>  Product Discount  : &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(0)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th> Bulk Discount  : &nbsp; </th>
                                    <th> &nbsp; {this.numberFormat(0)}</th>
                                   </tr>
                                   <tr key="2">
                                    <th> Total :  &nbsp; </th>
                                    <th>&nbsp;{this.numberFormat(760)}</th>
                                   </tr>
                                </table>  
                            </div>
                            </Col>
                            <Col md={12} xs={24} className="center shate">

                                <Col span={4}> 
                                <PictureAsPdfIcon />
                                
                                </Col>
                                <Col span={4}> 
                              
                                <SendIcon />
                              
                                </Col>
                                <Col span={4}> 
                               
                                <PrintIcon /> 
                                
                                </Col>
                                <Col span={4}> 
                               
                                <ShareIcon /> 
                                </Col>
                                <Col span={4}  onClick={this.closereceipt}> 
                               
                                   <CancelPresentationIcon /> 
                               </Col>
                                
                            </Col>
                            
                </Col>
          </Row>
      </div>
    );
  }
}
export default Form.create()(EditProduct);