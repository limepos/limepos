import React from "react";

var ModeStateContext = React.createContext();
var ModeDispatchContext = React.createContext();

function modeReducer(state, action) {
    switch (action.type) {
      case "RETAILE_MODE":
        return { ...state, isMode: 'retaile_mode' };
      case "KITCHEN_MODE":
        return { ...state, isMode: 'kitchen_mode' };
      case "CASHIER_MODE":
          return { ...state, isMode: 'cashier_mode'  };
      case "WAITER_MODE":
         return { ...state, isMode: 'waiter_mode'  };
      default: {
        throw new Error(`Unhandled action type: ${action.type}`);
      }
    }
  }


  function ModeProvider({ children }) {
    var [state, dispatch] = React.useReducer(modeReducer, {
        isMode: localStorage.getItem('currentMode'),
    });
    return (
      <ModeStateContext.Provider value={state}>
        <ModeDispatchContext.Provider value={dispatch}>
          {children}
        </ModeDispatchContext.Provider>
      </ModeStateContext.Provider>
    );
  }
  
  function useModeState() {
    var context = React.useContext(ModeStateContext);
    if (context === undefined) {
      throw new Error("useLayoutState must be used within a LayoutProvider");
    }
    return context;
  }
  
  function useModeDispatch() {
    var context = React.useContext(ModeDispatchContext);
    if (context === undefined) {
      throw new Error("useLayoutDispatch must be used within a LayoutProvider");
    }
    return context;
  }
  export { ModeProvider, checkMode, useModeState, useModeDispatch };

  function checkMode( modeValue, dispatch, history) {

    console.log("modeValue", modeValue)

    if(modeValue === 'retaile_mode'){
        localStorage.setItem('currentMode', "retaile_mode")
         dispatch({
            type: "RETAILE_MODE",
          });

          history.push('/app/dashboard')

    }else if (modeValue === 'kitchen_mode'){
        localStorage.setItem('currentMode', "kitchen_mode")
        dispatch({
            type: "KITCHEN_MODE",
          });
          history.push('/app/dashboard')

    }else if(modeValue === 'waiter_mode'){
        localStorage.setItem('currentMode', "waiter_mode")

        dispatch({
            type: "WAITER_MODE",
          });
         history.push('/app/dashboard')
    }
    else if(modeValue === 'cashier_mode'){
        localStorage.setItem('currentMode', "cashier_mode")
        dispatch({
            type: "CASHIER_MODE",
          });

         history.push('/app/dashboard')

    }
 
  }
  