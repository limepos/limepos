import React from "react";
//var axios = require('../service/api');


var UserStateContext = React.createContext();
var UserDispatchContext = React.createContext();

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return { ...state, isAuthenticated: true, role : 'admin' };
    case "SIGN_OUT_SUCCESS":
      return { ...state, isAuthenticated: false };
   case "LOGIN_FAILURE":
        return { ...state, isAuthenticated: false };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function UserProvider({ children }) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: !!localStorage.getItem("id_token"),
  });

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

function useUserState() {
  var context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

function useUserDispatch() {
  var context = React.useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error("useUserDispatch must be used within a UserProvider");
  }
  return context;
}

export { UserProvider, useUserState, useUserDispatch, loginUser, RegisterUser, signOut, movetologin, back };

// ###########################################################

function loginUser(dispatch, login, password, history, setIsLoading, setError) {

  setError(false);
  setIsLoading(true);

  //   var obj = { 
  //     password : password,
  //     email : login 
  //   }
    
  // axios.post('/login', obj)
  // .then(res => {
  //          // console.log("userdata", res.data.data.length)
  //          if(res.data.data.length > 0){
  //           localStorage.setItem('id_token', 1)
  //           setError(null)
  //           setIsLoading(false)
  //           dispatch({ type: 'LOGIN_SUCCESS' })
      
  //           history.push('/mode')
  //          } else {
  //           setTimeout(() => {
  //           dispatch({ type: "LOGIN_FAILURE" });
  //           setError(true);
  //           setIsLoading(false);
           
  //         }, 2000);
  //          }
  //         }
  //   );

     //--------Without api login ----------

  if (!!login && !!password) {
    setTimeout(() => {
      localStorage.setItem('id_token', 1)
      setError(null)
      setIsLoading(false)
      dispatch({ type: 'LOGIN_SUCCESS' })
      //console.log(fnameValue,lnameValue,login,password)
      history.push('/mode')
    }, 2000);
  } else {
    dispatch({ type: "LOGIN_FAILURE" });
    setError(true);
    setIsLoading(false);
  }
}

function RegisterUser(dispatch, login, fnameValue, lnameValue, password, history, setIsLoading, setError) {
  setError(false);
  setIsLoading(true);

  //   var obj = { 

  //        firstname :  fnameValue,
  //        lastname : lnameValue,
  //        password : password,
  //        email : login 
  //   }

  // axios.post('/register', obj)
  //       .then(res => console.log(res.data));
  if (!!login && !!password) {
    setTimeout(() => {
      localStorage.setItem('id_token', 1)
      setError(null)
      setIsLoading(false)
      dispatch({ type: 'LOGIN_SUCCESS' })


    //  console.log(fnameValue,lnameValue,login,password)

    history.push('/mode')
    }, 2000);
  } else {
    dispatch({ type: "LOGIN_FAILURE" });
    setError(true);
    setIsLoading(false);
  }
}


function signOut(dispatch, history) {
  localStorage.removeItem("id_token");
  dispatch({ type: "SIGN_OUT_SUCCESS" });
  history.push("/account");
}

function movetologin ( url ,history) {
  history.push(url)
}

function back(history){
  history.push("/account")
}

// function handleChange (event) {
  
//   console.log();
//   // const { formData } = this.state;
//   // formData[event.target.name] = event.target.value;
//   // this.setState({ formData });
// }