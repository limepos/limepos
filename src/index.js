import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "@material-ui/styles";
import { CssBaseline } from "@material-ui/core";

import Themes from "./themes";
import App from "./components/App";
import * as serviceWorker from "./serviceWorker";
import { LayoutProvider } from "./context/LayoutContext";
import { UserProvider } from "./context/UserContext";
import { ModeProvider } from "./context/ModeContext";
import { message } from 'antd';
import "./components/style/antd.css";
import "./components/style/mystyle.css";
ReactDOM.render(
  <LayoutProvider>
    <UserProvider>
      <ModeProvider>
       <ThemeProvider theme={Themes.default}>
        <CssBaseline />
        <App />
      </ThemeProvider>
      </ModeProvider> 
    </UserProvider>
  </LayoutProvider>,
  document.getElementById("root"),
);
message.config({
  top: 75,
  duration: 2,
  maxCount: 3,
});
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
