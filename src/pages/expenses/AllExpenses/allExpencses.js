import React from 'react';
import { Table, Input, DatePicker, Modal, Icon } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
const { Search } = Input;
const { RangePicker} = DatePicker;

const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    date: `Feb 14, 2020, 5:00 PM`,
    amount : '₹50.00',
    note: `8871365420`,
    user: `Fulfilled`,
    action : `₹100.00`
  });
}



class AllExpenses extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date'
      },
      {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
      },
      {
        title: 'Notes',
        dataIndex: 'note',
        key: 'note',
      },
      {
        title: 'User Status',
        dataIndex: 'user',
        key: 'user',
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.deleteexpences(record.key) }> <Icon type="close" /> </a>
          </span>
        ),
      }
  
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
     

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

   changeDate = (date, dateString) =>{
    console.log(date, dateString);
  }

  deleteexpences =() =>{

    confirm({
      title: 'Confirm Cancellation',
      content: 'Are you sure you want to cancel this expense?',
      okText: 'Cancel Expenses',
      okType: 'danger',
      cancelText: 'Back',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });

  }


  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    
                     <RangePicker onChange={this.changeDate} className="searchBox " />
                     <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" /> 
                  
                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table  columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (AllExpenses);