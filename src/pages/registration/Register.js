import React, { useState } from "react";
import {
  Grid,
  CircularProgress,
  Typography,
  TextField,
  Fade,
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { Button} from 'antd';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
// styles
import useStyles from "./styles";
// logo
import logo from "../../images/logo.png";
// context
import { useUserDispatch, RegisterUser, back } from "../../context/UserContext";

function Register(props) {
  var classes = useStyles();

  // global
  var userDispatch = useUserDispatch();

  // local
  var [isLoading, setIsLoading] = useState(false);
  var [error, setError] = useState(null);
  var [fnameValue, setfNameValue] = useState("");
  var [lnameValue, setlNameValue] = useState("");
  var [loginValue, setLoginValue] = useState("");
  var [passwordValue, setPasswordValue] = useState("");

  return (
    <Grid container className={classes.container}>
      <div className={classes.logotypeContainer}>
        <img src={logo} alt="logo" className={classes.logotypeImage} />
      </div>
      <div className={classes.formContainer}>

      <div className="back" onClick={() =>
                      back(props.history) }> <ArrowBackIcon/></div>
        <div className={classes.form}>
      
         
              <Typography variant="h3" className={classes.subGreeting}>
                Create your account
              </Typography>
              <Fade in={error}>
                <Typography color="secondary" className={classes.errorMessage}>
                  Something is wrong with your login or password..!
                </Typography>
              </Fade>
              <ValidatorForm>    
              <TextValidator
                id="fname"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={fnameValue}
                onChange={e => setfNameValue(e.target.value)}
                margin="normal"
                placeholder="First Name"
                type="text"
                fullWidth
                validators={['required']}
                errorMessages={['Please Enter First Name']}
              />

              <TextValidator
                id="lname"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={lnameValue}
                onChange={e => setlNameValue(e.target.value)}
                margin="normal"
                placeholder="Last Name"
                type="text"
                fullWidth
                validators={['required']}
                errorMessages={['Please Enter Last Name']}
              />
             <TextValidator
                id="email"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={loginValue}
                onChange={e => setLoginValue(e.target.value)}
                margin="normal"
                placeholder="Email Address"
                type="email"
                fullWidth
                validators={['required', 'isEmail']}
                errorMessages={['Please enter email.', 'email is not valid']}
              />
              <TextField
                id="password"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={passwordValue}
                onChange={e => setPasswordValue(e.target.value)}
                margin="normal"
                placeholder="Password"
                type="password"
                fullWidth
                validators={['required']}
                errorMessages={['this field is required']}
              />
              <div className={classes.creatingButtonContainer}>
                {isLoading ? (
                  <CircularProgress size={26} />
                ) : (
                  <Button
                  block
                    onClick={() =>
                      RegisterUser(
                        userDispatch,
                        loginValue,
                        fnameValue,
                        lnameValue,
                        passwordValue,
                        props.history,
                        setIsLoading,
                        setError,
                      )
                      
                    }
                   
                    size="large"
                    variant="contained"
                    fullWidth
                    className ="logintransparent"
                  >
                    Create your account
                  </Button>
                )}
              </div>
              
              </ValidatorForm>
        </div>
      </div>
    </Grid>
  );
}

export default withRouter(Register);
