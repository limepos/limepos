import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({

  paper: {
    marginBottom: theme.spacing(2),
  },
}));
