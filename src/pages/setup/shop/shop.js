import React from 'react';
import {
  Form,
  Input,
  Tooltip,
  Icon,
  Select,
  Row,
  Col,
  Button,
  Typography,
  Upload,
  message 
   
} from 'antd';
import dummy from "../../../images/logo_dummy.png";
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;


//  file upload...

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}
 

class Shop extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  render() {
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;
  
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Setup Your Shop</Title>
           <Text>Your shop details and settings</Text>

        </Col>
        <Col span={16} className="pepar">

        <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={this.handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : <img src={dummy} alt="avatar" style={{ width: '100%' }} />}
      </Upload>   
       <Text> Upload your logo. It should be a square image.</Text>
       <br/>
       <Text> &nbsp; </Text>
       
      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Shop Name" {...formItemLayout}>
            {getFieldDecorator('shopname', {
            rules: [
              {
                required: true,
                message: 'Please input Shop Name',
              },
            ],
          })(<Input  placeholder="Shop Name" />)}
        </Form.Item>
        <Form.Item label="Business Type" {...formItemLayout}>
            <Select nstyle={{ width: '100%' }} >
            <Option value="rmb">Food and Drink </Option>
            <Option value="dollar">Retail</Option>
          </Select>
        </Form.Item>
        
        <Form.Item label="City" hasFeedback>
          {getFieldDecorator('city', {
            rules: [
              {
                required: true,
                message: 'Please Enter City',
              }
            ],
          })(<Input placeholder="City" />)}
        </Form.Item>
        <Form.Item
          label={
            <span>
              Shop Owner PIN &nbsp;
              <Tooltip title="Setting shop owner PIN allows you to add cashiers and enable locking and unlocking registers with a numeric PIN.">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          }
        ><Input placeholder="4 to 6 digit owner PIN Eg: 1234"/>
        </Form.Item>

        <Form.Item >
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col> 

    <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Your Account Details</Title>
           <Text>Your details help us serve you better.</Text>

        </Col>
        <Col span={16} className="pepar">



      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
         <Form.Item label="Email" {...formItemLayout}>
           <Input  placeholder="Email" value="Demo@email.com" readOnly />
        </Form.Item>
        <Form.Item label="Your Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please input Name',
              },
            ],
          })(<Input  placeholder="Your Name" />)}
        </Form.Item>
        <Form.Item label="Contact Number" hasFeedback>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: 'Please Enter Contact Noumber.',
              }
            ],
          })(<Input placeholder="Contact Number " />)}
        </Form.Item>
      
        <Form.Item >
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col> 
      </Row>
    </div>
    );
  }
}
export default Form.create()(Shop);