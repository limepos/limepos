import React from 'react';
import { Table, Input, Typography, Button, message, Row, Col} from 'antd';
 
  import tableSearch from '../../../../components/TableSearch/tableSearch';

import Grid from '@material-ui/core/Grid';
const { Title } = Typography;
const { Search } = Input;

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    name: `Main Register ${i}`,
    prefix: 	'DE',
    print: `Yes`,
  });
}

class Register extends React.Component {

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    searchText: '',
    searchedColumn: '',
    columns : [
      {
        title: 'Register Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.editRegister(record.key) }> {record.name} </a>
          </span>
        ),
      },
      {
        title: 'Receipt Number Prefix',
        dataIndex: 'prefix',
      },
      {
        title: 'Print Receipts? (for LimePOS Web)',
        dataIndex: 'print',
      }
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
      constructor(props) {
        super(props);
      }
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  addnewRegister = () => {
   
    this.props.history.push("/app/setup/register/new-register");
  }
   
  editRegister = (id) => {
   
    this.props.history.push(`/app/setup/register/new-register/${id}`);
  }
  
  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  
  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    return (<div>
           <Grid container spacing={3}>
             <Grid item xs={6} className="pageTitle">
                <Title level={4}> Registers </Title>  
            </Grid>
            <Grid item xs={6} className="rightButton">
               <Button type="primary" onClick={this.addnewRegister}  className="primarybutton">Add Register </Button>
            </Grid>
          </Grid>  
        <Row gutter={[50, 50]}>
         <Col span={24}>
           <Col span={24} className="pepar">
           <div style={{ marginBottom: 16 }}>
          <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
              { this.state.actiontitle }
          </Button>
          <Search
            placeholder="input search text"
            onChange={e => this.finder(e) }
             className="searchBox"
            />
          
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
           <Table rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.displaybookdata} />
         </Col>
         </Col>
        </Row>
        
    </div>)
  }
}
export default (Register);