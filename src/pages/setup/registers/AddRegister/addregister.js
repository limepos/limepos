import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
 
  Typography,

} from 'antd';

const { TextArea } = Input;
const { Text } = Typography;
const { Title } = Typography;


//  file upload...
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}


 

class Addregister extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };
  render() {
    const { value } = this.state;
    const { getFieldDecorator } = this.props.form;
    
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Registers </span>  / Add New Register </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Your Register Details</Title>
           <Text>Enable receipt printing to print receipts while billing with this register.</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>By default, The shop name will be printed on the receipt.</Text>

        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Register Name" {...formItemLayout}>
            {getFieldDecorator('registername', {
            rules: [
              {
                required: true,
                message: 'Please enter Register Name',
              },
            ],
          })(<Input  placeholder="Register Name" />)}
        </Form.Item>

        <Form.Item label="Receipt Number Prefix" {...formItemLayout}>
            {getFieldDecorator('prefix', {
            rules: [
              {
                required: true,
                message: 'Please enter receipt prefix',
              },
            ],
          })(<Input  placeholder="Receipt Number Prefix" />)}
        </Form.Item>
      
        <Form.Item label="Bill Header" hasFeedback>
          {getFieldDecorator('billheader', {
            rules: [
              {
                required: true,
                message: 'Please enter Bill header',
              }
            ],
          })(<TextArea
            value={value}
            onChange={this.onChange}
            placeholder="Controlled autosize"
            autoSize={{ minRows: 3, maxRows: 5 }}
          />)}
        </Form.Item>

        <Form.Item label="Bill Footer" hasFeedback>
          {getFieldDecorator('billfooter', {
            rules: [
              {
                required: true,
                message: 'Please enter Bill Footer',
              }
            ],
          })(<TextArea
            value={value}
            onChange={this.onChange}
            placeholder="Controlled autosize"
            autoSize={{ minRows: 3, maxRows: 5 }}
          />)}
        </Form.Item>

        <Form.Item label="Table Numbers" hasFeedback>
        <TextArea
            value="1-15"
          //  onChange={this.onChange}
            placeholder="Controlled autosize"
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
        <Form.Item >
        
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
           
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addregister);