import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  Typography,
} from 'antd';
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;


class Addcashier extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  onChangeRegister = (value) => {
    console.log(`selected ${value}`);
  }

  permission = () => {

    console.log(`selected permission`);
    
  }

  render() {

    const { getFieldDecorator } = this.props.form;


    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Users </span> / New Cashier </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Cashier Details</Title>
           <Text>Cashiers have access only to billing. Cashier will use PIN to lock and unlock their register.</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>Once you have cashiers, LimePOS will promot for a unlock PIN when you sign in.</Text>

        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Cashier Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter textname',
              },
            ],
          })(<Input  placeholder="Cashier Name" />)}
        </Form.Item>

        <Form.Item label="Cashier PIN" {...formItemLayout}>
            {getFieldDecorator('pin', {
            rules: [
              {
                required: true,
                message: 'PIN should be a 4 to 6 digit number.',
              },
            ],
          })(<Input  placeholder="Please Enter PIN" />)}
        </Form.Item> 

        <Form.Item label="Cashier Register" {...formItemLayout}>
            {getFieldDecorator('register', {
            rules: [
              {
                required: true,
                message: 'A register must be selected for the cashier.',
              },
            ],
          })( <Select
            showSearch
            placeholder="Select a person"
            optionFilterProp="children"
            onChange={this.onChangeRegister}
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="tom">Tom</Option>
          </Select>)}
        </Form.Item> 
        <Form.Item> 
        <Checkbox onChange={this.permission}>Allow manager permissions</Checkbox>
        </Form.Item> 
        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addcashier);