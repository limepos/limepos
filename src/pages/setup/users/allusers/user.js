import React from 'react';
import { Typography, Button, Tabs,Row, Col  } from 'antd';
import Grid from '@material-ui/core/Grid';

import Appuser from './appuser/appuser';
import Cashier from './cashiers/cashiers';
import Kitchenuser from './kitchenuser/kitchenusers';
import Waiters from './waiters/waiters';

const { Title } = Typography;
const { TabPane } = Tabs;

class Register extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  const currantbackurl = this.props.history.location.pathname;

    if( currantbackurl === '/app/setup/users/1'){
      
      this.state.current_tab = '1';
    
      console.log("current tab", this.state.current_tab)

    } 
    if( currantbackurl === '/app/setup/users/2'){
      this.state.current_tab = '2';
      console.log("current tab", this.state.current_tab)
      
    }

    if(currantbackurl === '/app/setup/users/3'){
      this.state.current_tab = '3';

      console.log("current tab", this.state.current_tab)
    
    }
    if(currantbackurl === '/app/setup/users/4'){
      this.state.current_tab = '4';
      console.log("current tab", this.state.current_tab)
    
    }
   
  }

  state = {
     current_tab: '1',
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })
    if( key === 1){
      this.props.history.push("/app/setup/users/1");
      console.log(key)
    }else if( key === 2){
      this.props.history.push("/app/setup/users/2");
      console.log(key)
    }else if( key === 3){
      this.props.history.push("/app/setup/users/3");
      console.log(key)
    }else if( key === 4){
      this.props.history.push("/app/setup/users/4");
      console.log(key)
    }
  }
   
  addnewRegister = (tab) => {

    console.log("===", tab)

    if(tab === 1){
     this.props.history.push("/app/setup/user/new-cashier");
    }
     else if(tab === 2){
      this.props.history.push("/app/setup/user/new-appuser");
     }
     else if(tab === 3){
      this.props.history.push("/app/setup/user/new-waiter");
     }
     else if(tab === 4){
      this.props.history.push("/app/setup/user/new-kitchenuser");
     }
     
  }
  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Users </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                   { this.state.current_tab === 1 ? <Button type="primary" onClick={() =>  this.addnewRegister(1)}  className="primarybutton">Add Cashier </Button> : '' }
                   { this.state.current_tab === 2 ? <Button type="primary" onClick={() =>  this.addnewRegister(2)}  className="primarybutton">Add App User </Button> : '' }
                   { this.state.current_tab === 3 ? <Button type="primary" onClick={() =>  this.addnewRegister(3)}  className="primarybutton">Add Waiter </Button> : '' }
                   { this.state.current_tab === 4 ? <Button type="primary" onClick={() =>  this.addnewRegister(4)}  className="primarybutton">Add Kitchen User </Button> : '' }
                 </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                      <TabPane tab="All Cashiers"  key="1">
                        <Cashier history={this.props} />
                      </TabPane>
                      <TabPane tab="All App Users" key="2">
                        <Appuser history={this.props} />
                      </TabPane>
                      <TabPane tab="All Waiters" key="3">
                          <Waiters history={this.props} />
                      </TabPane>
                      <TabPane tab="All kitchen Users" key="4">
                          <Kitchenuser history={this.props} />
                      </TabPane>
                    </Tabs>
                  </Col>
                  </Col>
              </Row>    
            </div>
    )
  }
}
export default (Register);