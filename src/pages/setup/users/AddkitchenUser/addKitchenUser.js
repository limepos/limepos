import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Typography
} from 'antd';

const { Title,Text } = Typography;
const { Option } = Select;


class Addkitchenuser extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  onChangeRegister = (value) => {
    console.log(`selected ${value}`);
  }


  render() {
   
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Users </span> /  kitchenuser </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Kitchen User Details</Title>
           <Text>Kitchen users have access only to LimePOS Kitchen Display System (KDS) App.</Text>


        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Kitchen User Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter kitchen user name',
              },
            ],
          })(<Input  placeholder="Kitchen User Name" />)}
        </Form.Item>

        <Form.Item label="Kitchen User PIN" {...formItemLayout}>
            {getFieldDecorator('pin', {
            rules: [
              {
                required: true,
                message: 'PIN should be a 4 to 6 digit number.',
              },
            ],
          })(<Input  placeholder="Please Enter PIN" />)}
        </Form.Item> 

        <Form.Item label="Kitchen User Register" {...formItemLayout}>
            {getFieldDecorator('register', {
            rules: [
              {
                required: true,
                message: 'A register must be selected for the Kitchen user',
              },
            ],
          })( <Select
            showSearch
            placeholder="Select a Register"
            optionFilterProp="children"
            onChange={this.onChangeRegister}
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="tom">Tom</Option>
          </Select>)}
        </Form.Item> 
        
        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addkitchenuser);