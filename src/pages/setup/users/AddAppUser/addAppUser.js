import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Typography,
} from 'antd';
const { Text } = Typography;
const { Title } = Typography;

class Addappuser extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

  onChangeRegister = (value) => {
    console.log(`selected ${value}`);
  }

  permission = () => {

    console.log(`selected permission`);
    
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Users </span> / New App User </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>App User Details</Title>
           <Text>App users have access only to LimePOS Apps. An app user will require a PIN to lock and unlock the application.</Text>
           <div style={{ margin: '24px 0' }} />


        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="App User Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter Name',
              },
            ],
          })(<Input  placeholder="App User Name" />)}
        </Form.Item>

        <Form.Item label="App User PIN" {...formItemLayout}>
            {getFieldDecorator('pin', {
            rules: [
              {
                required: true,
                message: 'PIN should be a 4 to 6 digit number.',
              },
            ],
          })(<Input  placeholder="Please Enter PIN" />)}
        </Form.Item> 
        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addappuser);
