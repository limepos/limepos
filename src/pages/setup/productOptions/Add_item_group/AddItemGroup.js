import React from 'react';
import {
  Form,
  Input,
  Icon,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  Typography
} from 'antd';
const { Text,Title } = Typography;
const { Option } = Select;


const defaultCheckedList = ['Apple', 'Orange'];


//  file upload...
const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function handleChange(value) {
  console.log(`selected ${value}`);
}

class AddItemGroup extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
    checkedList: defaultCheckedList,
    indeterminate: true,
    checkAll: false,
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };
  
 
  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Product Options  </span> / New Item Group </Title>  
          </Col>
        </Row>

      <Row gutter={[20, 20]}>
      <Col span={24}>
        <Col span={8}>
           <Title level={4}>Setup Item Group</Title>
           <Text>Create an item group that can be attached to a combo.</Text>
         </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Item Group Name" {...formItemLayout}>
            {getFieldDecorator('registername', {
            rules: [
              {
                required: true,
                message: 'Please enter Addon Group Name',
              },
            ],
          })(<Input  placeholder="Addon Group Name" />)}
      </Form.Item>
      <Form.Item label="Select Product" {...formItemLayout}>
      <Input
      placeholder="Search Product"
      prefix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)' }} />}
      onSearch={value => console.log(value)}
      style={{ width: '75%' }}
    />
          <Select
          mode="multiple"
          size="default"
          style={{ width: '100%' }}
          placeholder="Category"
          defaultValue={['a10', 'c12']}
          onChange={handleChange}
          style={{ width: '25%' }}
          >
          {children}
          </Select>
      </Form.Item>
      <Form.Item label="Select the products">
          {getFieldDecorator('checkbox-group', {
            initialValue: ['A', 'B'],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={6}>
                  <Checkbox value="A"> Addon 1</Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="B">
                  Addon 2
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="C"> Addon 3 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="D"> Addon 4  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="E"> Addon 5 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="f"> Addon 1</Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="g">
                  Addon 2
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="h"> Addon 3 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="i"> Addon 4  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="j"> Addon 5 </Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>
          )}
        </Form.Item>
        <Col span={24}>
        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
        </Col>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(AddItemGroup);
