import React from 'react';
import {
  Form,
  Input,
  Icon,
  Row,
  Col,
  Button,
  Typography,
} from 'antd';

let id = 0;

const { Text } = Typography;
const { Title } = Typography;

class AddNewAddon extends React.Component {

  state = {
    value: '',
  };

  constructor(props) {
    super(props);
  }
 
  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
     form.setFieldsValue({
       keys: nextKeys,
      });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { keys, names } = values;
        console.log('Received values of form: ', values);
        console.log('Merged values:', keys.map(key => names[key]));
      }
    });
  };

  render() {

    
    const { goBack } = this.props.history;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };

     getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <Row gutter={[10,10]}> 
      <Col span={7}> 
          <Form.Item
          {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
          required={false}
          key={k}
        >
          {getFieldDecorator(`varianttitle[${k}]`, {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Please Addon Name",
              },
            ],
          })(<Input placeholder="Addon Name" />)}
        </Form.Item> 
     </Col>
      <Col span={7}>
        <Form.Item
        {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        required={false}
        key={k}
      >
        {getFieldDecorator(`price[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
              required: true,
              whitespace: false,
              message: "Please enter price.",
            },
          ],
        })(<Input placeholder="Price"  />)}
      </Form.Item>
      </Col>
      <Col span={7}> <Form.Item
      {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
      required={false}
      key={k}
    >
      {getFieldDecorator(`sortorder[${k}]`, {
        validateTrigger: ['onChange', 'onBlur']
      })(<Input placeholder="Sort Order" />)}
    </Form.Item> </Col>
      <Col span={3}> 
      {keys.length > 1 ? (
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => this.remove(k)}
        />
      ) : null}
   </Col>
   </Row>    
    ));
   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}>  <span onClick={ () => goBack("ok") }>  Product Options </span>  / New Addon </Title>  
          </Col>
        </Row>

       <Row gutter={[50, 50]}>
         <Col span={24}>
          <Form onSubmit={this.handleSubmit}>
          <Row gutter={[10,10]}> 
            <Col span={7}><Text strong> Addon Name </Text></Col>
            <Col span={7}><Text strong> Price </Text></Col>
            <Col span={7}><Text strong> Sort Order </Text></Col>
          </Row>  
           {formItems}
        <Form.Item {...formItemLayoutWithOutLabel}>
          <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
            <Icon type="plus" /> Add field
          </Button>
        </Form.Item>
        <Form.Item {...formItemLayoutWithOutLabel}>
         <Button type="default" onClick={() => goBack() }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
        </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(AddNewAddon);