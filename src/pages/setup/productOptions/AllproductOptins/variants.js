import React from 'react';
import { Table, Divider, Input, Typography, Button, message  } from 'antd';

import Grid from '@material-ui/core/Grid';
const { Title } = Typography;
const { Search } = Input;
const columns = [
  {
    title: 'Register Name',
    dataIndex: 'name',
  },
  {
    title: 'Receipt Number Prefix',
    dataIndex: 'prefix',
  },
  {
    title: 'Print Receipts? (for LimePOS Web)',
    dataIndex: 'print',
  },
];

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    name: `Main Register ${i}`,
    prefix: 	'DE',
    print: `Yes`,
  });
}

function handleButtonClick(e) {
  message.info('Click on left button.');
  console.log('click left button', e);
}


class Variant extends React.Component {



  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action"
  };
      constructor(props) {
        super(props);
      }
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  addnewRegister = () => {
   
    this.props.history.push("/app/setup/register/new-register");
  }

  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                     { this.state.actiontitle }
                    </Button>
                    <Search placeholder="input search text" onSearch={value => console.log(value)} className="searchBox" />

                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
            </div>
    )
  }
}
export default (Variant);