import React from 'react';
import { Table, Input, Button, message  } from 'antd';
import tableSearch from '../../../../../components/TableSearch/tableSearch';
const { Search } = Input;
const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    name: `Name ${i}`,
    item: `2`,
  });
}

class Itemgroups extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
      {
        title: 'Item Group Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.addnewRegister(record.key) }> {record.name} </a>
          </span>
        ),
      },
      {
        title: 'team',
        dataIndex: 'item',
        key: 'item',
      },
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
     
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  addnewRegister = (id) => {
    this.props.history.history.push(`/app/setup/product-option/new-item-group/${id}`);
  }

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                     { this.state.actiontitle }
                    </Button>
                    <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" />

                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (Itemgroups);