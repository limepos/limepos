import React from 'react';
import { Table, Input, Button, message  } from 'antd';
const { Search } = Input;
const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    variantname: `Variant ${i}`,
    comment: 'Comments ',
    price: `500`,
    order: `${i}`,
    group: 'Yes'
  });
}

class Variant extends React.Component {

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns:[
      {
        title: 'Variant Name',
        dataIndex: 'variantname',
        key: 'variantname',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.editvarint(record.key) }> {record.variantname} </a>
          </span>
        ),
      },
      { 
        title: 'Variant Comment',
        dataIndex: 'comment',
        key: 'comment',
        render: (text, record) => (
          <span  onClick={() => this.editvarint(record.key) }> {record.comment} </span>
        ),
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        render: (text, record) => (
          <span  onClick={() => this.editvarint(record.key) }> {record.price} </span>
        ),
      },
      {
        title: 'Sort Order',
        dataIndex: 'order',
      },
      {
        title: 'Is Linked To A Variant Group?',
        dataIndex: 'group',
      }
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
      constructor(props) {
        super(props);
      }
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  editvarint = (id) => {
   
    this.props.history.history.push(`/app/setup/product-option/edit-variant/${id}`);
  }
  
  finder = (e) => {
    console.log(e.target.value)
     let customerSearchLower = e.target.value.toLowerCase()
    const filterFunc = (item: { variantname : string; comment : string; price : string;  order : string;  group : string }) => {
      return ( item.variantname.toLowerCase().indexOf(customerSearchLower) !== -1 || item.comment.toLowerCase().indexOf(customerSearchLower) !== -1 || item.price.toLowerCase().indexOf(customerSearchLower) !== -1 || item.order.toLowerCase().indexOf(customerSearchLower) !== -1 || item.group.toLowerCase().indexOf(customerSearchLower) !== -1)
    }

      const data = this.state.bookdata.filter((item: { variantname : string; comment : string; price : string;  order : string;  group : string }) => filterFunc(item))

    this.setState({
      displaybookdata: data
    })

    if(customerSearchLower.length === 0) {
      
      this.setState({
        displaybookdata: [...this.state.bookdata]
      })
    }
    
   }

  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                     { this.state.actiontitle }
                    </Button>
                    <Search placeholder="input search text"  onChange={e => this.finder(e) } className="searchBox" />

                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table rowSelection={rowSelection} columns={this.state.columns}  dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (Variant);