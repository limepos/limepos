import React from 'react';
import { Typography, Button, Tabs, Row, Col, Modal,Input} from 'antd';
import Grid from '@material-ui/core/Grid';

import Variant from './variants/variants';
import VariantsGroup from './variantsGroup/variantsGroup';
import Addons from './addons/addons';
import AddonsGroups from './addonGroups/addonGroups';
import ItemGroups from './itemGroup/itemGroups';
const { Title,Text } = Typography;
const { TabPane } = Tabs;

class ProductOption extends React.Component {

  constructor(props) {
    super(props);

  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-options/1'){
     
    this.state.current_tab = '1';
  
     console.log("current tab", this.state.current_tab)

  } 
  if( currantbackurl === '/app/setup/product-options/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
    
  }

  if(currantbackurl === '/app/setup/product-options/3'){
    this.state.current_tab = '3';

    console.log("current tab", this.state.current_tab)
   
  }
  if(currantbackurl === '/app/setup/product-options/4'){
    this.state.current_tab = '4';
    console.log("current tab", this.state.current_tab)
  
  }
  if(currantbackurl === '/app/setup/product-options/5'){

    this.state.current_tab = '5';

    console.log("current tab 5", this.state.current_tab)  
    
  }   


  }

  state = {
     current_tab: '1',
     visible: false,
     confirmLoading: false,
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })
    if( key === 1){
      this.props.history.push("/app/setup/product-options/1");
      console.log(key)
    }else if( key === 2){
      this.props.history.push("/app/setup/product-options/2");
      console.log(key)
    }else if( key === 3){
      this.props.history.push("/app/setup/product-options/3");
      console.log(key)
    }else if( key === 4){
      this.props.history.push("/app/setup/product-options/4");
      console.log(key)
    }else if( key === 5){
      this.props.history.push("/app/setup/product-options/5");
      console.log(key)
    }   
    
  }
   
  addnewRegister = (tab) => {

    console.log("===", tab)

    if(tab === 1){
     this.props.history.push("/app/setup/product-option/new-variant");
    }
     else if(tab === 2){
      this.props.history.push("/app/setup/product-option/new-variant-group");
     }
     else if(tab === 3){
      this.props.history.push("/app/setup/product-option/new-addon");
     }
     else if(tab === 4){
      this.props.history.push("/app/setup/product-option/new-addon-group");
     }
     else if(tab === 5){
      this.props.history.push("/app/setup/product-option/new-item-group");
     }
  }

  export = (tab) =>{

        this.setState({
          visible: true,
        });
  }

  import = (tab) => {
      
    if(tab === 1){
      this.props.history.push("/app/setup/product-option/import/variant");
     }
      else if(tab === 2){
       this.props.history.push("/app/setup/product-option/import/varinat-group");
      }
      else if(tab === 3){
       this.props.history.push("/app/setup/product-option/import/addon");
      }
      else if(tab === 4){
       this.props.history.push("/app/setup/product-option/import/addon-group");
      }
      else if(tab === 5){
       this.props.history.push("/app/setup/product-option/import");
      }
  }


  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };

  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Product Options </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                   { this.state.current_tab === 1 ?<span> <Button type="" onClick={() =>  this.export(1)}  className=""> Export  </Button> <Button type="" onClick={() =>  this.import(1)}  className=""> Import </Button> <Button type="primary" onClick={() =>  this.addnewRegister(1)}  className="primarybutton">Add Variant </Button> </span> : '' }
                   { this.state.current_tab === 2 ? <span> <Button type="" onClick={() =>  this.export(2)}  className=""> Export  </Button> <Button type="" onClick={() =>  this.import(2)}  className=""> Import </Button>  <Button type="primary" onClick={() =>  this.addnewRegister(2)}  className="primarybutton">Add Variant Group </Button>  </span>: '' }
                   { this.state.current_tab === 3 ? <span> <Button type="" onClick={() =>  this.export(3)}  className=""> Export  </Button> <Button type="" onClick={() =>  this.import(3)}  className=""> Import </Button> <Button type="primary" onClick={() =>  this.addnewRegister(3)}  className="primarybutton">Add Addon </Button>  </span>: '' }
                   { this.state.current_tab === 4 ? <span> <Button type="" onClick={() =>  this.export(4)}  className=""> Export  </Button> <Button type="" onClick={() =>  this.import(4)}  className=""> Import </Button> <Button type="primary" onClick={() =>  this.addnewRegister(4)}  className="primarybutton">Add Addon Group </Button>  </span>: '' }
                   { this.state.current_tab === 5 ? <span> <Button type="" onClick={() =>  this.export(5)}  className=""> Export  </Button> <Button type="" onClick={() =>  this.import(5)}  className=""> Import </Button> <Button type="primary" onClick={() =>  this.addnewRegister(5)}  className="primarybutton">Add Item Group </Button>  </span> : '' }
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                      <TabPane tab="Variants"  key="1">
                        <Variant history={this.props} />
                      </TabPane>
                      <TabPane tab="Variants Groups" key="2">
                          <VariantsGroup history={this.props} />
                      </TabPane>
                      <TabPane tab="Addons" key="3">
                          <Addons history={this.props} />
                      </TabPane>
                      <TabPane tab="Addon Groups" key="4">
                          <AddonsGroups history={this.props} />
                      </TabPane>
                      <TabPane tab="Item Groups" key="5">
                          <ItemGroups history={this.props} />
                      </TabPane>
                    </Tabs>
                  </Col> 
                </Col>
                <Modal
                  title="Export"
                  visible={this.state.visible}
                  onOk={this.handleOk}
                  confirmLoading={this.state.confirmLoading}
                  onCancel={this.handleCancel}
                >
                  <Text>Send To Email Address </Text> 
                  <Input  placeholder="Enter Your Email" />
                </Modal>
              </Row>     
            </div>
    )
  }
}
export default (ProductOption);