import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Checkbox,
  Button,
  Typography
} from 'antd';

const { Text } = Typography;
const { Title } = Typography;

class Addregister extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };

  render() {
    const { value } = this.state;
    const { getFieldDecorator } = this.props.form;


    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
 
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Product Options </span> / New Addon Group </Title>  
          </Col>
        </Row>

      <Row gutter={[20, 20]}>
      <Col span={24}>
        <Col span={8}>
           <Title level={4}>Setup Addon Group</Title>
           <Text>Addon groups are used to bunch a set of addons and attach to a product. Multiple addons can be chosen from an addon group.</Text>
         </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Addon Group Name" {...formItemLayout}>
            {getFieldDecorator('registername', {
            rules: [
              {
                required: true,
                message: 'Please enter Addon Group Name',
              },
            ],
          })(<Input  placeholder="Addon Group Name" />)}
        </Form.Item>
        <Form.Item label="Sort Order " {...formItemLayout}>
            <Input  placeholder="Order" />
        </Form.Item>
        <Col span={12}>
          <Form.Item label="Min Selectable" {...formItemLayout}>
            <Input  placeholder="Min Selectable" type="number" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Max Selectable" {...formItemLayout}>
            <Input  placeholder="Min Selectable" type="number" />
          </Form.Item>
        </Col>
        <Col span={24}>
        <Form.Item label="Select the addons">
          {getFieldDecorator('checkbox-group', {
            initialValue: ['A', 'B'],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={6}>
                  <Checkbox value="A"> Addon 1</Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="B">
                  Addon 2
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="C"> Addon 3 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="D"> Addon 4  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="E"> Addon 5 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="f"> Addon 1</Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="g">
                  Addon 2
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="h"> Addon 3 </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="i"> Addon 4  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="j"> Addon 5 </Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>,
          )}
        </Form.Item>
        </Col>
        <Col span={24}>

        <Form.Item >
        <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
        </Col>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addregister);