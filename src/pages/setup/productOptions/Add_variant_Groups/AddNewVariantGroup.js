import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Checkbox,
  Button,
  Typography 
   
} from 'antd';
const { Text,Title } = Typography;

class AddNewVariantGroup extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Product Options </span>  /  New Variant Group </Title>  
          </Col>
        </Row>

      <Row gutter={[20, 20]}>
      <Col span={24}>
        <Col span={8}>
           <Title level={4}>Setup Variant Group</Title>
           <Text>Variant groups are used to bunch a set of variants and attach it to a product. Only one variant can be chosen from a variant group.</Text>
         </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Variant Group Name" {...formItemLayout}>
            {getFieldDecorator('registername', {
            rules: [
              {
                required: true,
                message: 'Please enter Variant Group Name',
              },
            ],
          })(<Input  placeholder="Name" />)}
        </Form.Item>
    
        <Form.Item label="Sort Order" {...formItemLayout}>
            <Input  placeholder="Order" type="number" />
        </Form.Item>
      
        <Col span={24}>
        <Form.Item label="Select the variants">
          {getFieldDecorator('checkbox-group', {
            initialValue: ['A', 'B'],
          })(
            <Checkbox.Group style={{ width: '100%' }}>
              <Row>
                <Col span={6}>
                  <Checkbox value="A"> chees (extra chees) </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="B">
                  larg (larg item)
                  </Checkbox>
                </Col>
                <Col span={6}>
                  <Checkbox value="C"> normal (Norma) </Checkbox>
                </Col>
              </Row>
            </Checkbox.Group>
          )}
        </Form.Item>
        </Col>
        <Col span={24}>

        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
        </Col>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(AddNewVariantGroup);
