import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Typography,

   
} from 'antd';

const { Text } = Typography;
const { Title } = Typography; 


//  file upload...
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

class Addregister extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };
  render() {

    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };



    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}>  <span onClick={ this.goback }> Order Ticket Groups </span> / New Order Ticket Group </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Your Order Ticket Group Details</Title>
           <Text>You can use Order Ticket Group to split and print KOTs across kitchens. For example, you can create a group called Sandwich Counter and assign Sandwich category products to use a separate KOT printer.</Text>
        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Order Ticket Group Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Order ticket group name is required.',
              },
            ],
          })(<Input  placeholder="Order Ticket Group Name" />)}
        </Form.Item>
        <Form.Item>
        <Button type="default" onClick={ this.goback }>
            Go Back
          </Button> 
          
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addregister);