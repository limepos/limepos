import React from 'react';
import { Typography, Button, Tabs, Row, Col} from 'antd';
import Grid from '@material-ui/core/Grid';

import AllTicketGroup from './AllOrderTicketGroup/allOrderTicketGroup';
import AllProductGroup from './AllProductCategories/AllProductCategories';

const { Title } = Typography;
const { TabPane } = Tabs;

class ProductCategories extends React.Component {

  constructor(props) {
    super(props);
  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-categories/1'){
     
    this.state.current_tab = '1';
  
     console.log("current tab", this.state.current_tab)

  } 
  if( currantbackurl === '/app/setup/product-categories/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
    
  }
  }

  state = {
     current_tab: '1',
  }

  componentDidMount(){

  }

  callback = key => {
    this.setState({
      current_tab: key,

    })
    if( key === 1){
      this.props.history.push("/app/setup/product-categories/1");
    }else if( key === 2){
      this.props.history.push("/app/setup/product-categories/2");
    }   
  }
   
  addnewRegister = (tab) => {
    if(tab === 1){
     this.props.history.push("/app/setup/product-categorie/new-category");
    }
     else if(tab === 2){
      this.props.history.push("/app/setup/new-ticket-group");
     }
  }
  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Product Categories </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                   { this.state.current_tab === 1 ? <Button type="primary" onClick={() =>  this.addnewRegister(1)}  className="primarybutton"> Add Product Category </Button> : '' }
                   { this.state.current_tab === 2 ? <Button type="primary" onClick={() =>  this.addnewRegister(2)}  className="primarybutton"> Add Order Ticket Group </Button> : '' }
                 </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                      <TabPane tab="All Product Categories "  key="1">
                        
                        <AllProductGroup history={this.props} />
                      </TabPane>
                      <TabPane tab="All Order Ticket Groups" key="2">
                         <AllTicketGroup history={this.props} />
                      </TabPane>
                    </Tabs>
                  </Col> 
                </Col>
              </Row>     
            </div>
    )
  }
}
export default (ProductCategories);