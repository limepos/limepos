import React from 'react';
import { Table, Input, Button, message  } from 'antd';
const { Search } = Input;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    name: `Main Kitchen ${i}`,
    order: `${i}`, 
  });
}
class AllTicketGroup extends React.Component {
  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
      {
        title: 'Order Ticket Group Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.editvarintGroup(record.key) }> {record.name} </a>
          </span>
        ),
      }
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
      constructor(props) {
        super(props);
      }
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  editvarintGroup = (id) => {
   
    this.props.history.history.push(`/app/setup/new-ticket-group/${id}`);
  }

  finder = (e) => {
    console.log(e.target.value)
     let customerSearchLower = e.target.value.toLowerCase()
    const filterFunc = (item: { name : string }) => {
      return (item.name.toLowerCase().indexOf(customerSearchLower) !== -1 )
    }

      const data = this.state.bookdata.filter((item: { name : string }) => filterFunc(item))

     let displaybookdata = data.sort((a, b) =>
      this.state.sortValue === 'end'
        ? a[this.state.sortName] < b[this.state.sortName]
          ? -1
          : 1
        : b[this.state.sortName] < a[this.state.sortName]
        ? -1
        : 1
    ) 

    this.setState({
      displaybookdata: data
    })

    if(customerSearchLower.length === 0) {
      
      this.setState({
        displaybookdata: [...this.state.bookdata]
      })
    }
    
   }

  render() {
    
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                     { this.state.actiontitle }
                    </Button>
                    <Search placeholder="input search text"  onChange={e => this.finder(e) }  className="searchBox" />

                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table rowSelection={rowSelection} columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (AllTicketGroup);