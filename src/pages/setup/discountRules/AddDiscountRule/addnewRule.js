import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Checkbox,
  Button,
  Typography,
  DatePicker,
  TimePicker  
   
} from 'antd';

const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;
class Addnewdiscout extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
    visible: false,
    tags: [],
    inputVisible: false,
    inputValue: '',
    register : ['Register 1','Register 6','Register 5','Register 4','Register 3','Register 2'],
    dateFormat : 'YYYY/MM/DD',
    showendtime : false,
    showhours : false,
  };

  constructor(props) {
    super(props);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  selectregister = (checkedValues) => {

     this.setState({ tags:checkedValues})

    console.log('checked = ', checkedValues);
  }

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

   datachnage = (date, dateString) => {
    console.log(date, dateString);
  }


// Disable past dates

  disabledDate = (current) => {
  
    console.log("current", current)
    return  current < (Date.now() - 1*24*60*60*1000)   // Minus 1 day millisecond to current date.
  }

  // Show and hide end data field.

  showEndTime = () => {
    
       this.setState({
         showendtime : !this.state.showendtime
       })
  }

  // show and hide hourse field.  
  showhours = () => {        
    this.setState({
      showhours : !this.state.showhours
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
  
    return (

      <div>
       <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Discount Rules </span> / New Discount Rule </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Discount Rule Details</Title>
           <Text>Choose the type of discount rule you want to create. Discount rules are identified by a unique coupon code.</Text>
        </Col>
        <Col span={16} className="pepar">

      
        <Form.Item label="Coupon Code" {...formItemLayout}>
            {getFieldDecorator('couponcode', {
            rules: [
              {
                required: true,
                message: 'Please enter coupon code',
              },
            ],
          })(<Input  placeholder="Discount Coupon Code" />)}
        </Form.Item>

        <Form.Item label="Discount Type" {...formItemLayout}>
            {getFieldDecorator('discounttype', {
            rules: [
              {
                required: true,
                message: 'Please select type.',
              },
            ],
          })( <Select
            showSearch
            placeholder="Select a type"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="fixamount">Fix Amount</Option>
            <Option value="parcentage">Parcentage</Option>
           </Select>)}
        </Form.Item> 

        <Form.Item label="Level" {...formItemLayout}>
            {getFieldDecorator('level', {
            rules: [
              {
                required: true,
                message: 'Please select level.',
              },
            ],
          })( <Select
            showSearch
            placeholder="Select a level"
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="product">Product</Option>
            <Option value="order">Order</Option>
           </Select>)}
        </Form.Item> 
         
            <Form.Item  label="Selected Registers">

            <Checkbox.Group style={{ width: '100%' }}  defaultChecked={ this.state.checkRegister} options={this.state.register} onChange={this.selectregister} />
      
            </Form.Item>
        

        <Form.Item >
        
        </Form.Item>
      
      </Col>
    </Col>

    <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Discount Visibility</Title>
           <Text>Set the time period when the discount rule is active. Two column layout for start and end date.</Text>
        </Col>
        <Col span={16} className="pepar">

      
        <Form.Item label="Start Date" {...formItemLayout}>
                <DatePicker disabledDate={this.disabledDate} onChange={this.datachnage} format={this.state.dateFormat} style={{ width: '100%' }} />
        </Form.Item>

        <Form.Item>
           <Checkbox  onChange={ this.showEndTime }> Set End Date </Checkbox>
           <div  style={{ marginBottom: '10px' }}/>
           { this.state.showendtime ? <DatePicker disabledDate={this.disabledDate} onChange={this.datachnage} format={this.state.dateFormat} style={{ width: '100%' }} /> : '' }
        </Form.Item>
         
        <Form.Item >
           <Checkbox  onChange={ this.showhours }> Set Happy Hours Time </Checkbox>
           <div  style={{ marginBottom: '10px' }}/>
           { this.state.showhours ? <span><TimePicker use12Hours format="h:mm a"  style={{ width: '48%' }} />  &nbsp;&nbsp; <TimePicker use12Hours format="h:mm a"  style={{ width: '48%' }} /> </span> : '' }
        </Form.Item> 

        <Checkbox  > Apply the discount automatically  </Checkbox>
  
         <div  style={{ marginBottom: '50px' }}/>
        

        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
     
      </Col>
    </Col>
      </Row>
      </Form>
    </div>
    );
  }
}
export default Form.create()(Addnewdiscout);
