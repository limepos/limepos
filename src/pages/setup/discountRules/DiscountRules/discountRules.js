import React from 'react';
import { Typography, Button, Tabs, Row, Col } from 'antd';
import Grid from '@material-ui/core/Grid';

import Alldisconts from '../AllDiscount/AllDiscounts';
const { Title } = Typography;
const { TabPane } = Tabs;

class Discountrules extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  }

  state = {
     current_tab: '1',
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })
  }
   
  addnewRegister = () => {
     this.props.history.push("/app/setup/discount-rule/new-rule");
  }
  render() {
    
    return (
            <div>
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Discount Rules </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                  <Button type="primary" onClick={this.addnewRegister }  className="primarybutton"> Add Discount Rule </Button>
                 </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                      <TabPane tab="All Discount Rules"  key="1">
                        <Alldisconts history={this.props} />
                      </TabPane>
                    </Tabs>
                  </Col> 
                </Col>
              </Row>     
            </div>
    )
  }
}
export default (Discountrules);