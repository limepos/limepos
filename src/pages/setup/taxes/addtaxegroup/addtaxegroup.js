import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Checkbox,
  Button,
  Typography
} from 'antd';

const { Text } = Typography;
const { Title } = Typography;

class Addtaxegroup extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  goback = () => {
     this.props.history.goBack();
  }
  onChange = (checkedValues) => {
    console.log('checked = ', checkedValues);
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Taxes </span>  / New Tax Groups </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}> Setup Tax Groups </Title>
           <Text>Create separate taxes for different tax rates and types.</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>One or more taxes can be grouped under a tax group and applied to products.</Text>

        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Tax Group Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please Enter Tax Group Name',
              },
            ],
          })(<Input  placeholder="Tax Group Name" />)}
        </Form.Item>
 
        <Checkbox value="yes"> Taxes inclusive in product price </Checkbox>
        <div style={{ margin: '15px 0' }} />
        <Form.Item label="Select one or more taxes to add to this tax group" {...formItemLayout}>
        <Checkbox.Group style={{ width: '100%' }} onChange={this.onChange }>
    <Row>
      <Col span={8}>
        <Checkbox value="A">Group 1</Checkbox>
      </Col>
      <Col span={8}>
        <Checkbox value="B"> Group 2 </Checkbox>
      </Col>
      <Col span={8}>
        <Checkbox value="C"> Group 3 </Checkbox>
      </Col>
      <Col span={8}>
        <Checkbox value="D"> Group 4 </Checkbox>
      </Col>
      <Col span={8}>
        <Checkbox value="E"> Group 5 </Checkbox>
      </Col>
    </Row>
  </Checkbox.Group>
        </Form.Item>
        <Form.Item >
          <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addtaxegroup);
