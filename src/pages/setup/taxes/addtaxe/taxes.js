import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
  Typography
} from 'antd';

const { Text } = Typography;
const { Title } = Typography;



class Addtax extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };
  render() {
 
    const { getFieldDecorator } = this.props.form;
    

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };

   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Taxes </span>  / New Tax </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Setup Taxes</Title>
           <Text>Create separate taxes for different tax rates and types.</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>One or more taxes can be grouped under a tax group and applied to products.</Text>

        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Tax Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter textname',
              },
            ],
          })(<Input  placeholder="Tax Name" />)}
        </Form.Item>

        <Form.Item label="Tax Percent" {...formItemLayout}>
            {getFieldDecorator('prefix', {
            rules: [
              {
                required: true,
                message: 'Please enter tax percent',
              },
            ],
          })(<Input  placeholder="Tax Percent" />)}
        </Form.Item> 
        <Form.Item >

        
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addtax);