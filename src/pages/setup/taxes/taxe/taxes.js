import React from 'react';
import {Typography, Button, Tabs, Row, Col} from 'antd';
import Grid from '@material-ui/core/Grid';

import Taxegroup from '../taxegrouptab/taxegroup';
import Taxe from '../taxetab/taxe';

const { Title } = Typography;
const { TabPane } = Tabs;

class Taxes extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/taxes/1'){
  
    this.state.current_tab = '1';
     console.log("current tab", this.state.current_tab)
  } 
   if( currantbackurl === '/app/setup/taxes/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
   }
  }

  state = {
     current_tab: '1',
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })
    if( key === 1){
       this.props.history.push("/app/setup/taxes/1");
       console.log(key)
    }else if( key === 2){
      this.props.history.push("/app/setup/taxes/2");
      console.log(key)
    }
    
  }
   
  addnewRegister = (tab) => {

    if(tab === 1){
    
     this.props.history.push("/app/setup/tax/addtax");
    }
     else if(tab === 2){
      this.props.history.push("/app/setup/tax/addtaxgroup");
     }
  }
  render() {
    
    return (
            <div>
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Taxes </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                   { this.state.current_tab === 1 ? <Button type="primary" onClick={() =>  this.addnewRegister(1)}  className="primarybutton">Add Tax </Button> : '' }
                   { this.state.current_tab === 2 ? <Button type="primary" onClick={() =>  this.addnewRegister(2)}  className="primarybutton">Add Tax Group </Button> : '' }
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                  <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                    <TabPane tab="Taxes "  key="1">
                      <Taxe history={this.props} />
                    </TabPane>
                    <TabPane tab="Tax Groups" key="2">
                        <Taxegroup history={this.props} />
                    </TabPane>
                  </Tabs>
                 </Col>
               </Col>
               </Row>   
            </div>
    )
  }
}
export default (Taxes);