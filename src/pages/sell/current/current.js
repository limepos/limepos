import React from 'react';
import { Input, Typography, Icon } from 'antd';
import Currency from "../../../components/currency";
import product from '../product.json';
const { Search } = Input;

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}
class CurrentOrder extends React.Component {
  state = {
    currenttype: 'all',
    displayProduct: product, 
    order : [],
    tables : [],
    qty : 0,
    columns : [
      {
        title: 'Name',
        dataIndex: 'name',
      },
      {
        title: 'price',
        dataIndex: 'price',
      }
    ]
  };
  constructor(props) {
    super(props);
    console.log("loading...");
  }

  componentDidMount() {
    console.log("1 loading...");
   // let orderedata =  localStorage.getItem('tables');
     
    console.log("orderedata", JSON.parse(localStorage.getItem("tables")))

    // this.setState({
    //   displayProduct : orderedata
    // })
  }

  currentType = (type) => {
    console.log("type", type)
    this.setState({
      currenttype: type
    })
  }

  addProduct =(product,table) => { 
    
          let getproductData = product;

          if (this.state.order.some((item) => item.id === getproductData.id  && item.table === table)) {
            
            var data = [...this.state.order];
            var displaydata = [...this.state.displayProduct];
            var index = data.findIndex(obj => obj.id === getproductData.id && obj.table === table);
            data[index].qty = data[index].qty + 1;
            displaydata[index].updateqty = displaydata[index].updateqty + 1;
            this.setState({data, displaydata },
              () =>
              console.log("qty update", this.state.tables)); 
             
              localStorage.setItem("tables", JSON.stringify(this.state.order));

          }else{
             
            let prod =  this.state.order.concat(product);
            var displaydata = [...this.state.displayProduct];
            var index = displaydata.findIndex(obj => obj.id === getproductData.id && obj.table === this.props.table);

            console.log(index)

            // displaydata1[index].updateqty = displaydata1[index].updateqty + 1;
             displaydata[index].table = this.props.table;

            this.setState({
              order : prod,
              displaydata
            }, () => {
              console.log("add Product", this.state.order);
              localStorage.setItem("tables",JSON.stringify(this.state.order));
            }
            )
         }     
  }
  
  renderTableProdudct() {
    // let table = this.state.displayProduct;
     return this.state.displayProduct.map((table, index) => {
       const { id, name, price, type, qty, updateqty } = table;
       return (
         <div>
           {this.state.currenttype === type || this.state.currenttype === 'all' ? <li onClick={() => this.addProduct(table,this.props.table)} >
             <div className="table">
               <div className="tablename"> {name}</div>
               <div class="price">
                     <span> {updateqty > 0 ? updateqty +' X' : '' } </span> <Currency/>{price}
               </div>
             </div>
           </li> : ''}
         </div>
       )
     })
   }

   quntyPlus = (id) => {
    
    var data = [...this.state.order];
    var displaydata = [...this.state.displayProduct];
    var index = data.findIndex(obj => obj.id === id);
    data[index].qty = data[index].qty + 1;
    displaydata[index].updateqty = displaydata[index].updateqty + 1;
    this.setState({data, displaydata},
      () =>
      console.log(" + qty update", this.state.order));
      localStorage.setItem(this.props.table, this.state.order);

   }

   quntyMinus = (id) => {

    var data = [...this.state.order];
    var displaydata = [...this.state.displayProduct];
    var index = data.findIndex(obj => obj.id === id);
     if(data[index].qty > 0){
    data[index].qty = data[index].qty - 1;
    displaydata[index].updateqty = displaydata[index].updateqty - 1;
     }
    this.setState({data, displaydata},
      () =>
      console.log("- qty update", this.state.order));
      localStorage.setItem(this.props.table, this.state.order);

  }

   renderItemlist() {
    const arr1 = this.state.order.filter(d => d.table === this.props.table);
    console.log('arr1', arr1);
    return arr1.map((items, index) => {
      const { id, name, price, type, qty, updateqty, table } = items;
       return (
         <div>
            <tr key={id} >
            <td> {name} </td>
            <td> <span onClick={() => this.quntyMinus(id) } className="circle"> <Icon type="minus" /> </span>  <span className="qty"> {updateqty} </span> <span className="circle" onClick={() => this.quntyPlus(id) }> <Icon type="plus" /> </span> </td>
            <td> <span className="textprice">  <Currency/> { price * updateqty }</span> </td>
            <td> <Icon type="edit" /> </td>
          </tr> 
         </div>
       )
     })

   }

  render() {

    return (
      <div>
        <div className="productSide">
          <div style={{ marginBottom: 16, overflow: 'auto' }}>
            <Search placeholder="Search Order" onChange={e => this.finder(e)} />
          </div>
          <div className="order_tab">
            <ul className="ordertype">
              <li onClick={() => this.currentTypeProduct('all')} className={`${this.state.currenttype === 'all' ? 'active' : ''}`}> All </li>
              <li onClick={() => this.currentTypeProduct('top')} className={`${this.state.currenttype === 'top' ? 'active' : ''}`}> Top </li>
              <li onClick={() => this.currentTypeProduct('general')} className={`${this.state.currenttype === 'general' ? 'active' : ''}`}> General </li>
            </ul>
          </div>
          <div className="orderTables">
            <ul className="tables product">
                 {this.renderTableProdudct() }
            </ul>
          </div>
        </div>
        <div class="orderside">
                <div className="orderItems">
                    <div className="ordemeta"> 
                      <span> {this.props.table} </span> | <a> Lakhan </a>   < a className="rightitem">Swap</a> <a className="rightitem"> Clear </a>
                        <Search className="customerSearch" />
                    </div>
                    <table className="orders">
                      <thead>
                      <tr key="1">
                        <th> Item </th>
                        <th> Quantity </th>
                        <th> Price</th>
                        <th> </th>
                      </tr>
                    </thead> 
                    <tbody> 
                       {this.renderItemlist() }
                     </tbody>  
                    </table>
                    
                </div>
        </div>
      </div>
    )
  }
}
export default (CurrentOrder);