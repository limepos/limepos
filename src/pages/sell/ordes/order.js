import React from 'react';
import { Table, Divider, Input, Typography, Button, message  } from 'antd';

import Grid from '@material-ui/core/Grid';
const { Title } = Typography;
const { Search } = Input;

class Order extends React.Component {

  state = {
    activeorder: 1,
    tables : [
            {
            id : 1,
            name: 'Table 1',
            status: 'In Progress',
            type : "take-away",
            split : false,
           },
           {
            id : 2,
            name: 'Table 2',
            status: 'In Progress',
            split : false,
            type : "delivery",
           },
           {
            id : 3,
            name: 'Table 4',
            status: 'In Progress',
            split : false,
            type : "table",
           },
            {
            id : 4,
            name: 'Table 4',
            status: 'In Progress',
            split : false,
            type : "free",
           },
           {
            id : 5,
            name: 'Table 5',
            status: 'In Progress',
            split : false,
            type : "occupied",
           },
           {
            id : 6,
            name: 'Table 6',
            status: 'In Progress',
            split : false,
            type : "unpaid",
           },
           {
            id : 7,
            name: 'Table 7',
            status: 'In Progress',
            split : false,
            type : "table",
           },
            {
            id : 8,
            name: 'Table 8',
            status: 'In Progress',
            split : false,
            type : "table",
           }
          ]
  };
      constructor(props) {
        super(props);
      }
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  editaddongroup = (id) => {
    this.props.history.history.push(`/app/setup/product-option/edit-addon/${id}`);
  }

  currentTable = (id) => {
     console.log( id )

     

      this.setState({
        activeorder: id,
        current_tab : 2
      })
  }

  renderTableOrder() {
    return this.state.tables.map((table, index) => {
       const { id, name, status, splite } = table //destructuring
       return (
              <li onClick={() => this.currentTable(id)} >
                <div className="table">
                    <div className="tablename">
                        {name}
                    </div>
                    <div class="tablestatus">
                         { this.state.activeorder === id ? <span className="active_status"></span>: ''} {status}
                    </div>
                </div>
                 <div class="splite" >{ splite ? `split` : ''} </div> 
              </li>
       )
    })
 }


  render() {

    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Search placeholder="input search text" onSearch={value => console.log(value)} className="searchBox" />
                </div>
                  <div className="order_tab">
                    <ul className="ordertype">
                       <li > All </li>
                       <li > Take-Away </li>
                       <li > Delivery </li>
                       <li > Tables </li>
                       <li > Free </li>
                       <li > Occupied </li>
                       <li > Unpaid </li>
                    </ul>
                  </div>
                  <div className="orderTables">
                       <ul className="tables">
                           {this.renderTableOrder()}
                       </ul>     
                  </div>
            </div>
    )
  }
}
export default (Order);