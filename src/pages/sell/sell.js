import React from 'react';
import ReactDOM from 'react-dom';
import { DatePicker, Typography, Button, Tabs, Row, Col, Input, Icon, Modal, Radio, Checkbox, TimePicker,Select  } from 'antd';
import Grid from '@material-ui/core/Grid';
import data from './data.json';
import product from './product.json';
import Currency from "../../components/currency";
import tableSearch from '../../components/TableSearch/tableSearch';

import burger from '../../images/products/burger.jpg';
import coffee from '../../images/products/coffee.jpg';
import coldcoffee from '../../images/products/coldcoffee.jpg';
import noodles from '../../images/products/noodles.jpg';
import pizza from '../../images/products/pizza.jpeg';
import tea from '../../images/products/tea.webp';
const { Search } = Input;
const { Title } = Typography;
const { TabPane } = Tabs;
const { confirm } = Modal;
const { Option } = Select;

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

class Sell extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    current_tab: '1',
    activeorder: 0,
    activetablename: '',
    currenttype: 'all',
    inProgress: [],
    sortName: '',
    sortValue: '',
    order: [],
    visible: false,
    columns: [
      {
        title: 'Name',
        dataIndex: 'name',
      },
      {
        title: 'status',
        dataIndex: 'status',
      },
      {
        title: 'type',
        dataIndex: 'type',
      }
    ],
    productcolumns: [
      {
        title: 'Name',
        dataIndex: 'name',
      },
      {
        title: 'type',
        dataIndex: 'type',
      },
      {
        title: 'price',
        dataIndex: 'price',
      },
      {
        title: 'category',
        dataIndex: 'category',
      }
    ],
    bookdata: data,
    displaybookdata: data,
    productList: product,
    Product: product,
    qty: 0,
    currentTypeProduct: 'all',
    total: 0,  // total of  minus discount.
    discountTotal: 0,    // total of discount.
    subtotal: 0,     //  total of all product price. 
    bulkdiscount: 0,   //  bulk discount calculation total.
    coupnediscount: 0,   // coupne discunt calculation total.
    BulkdiscountType: '',
    bulkdiscountValue: 0,  // bulk discount vlaue in cash or parcentage.
    viewItem: [{
      id: 0,
      name: '',
      price: 0,
      type: 'type',
      updatePrice: 0,
      qty: 1,
      discount: 0,
      note: '',
      discountby: ''
    }],
    discountType: '',
    showbulkdiscount: false,
    showCoupme: false,
    error: false,
    orderticketshow: false,
    startValue: null,
    endValue: null,
    endOpen: false,
    ordertyepe: 'sale',
    chargeshow: false,
    customerForm: false,
    swaptableshow :false,
    swapTableName : '',
    clickitem : 0,
    move : false,
    movefix :false,
    viewbill : true
    

  }

  componentDidMount() {

    // this.props.history
    if (localStorage.getItem("inProgress")) {
      this.setState({ inProgress: JSON.parse(localStorage.getItem("inProgress")) })
    } else {
      console.log("noe")
      this.setState({ inProgress: [] })
    }
    this.subtotalCalc();
    //   localStorage.setItem('inProgress', )
  }


  finder = (e) => {

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }
  finderProduct = (e) => {

    let searchtext = e.target.value
    let columns = this.state.productcolumns
    let bookdata = this.state.productList

    let r = tableSearch(searchtext, bookdata, columns);
    //console.log("Response", r)
    this.setState({
      Product: r
    })
  }

  callback = key => {
    console.log(key)
    this.setState({
      current_tab: key
    }, () =>
      this.getCurrentTableOrder()
    )
  }

  getCurrentTableOrder = () => {
    //console.log("sse ",this.state.current_tab);
    if (this.state.current_tab === 2) {
      //  localStorage.getItem( this.state.activetablename);
      // console.log("table Items", JSON.parse(localStorage.getItem("table")));
      if (localStorage.getItem(this.state.activetablename) === null) {
        // console.log("this is null");
        setTimeout(
          function () {
            this.setState({
              order: []
            }, () => this.subtotalCalc())
          }
            .bind(this),
          10
        );

      } else {
        setTimeout(
          function () {
            this.setState({
              order: JSON.parse(localStorage.getItem(this.state.activetablename))
            }, () => this.subtotalCalc())
          }
            .bind(this),
          10
        );
      }

      if (localStorage.getItem("bill" + this.state.activetablename) === null) {



      } else {
        let bill = JSON.parse(localStorage.getItem("bill" + this.state.activetablename))

        console.log("bill is ", bill);

        this.setState({
          subtotal: bill.subtotal,
          discountTotal: bill.discountTotal,
          bulkdiscount: bill.bulkdiscount,   //  bulk discount calculation total.
          coupnediscount: bill.coupnediscount,   // coupne discunt calculation total.
          BulkdiscountType: bill.BulkdiscountType,
          bulkdiscountValue: bill.bulkdiscountValue,
          total: bill.total
        })

      }

    }
  }

  addnewRegister = (tab) => {

    
    if (tab === 1) {
      this.props.history.push("/app/setup/product-categorie/new-category");
    }
    else if (tab === 2) {
      this.props.history.push("/app/setup/new-ticket-group");
    }
  }
  currentTable = (id, activetablename) => {

    let tab = this.state.inProgress.concat(id)

    this.setState({ inProgress: tab })

    setTimeout(
      function () {
        const sampleValues = this.state.inProgress;
        var unique = sampleValues.filter(onlyUnique);
        //console.log(unique);
        let v = JSON.stringify(unique);
        localStorage.setItem('inProgress', v);
        //console.log(JSON.parse(localStorage.getItem("inProgress")));
      }
        .bind(this),
      10
    );

    this.setState({
      activeorder: id,
      current_tab: '2',
      activetablename: activetablename,
    }, () => {
      this.getCurrentTableOrder();
      this.subtotalCalc();
    }
    )
  }

  currentType = (type) => {
    console.log("type", type);
    this.setState({
      currenttype: type
    })
  }

  currentTypeProduct = (type) => {
    console.log("type", type);
    this.setState({
      currentTypeProduct: type
    })
  }



  renderTableOrder() {
    // let table = this.state.displaybookdata;
    return this.state.displaybookdata.map((table, index) => {
      const { id, name, splite, type } = table;
      return (
        <>
          {this.state.currenttype === type || this.state.currenttype === 'all' ? <li key={id} onClick={() => this.currentTable(id, name)} >
            <div key={`table` + id} className={`table ${this.state.inProgress.includes(id) ? 'inprogress' : ''}`}>
              <div key={`tablename` + id} className="tablename">
                {name}
              </div>
              <div key={`tablestatus` + id} className="tablestatus">
                {this.state.activeorder === id ? <span className="active_status" key={`active_status` + id}></span> : ''}
                {this.state.inProgress.includes(id) ? <span key={`status` + id}> In Progress </span> : ''}
              </div>
            </div>
            <div className="splite" key={`splite` + id} >{splite ? `split` : ''} </div>
          </li> : ''}
        </>
      )
    })
  }

  /* -***** Tab 2 *****  -- */

  addProduct = (id, name, price, type, e ) => {

    var n = ReactDOM.findDOMNode(this);
    console.log(n);
    
    let getproductData = {
      "id": id,
      "name": name,
      "price": price,
      "type": type,
      "updatePrice": price,
      "qty": 1,
      "discount": 0,
      "note": '',
      "discountby": '',
      "discoutnumber": 0
    };

    console.log(getproductData);

    if (this.state.order.some((item) => item.id === getproductData.id)) {
      var data = [...this.state.order];
      var index = data.findIndex(obj => obj.id === getproductData.id);
      data[index].qty = data[index].qty + 1;
      data[index].updatePrice = data[index].price * (data[index].qty);
      this.setState({ order: data,clickitem : id },
        () =>
          console.log("qty update", this.state.order));
      this.subtotalCalc();
      localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
      this.billcalcaulatore()

    } else {

      console.log(getproductData)
      console.log("product", getproductData)
      let prod = this.state.order.concat(getproductData);

      console.log("new Prouduct", prod);

      this.setState({
        order: prod,
        clickitem : id 
      }, () => {
        console.log("add Product", this.state.order);
        this.subtotalCalc();
        localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
        this.billcalcaulatore()
      }
      )
    }

    setTimeout(
      function () {
        this.setState({
         move:true
        })
      }
        .bind(this),
      100
    );
    setTimeout(
      function () {
        this.setState({
          clickitem : 0,
          move:false,
        })
      }
        .bind(this),
      500
    );

      

  }

  renderTableProdudct() {

    return this.state.Product.map((table, index) => {
      const { id, name, price, type } = table;
      let qts = this.state.order.filter(d => d.id === id);

      if (qts[0] !== undefined) {
        var q = qts[0].qty;
      }

      return (
        <>
          {this.state.currentTypeProduct === type || this.state.currentTypeProduct === 'all' ? <li  onClick={() => this.addProduct(id, name, price, type, this)}> 
            <div className="table prod"  style={{ backgroundImage: `url(${ name === 'Burger' ? burger : name === 'Pizza' ? pizza : name === 'Tea' ? tea : name === 'Cold Coffee' ? coldcoffee : name === 'Coffee' ? coffee : name === 'Noodles' ? noodles: '' })` }}>
             
            </div>
            <div className="pdetails">
            <div className="tablename">
                {name}
              </div>
              <div className="price">
                <span> {q > 0 ? q + ' X' : ''}</span> <Currency />{price}
              </div>
            </div>
             <>{ this.state.clickitem  === id ? <img  className={`b-flying-img ${this.state.move ? 'move' : this.state.movefix ? 'movefix':''} `} src={ name === 'Burger' ? burger : name === 'Pizza' ? pizza : name === 'Tea' ? tea : name === 'cold Coffee' ? coldcoffee : name === 'Coffee' ? coffee : name === 'Noodles' ? noodles: '' } /> : '' }</>
          </li> : ''}
        </>
      )
    })
  }

  quntyPlus = (id) => {
    var data = this.state.order;
    var index = data.findIndex(obj => obj.id === id);
    data[index].qty = data[index].qty + 1;
    data[index].updatePrice = data[index].price * data[index].qty;
    this.setState({ order: data },
      () =>
        console.log(" + qty update", this.state.order));
    this.subtotalCalc();
    localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

  }

  quntyMinus = (id) => {

    var data = this.state.order;

    var index = data.findIndex(obj => obj.id === id);
    if (data[index].qty > 0) {
      data[index].qty = data[index].qty - 1;
      data[index].updatePrice = data[index].price * data[index].qty;
    }
    this.setState({ order: data },
      () =>
        console.log("- qty update", this.state.order));
    this.subtotalCalc();
    localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

  }

  /* Edit quntity */

  editQuntyPlus = () => {

    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber

    }]


    items[0].qty = this.state.viewItem[0].qty + 1
     
    items[0].updatePrice = items[0].price * (this.state.viewItem[0].qty + 1);

    this.setState({
      viewItem: items
    })

  }

  editQuntyMinus = () => {


    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber

    }]

    if (items[0].qty > 0) {
      items[0].qty = this.state.viewItem[0].qty - 1
      items[0].updatePrice = items[0].price * (this.state.viewItem[0].qty - 1);
      this.setState({
        viewItem: items
      })
    }

  }


  subtotalCalc = () => {

    let subtotalSum = 0;
    let discountSum = 0;

    this.state.order.forEach(element => {
      discountSum += parseInt(element.discount);
      subtotalSum += parseInt(element.updatePrice);
    })

    console.log("Total ", parseInt(subtotalSum));


    let bulkdis = this.state.bulkdiscount;



    this.setState({
      total: subtotalSum - discountSum - bulkdis,
      discountTotal: discountSum,
      subtotal: subtotalSum,
    }, () => this.billcalcaulatore())

  }

  showModal = (id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber) => {

    let items = [{ id: id, name: name, price: price, type: type, qty: qty, updatePrice: updatePrice, discount: discount, note: note, discountby: discountby, discoutnumber: discoutnumber }]

    this.setState({
      visible: true,
      viewItem: items
    }, () => console.log("viewItem", this.state.viewItem));
  };

  handleOk = e => {
    console.log(e);
    if (this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice) { } else {
      this.setState({
        visible: false,
      });
    }


    if (this.state.order.some((item) => item.id === this.state.viewItem[0].id)) {
      var data = [...this.state.order];
      //id, name, price, type, qty, updatePrice, discount, note, discountby

      var index = data.findIndex(obj => obj.id === this.state.viewItem[0].id);
      data[index].qty = this.state.viewItem[0].qty;
      data[index].updatePrice = this.state.viewItem[0].updatePrice;
      data[index].discount = this.state.viewItem[0].discount;
      data[index].note = this.state.viewItem[0].note;
      data[index].discountby = this.state.viewItem[0].discountby;
      data[index].discoutnumber = this.state.viewItem[0].discoutnumber;

      this.setState({ order: data },
        () =>
          console.log("qty update", this.state.order));
      this.subtotalCalc();
      localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));

    }

  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
      customerForm: false,
      swaptableshow : false
    });
  };

  changeNot = (event) => {

    let items = [{
      id: this.state.viewItem[0].id,
      name: this.state.viewItem[0].name,
      price: this.state.viewItem[0].price,
      type: this.state.viewItem[0].type,
      qty: this.state.viewItem[0].qty,
      updatePrice: this.state.viewItem[0].updatePrice,
      discount: this.state.viewItem[0].discount,
      note: this.state.viewItem[0].note,
      discountby: this.state.viewItem[0].discountby,
      discoutnumber: this.state.viewItem[0].discoutnumber

    }]

    if (event.target.name === "discount") {

      let price = items[0].updatePrice;


      items[0].discountby = this.state.discountType;
      items[0].discoutnumber = event.target.value;

      if (this.state.discountType === 'percentage') {

        let parsent = (parseInt(price) * parseInt(event.target.value)) / 100;
        console.log("parcentage", parsent);
        items[0].discount = parsent;
        this.setState({
          viewItem: items
        })

      } else {

        items[0].discount = parseInt(event.target.value);
        this.setState({
          viewItem: items
        })

      }

    }
    if (event.target.name === "note") {
      items[0].note = event.target.value;
      this.setState({
        viewItem: items
      })
    }
    if (event.target.name === "qty") {


      items[0].qty = parseInt(event.target.value);

      console.log(items[0].price, event.target.value);

      items[0].updatePrice = parseInt(items[0].price) * parseInt(event.target.value);

      console.log("price update", items)

      this.setState({
        viewItem: items
      })
    }
  }

  checkDiscount = (e) => {
    console.log("discount", e.target.value)
    this.setState({
      discountType: e.target.value,
    })
  }



  renderItemlist() {
    return this.state.order.map((items, index) => {
      const { id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber } = items;
      return (
          <tr key={id}  className={this.state.clickitem === id ? 'blink' : '' }>
            <td className="pname"> <div className="pimage"  style={{ backgroundImage: `url(${ name === 'Burger' ? burger : name === 'Pizza' ? pizza : name === 'Tea' ? tea : name === 'Cold Coffee' ? coldcoffee : name === 'Coffee' ? coffee : name === 'Noodles' ? noodles: '' })` }}></div> <span>{name}</span> </td>
            <td> <span onClick={() => this.quntyMinus(id)} className="circle"> <Icon type="minus" /> </span>  <span className="qty"> {qty} </span> <span className="circle" onClick={() => this.quntyPlus(id)}> <Icon type="plus" /> </span> </td>
            <td> <span className="textprice">  <Currency /> {updatePrice} </span> </td>
            <td> <span className="textprice">  <Currency /> {discount} </span> </td>
            <td> <Icon type="edit" onClick={() => this.showModal(id, name, price, type, qty, updatePrice, discount, note, discountby, discoutnumber)} /> </td>
          </tr>
      )
    })

  }

  bulkdiscountshow = () => {

    this.setState({
      showbulkdiscount: !this.state.showbulkdiscount,
      showCoupme: false

    })

  }

  coupnshow = () => {
    this.setState({
      showCoupme: !this.state.showCoupme,
      showbulkdiscount: false
    })
  }

  bulkdiscounttype = (e) => {

    this.setState({
      BulkdiscountType: e.target.value
    })
  }

  bulkdiscounCount = (e) => {

    this.setState({
      bulkdiscountValue: e.target.value
    })

  }

  bulkdiscountCalculate = () => {

    let bulkdiscountvalue = this.state.bulkdiscountValue;



    let price = this.state.subtotal - this.state.discountTotal;

    console.log("Total Discount ", this.state.discountTotal);

    console.log("value", price)

    if (this.state.BulkdiscountType === 'percentage') {

      let parsent = (parseInt(price) * parseInt(bulkdiscountvalue)) / 100;
      console.log("parcentage", parsent);



      console.log("Parsent", this.state.total)

      if (price > parsent) {

        this.setState({
          bulkdiscount: parsent,
          total: price - parsent,
          showbulkdiscount: false,
          error: false
        }, () => this.billcalcaulatore())

      } else {
        console.log("Parsent is greater")
        this.setState({

          error: true
        }, () => console.log(this.state.error))

      }
    } else {
      let total = parseInt(price) - bulkdiscountvalue;

      console.log("Total ", price, bulkdiscountvalue)

      if (price > bulkdiscountvalue) {

        this.setState({
          bulkdiscount: bulkdiscountvalue,
          total: total,
          showbulkdiscount: false,
          error: false
        }, () => this.billcalcaulatore())
      } else {
        console.log("Parsent is greater")
        this.setState({
          error: true
        }, () => console.log(this.state.error))

      }



    }
  }

  billcalcaulatore = () => {
    let bill = {
      subtotal: this.state.subtotal,
      discountTotal: this.state.discountTotal,
      bulkdiscount: this.state.bulkdiscount,   //  bulk discount calculation total.
      coupnediscount: this.state.coupnediscount,   // coupne discunt calculation total.
      BulkdiscountType: this.state.BulkdiscountType,
      bulkdiscountValue: this.state.bulkdiscountValue,
      total: this.state.total,

    }

    // let prod = this.state.order.concat(bill);

    localStorage.setItem("bill" + this.state.activetablename, JSON.stringify(bill));
  }

  clearall = () => {

    let tabelid = this.state.activeorder;
    console.log(tabelid)

    let talels = this.state.inProgress;
    console.log(talels)

    var filteredAry = talels.filter(e => e !== tabelid)

    console.log(filteredAry)

    let inpro = JSON.stringify(filteredAry);

    localStorage.setItem('inProgress', inpro);

    localStorage.removeItem("bill" + this.state.activetablename);
    localStorage.removeItem(this.state.activetablename);

    this.setState({
      current_tab: '1',
      inProgress: filteredAry,
      activeorder: 0

    })
  }

  showDeleteConfirm = (e) => {
    confirm({
      title: 'Clear Receipt',
      content: 'This will create a cancellation order ticket and cancel this order. Are you sure you want to proceed?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {

        e.clearall()

      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }

  removeItem = (id) => {
    console.log("Remove id = ", id)

    if (this.state.order.some((item) => item.id === id)) {
      var data = [...this.state.order];

      var i = data.findIndex(obj => obj.id === id);

      if (i > -1) {
        data.splice(i, 1);
      }

      console.log("remaining items", data);

      this.setState({ order: data, visible: false },
        () =>
          console.log("qty update", this.state.order));
      this.subtotalCalc();
      localStorage.setItem(this.state.activetablename, JSON.stringify(this.state.order));
      this.billcalcaulatore()


    }
  }

  orderticket = () => {
    this.setState({
      orderticketshow: true
    })
  }

  orderItems() {
    return this.state.order.map((items, index) => {
      const { id, name, qty} = items;
      return (
          <li key={id} >
            {qty} X {name}
          </li>
      )
    })

  }

  generateOrderTicketPrint = () => {

  }

  onCancelorderTicket = () => {

    this.setState({
      orderticketshow: false
    })

  }

  disabledStartDate = startValue => {
    const { endValue } = this.state;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledStartDate = startValue => {
    const { endValue } = this.state;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = endValue => {
    const { startValue } = this.state;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value,
    });
  }

  onStartChange = value => {
    this.onChange('startValue', value);
  }

  handleStartOpenChange = open => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }
  checkorderfor = (e) => {
    this.setState({
      ordertyepe: e.target.value
    })
  }
  timechange = (time, timeString) => {
    console.log(time, timeString);
  }

  innerTabCallback = (key) => {
    console.log(key);
  }
  changebill = () => {

    this.setState({
      chargeshow: true
    })

  }
  closechargeview = () => {
    this.setState({
      chargeshow: false
    })
  }

  addCustomer = () => {
    this.setState({
      customerForm: true
    })
  }
  swaptable = () => {

     this.setState({
        activetablename : this.state.activetablename,
        swaptableshow: false
     })

  }
  showSwapform = () => {
    this.setState({
      swaptableshow: true
    })
  }
  tabelselect = (value) => {
    console.log(`selected ${value}`);
    this.setState({
      activetablename : value
    })
  }
  viewProduct =() => {
      this.setState({
        viewbill:false
      })
  }
  billShow =() => {
    this.setState({
      viewbill:true
    })
  }


  render() {

    return (
      <div hi="s">
        <Grid container spacing={3}>
          <Grid item xs={6} className="pageTitle">
            <Title level={4}> Sell </Title>
          </Grid>
          <Grid item xs={6} className="rightButton">
          </Grid>
        </Grid>
        <Row gutter={[50, 50]}>
          <Col span={24}>
            <Col span={24} className="pepar">
              <Tabs activeKey={this.state.current_tab} onChange={this.callback}>
                <TabPane tab="Orders" key="1">
                  <div>
                    <div style={{ marginBottom: 16, overflow: 'auto' }}>
                      <Search placeholder="Search Order" onChange={e => this.finder(e)} className="" />
                    </div>
                    <div className="order_tab">
                      <ul className="ordertype">
                        <li onClick={() => this.currentType('all')} className={`${this.state.currenttype === 'all' ? 'active' : ''}`}> All </li>
                        <li onClick={() => this.currentType('takeaway')} className={`${this.state.currenttype === 'takeaway' ? 'active' : ''}`}> Take-Away </li>
                        <li onClick={() => this.currentType('delivery')} className={`${this.state.currenttype === 'delivery' ? 'active' : ''}`}> Delivery </li>
                        <li onClick={() => this.currentType('tables')} className={`${this.state.currenttype === 'tables' ? 'active' : ''}`}> Tables </li>
                        <li onClick={() => this.currentType('free')} className={`${this.state.currenttype === 'free' ? 'active' : ''}`}>  Free </li>
                        <li onClick={() => this.currentType('occupied')} className={`${this.state.currenttype === 'occupied' ? 'active' : ''}`}> Occupied </li>
                        <li onClick={() => this.currentType('unpaid')} className={`${this.state.currenttype === 'unpaid' ? 'active' : ''}`}> Unpaid </li>
                      </ul>
                    </div>
                    <div className="orderTables">
                      <ul className="tables">
                        {this.renderTableOrder()}
                      </ul>
                    </div>
                  </div>
                </TabPane>
                <TabPane tab="Current" key="2">


                  <div>
                    {this.state.chargeshow === false ? <>
                      <div className="productSide">
                        <div style={{ marginBottom: 16, overflow: 'auto' }}>
                          <Search className="searchbutton" placeholder="Search Order" onChange={e => this.finderProduct(e)} /> <Button className="viewbill" onClick={ this.billShow}> View Bill </Button>
                        </div>
                        <div className="order_tab">
                          <ul className="ordertype">
                            <li onClick={() => this.currentTypeProduct('all')} className={`${this.state.currentTypeProduct === 'all' ? 'active' : ''}`}> All </li>
                            <li onClick={() => this.currentTypeProduct('top')} className={`${this.state.currentTypeProduct === 'top' ? 'active' : ''}`}> Top </li>
                            <li onClick={() => this.currentTypeProduct('general')} className={`${this.state.currentTypeProduct === 'general' ? 'active' : ''}`}> General </li>
                          </ul>
                        </div>
                        <div className="orderTables">
                          <ul className="tables product" >
                            {this.renderTableProdudct()}
                          </ul>
                        </div>
                      </div>
                      { this.state.viewbill ? 
                      <div class="orderside">
                        <div className="orderItems">
                          <div className="ordemeta">
                            <span> {this.state.activetablename} </span> | <a onClick={this.addCustomer}> Lakhan </a>   < a className="rightitem" onClick={this.showSwapform}>Swap</a> <a className="rightitem" onClick={() => this.showDeleteConfirm(this)}> Clear </a> <a className="rightitem viewProducts" onClick={ this.viewProduct}> View Products </a>
                            <Search className="customerSearch" />
                          </div>

                          {this.state.order.length > 0 ?
                            <>
                              <table className="orders">
                                <thead>
                                  <tr key="1">
                                    <th> Item </th>
                                    <th> Quantity </th>
                                    <th> Price</th>
                                    <th> Discount</th>
                                    <th> </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.renderItemlist()}
                                </tbody>
                              </table>


                              <div className="bulk_discount">

                                {this.state.showCoupme ? <div class="discountbox coupn">

                                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                                    <Input type="text" name="discount" value={this.state.viewItem[0].discoutnumber} onChange={this.changeNot} />
                                  </div>
                                  <button className="smallbutton"> Apply </button>
                                  <span className="discountboxarrow"></span>
                                </div> : ''}

                                {this.state.showbulkdiscount ? <div class="discountbox">
                                  <Radio.Group name="radiogroup" onChange={this.bulkdiscounttype} defaultValue={this.state.BulkdiscountType}>
                                    <Radio value={`cash`}> Cash</Radio>
                                    <Radio value={`percentage`}> Percentage </Radio>
                                  </Radio.Group>
                                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                                    <Input type="number" name="discount" value={this.state.bulkdiscountValue} onChange={this.bulkdiscounCount} />
                                  </div>
                                  <button className="smallbutton" onClick={this.bulkdiscountCalculate}> Done </button>
                                  <span className="discountboxarrow"></span>
                                </div> : ''}
                                <span> {}</span>

                                <a onClick={this.bulkdiscountshow}> {this.state.bulkdiscount > 0 && this.state.error === false ? <span> Bulk Discount <Currency />{this.state.bulkdiscount}</span> : this.state.error ? <small style={{ color: "red" }}> Discount more than total </small> : <span> Bulk Discount </span>}  </a> &nbsp; &nbsp; &nbsp;  <a onClick={this.coupnshow}> Coupon Code  </a>
                              </div>
                              <button className="bigbutton orderTicket" onClick={() => this.orderticket()}>Order Ticket </button>
                              &nbsp;
                         <button className="bigbutton chargeOrder" onClick={this.changebill}>  Charge <Currency /> {this.state.total}</button>
                            </>
                            :  <> 
                                 <p className="viewProductemptytitle">Your Cart is Empty Please add Proudct</p>
                                <Button onClick={ this.viewProduct} className="viewProductempty"> Add Products </Button> 
                               </> 
                                }
                        </div>
                        
                      </div>
                       : ''}
                    </> : ''}

                    {this.state.chargeshow ? <Col span={24} className="chargeOptions">
                      
                      <Col  xs={24} md={12} className="billingbooking">
                        <Tabs defaultActiveKey="1" onChange={this.innerTabCallback}>
                          <TabPane tab="General" key="1">

                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Customer Phone</label>
                              <Input type="text" placeholder="Customer mobile number" />
                            </div>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Customer Name</label>
                              <Input type="text" placeholder="Customer Name" />
                            </div>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Customer Email</label>
                              <Input type="text" placeholder="Customer email" />
                            </div>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Occupants</label>
                              <Input type="text" placeholder="Number of seats occupied" />
                            </div>

                          </TabPane>
                          <TabPane tab="Delivery" key="2">

                            <div style={{ marginBottom: 16, marginTop: 16, marginLeft: 5, marginRight: 5 }}>
                              <label>Shipping Address</label>
                              <Input type="text" placeholder="Street Address" />
                            </div>
                            <Col span={12} style={{ padding: 5 }} >
                              <label>City</label>
                              <Input type="text" placeholder="City" />
                            </Col>
                            <Col span={12} style={{ padding: 5 }} >
                              <label>Pincode</label>
                              <Input type="text" placeholder="Zipcode" />
                            </Col>
                          </TabPane>

                        </Tabs>
                      </Col>
                      <Col xs={24} md={12} className="billingbooking">
                        <Radio.Group name="radiogroup" onChange={this.checkorderfor} defaultValue={this.state.ordertyepe}>
                          <Radio value={`sale`}> Immediate Sale</Radio>
                          <Radio value={`booking`}> Booking</Radio>
                        </Radio.Group>

                        {this.state.ordertyepe === 'sale' ? <div class="sale">
                          <div style={{ marginBottom: 16, marginTop: 16 }}>
                            <label>Order Ticket Notes</label>
                            <Input type="text" placeholder="Order Note..." />
                          </div>
                          <div style={{ marginBottom: 1, marginTop: 16 }}>
                            <label> Payment Type </label>
                          </div>
                          <div style={{ marginBottom: 16, marginTop: 0 }}>
                            <Button className="payment_type"> Cash</Button>
                            <Button className="payment_type"> Credit / Debit Card </Button>
                            <Button className="payment_type"> Other </Button>
                            <Button className="payment_type"> Credit Sale Pending </Button>
                            <Button className="payment_type"> Split Payments </Button>
                          </div>
                          <div style={{ marginBottom: 16, marginTop: 16 }}>
                            <label>Cash Tendered</label>
                            <Input type="number" placeholder="Cash Tendered" />
                          </div>
                          <div style={{ marginBottom: 16, marginTop: 16 }}>
                            <label>Balance to Customer</label>
                            <Input type="number" placeholder="Balance to Customer" />
                          </div>

                          <button class="bigbutton orderTicket" onClick={this.closechargeview}> Cancel </button>
                          &nbsp;
                         <button className="bigbutton chargeOrder"> ng Payment <Currency /> {this.state.total} </button>

                        </div> : ''}

                        {this.state.ordertyepe === 'booking' ?
                          <div class="booking">
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Order Ticket Notes</label>
                              <Input type="text" placeholder="Order Note..." />
                            </div>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Delivery Date & Time</label>
                              <DatePicker
                                disabledDate={this.disabledStartDate}

                                format="YYYY-MM-DD"
                                value={this.state.startValue}
                                placeholder="Start"
                                onChange={this.onStartChange}
                                onOpenChange={this.handleStartOpenChange}
                                className="datapicket" />
                              <TimePicker use12Hours format="h:mm a" onChange={this.timechange} className="timepicket" />
                            </div>
                            <Checkbox >Is Door Delivery?</Checkbox>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Booking Notes</label>
                              <Input type="text" placeholder="Type instructions or notes here (optional)" />
                            </div>
                            <div style={{ marginBottom: 16, marginTop: 16 }}>
                              <label>Booking Advance</label>
                              <Input type="number" placeholder="Booking advance payment (optional)" />
                            </div>
                            <button class="bigbutton orderTicket" onClick={this.closechargeview}> Cancel </button>
                            &nbsp;
                         <button className="bigbutton chargeOrder"> Print Receipt for <Currency />0 .00 / <Currency /> {this.state.total} </button>
                          </div> : ''}
                      </Col>
                    </Col> : ''}
                  </div>
                </TabPane>

              </Tabs>
              <Modal
                title={`Update ${this.state.viewItem[0].name}`}
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
              >
                <Input type="number" onChange={this.changeNot} name="qty" value={this.state.viewItem[0].qty} />
                <div style={{ marginBottom: 16, marginTop: 16, textAlign: `center` }}>
                  <span onClick={() => this.editQuntyMinus()} className="circle"> <Icon type="minus" /> </span>  <span className="qty"> {this.state.viewItem[0].qty} </span> <span className="circle" onClick={() => this.editQuntyPlus()}> <Icon type="plus" /> </span>
                </div>
                <p>Total Price <Currency /> {this.state.viewItem[0].updatePrice} </p>

                <Radio.Group name="radiogroup" onChange={this.checkDiscount} defaultValue={this.state.viewItem[0].discountby}>
                  <Radio value={`cash`}> Cash Discount</Radio>
                  <Radio value={`percentage`}> Percentage Discount</Radio>
                </Radio.Group>
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <Input type="number" name="discount" value={this.state.viewItem[0].discoutnumber} onChange={this.changeNot} />

                  <span> {this.state.viewItem[0].discountby === "percentage" && this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice ? <small style={{ color: "red" }}> Discount cannot be more than 100%. </small> : ''}</span>
                  <span> {this.state.viewItem[0].discountby === "cash" && this.state.viewItem[0].discount > this.state.viewItem[0].updatePrice ? <small style={{ color: "red" }}> Discount cannot be greater than item price.  </small> : ''}</span>
                </div>
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label>Order Ticket Notes</label>
                  <Input type="text" placeholder="Order Note..." value={this.state.viewItem[0].note} name="note" onChange={this.changeNot} />
                </div>
                <Button className="removeitem" onClick={() => this.removeItem(this.state.viewItem[0].id)}> Remove </Button>
              </Modal>

              <Modal
                title="Modal"
                visible={this.state.orderticketshow}
                onOk={this.generateOrderTicketPrint}
                onCancel={this.onCancelorderTicket}
                okText="Create Order Ticket"
                cancelText="Cancel"
              >

                <Input type="text" readOnly value={this.state.activetablename} />
                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label>Order Ticket Notes</label>
                  <Input type="text" placeholder="Order Note..." />
                </div>

                <div style={{ marginBottom: 16, marginTop: 16 }}>
                  <label> Main Kitchen - Updated Now </label>
                  <hr />
                  <span class="active_status"></span> Added Items
                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                    {this.orderItems()}
                  </div>
                </div>

              </Modal>

              <Modal
                title="Update Customer"
                visible={this.state.customerForm}
                onOk={this.addCustomer}
                onCancel={this.handleCancel}
              >
                <Tabs defaultActiveKey="1" onChange={this.innerTabCallback}>
                  <TabPane tab="General" key="1">

                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Phone</label>
                      <Input type="text" placeholder="Customer mobile number" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Name</label>
                      <Input type="text" placeholder="Customer Name" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Customer Email</label>
                      <Input type="text" placeholder="Customer email" />
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Occupants</label>
                      <Input type="text" placeholder="Number of seats occupied" />
                    </div>

                  </TabPane>
                  <TabPane tab="Delivery" key="2">

                    <div style={{ marginBottom: 16, marginTop: 16, marginLeft: 5, marginRight: 5 }}>
                      <label>Shipping Address</label>
                      <Input type="text" placeholder="Street Address" />
                    </div>
                    <Col span={12} style={{ padding: 5 }} >
                      <label>City</label>
                      <Input type="text" placeholder="City" />
                    </Col>
                    <Col span={12} style={{ padding: 5 }} >
                      <label>Pincode</label>
                      <Input type="text" placeholder="Zipcode" />
                    </Col>
                  </TabPane>

                </Tabs>

              </Modal>

              <Modal
                title={`Update ${this.state.activetablename}`}
                visible={this.state.swaptableshow}
                onOk={this.swaptable}
                onCancel={this.handleCancel}
              >

                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                        <label> Swap to </label>
                        <Select
                          showSearch
                          className="fullfidth"
                          placeholder="Select a person"
                          optionFilterProp="children"
                          onChange={ this.tabelselect}
                          filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="Table 1">Table 1</Option>
                          <Option value="Table 2">Table 2</Option>
                          <Option value="Table 3">Table 3</Option>
                          <Option value="Table 4">Table 4</Option>
                          <Option value="Table 5">Table 5</Option>
                          <Option value="Table 6">Table 6</Option>
                          <Option value="Table 7">Table 7</Option>
                          <Option value="Table 8">Table 8</Option>
                          <Option value="Table 9">Table 9</Option>
                          <Option value="Table 10">Table 10</Option>
                          <Option value="Table 11">Table 11</Option>
                          <Option value="Table 11">Table 12</Option>
                          <Option value="Table 13">Table 13</Option>
                          <Option value="Table 14">Table 14</Option>
                          <Option value="Table 15">Table 15</Option>

                        </Select>
                     </div>
                
              </Modal>



            </Col>
          </Col>
        </Row>
      </div>
    )
  }
}
export default (Sell);