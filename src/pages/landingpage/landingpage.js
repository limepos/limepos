import React from "react";
import {
  Grid,
  Typography
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { Button} from 'antd';
// styles
import useStyles from "./styles";

// logo
import fb from "../../images/loginwithfb.png";
import glogin from "../../images/loginwithg.png";
import logo from "../../images/logo.png";

// context
import { movetologin } from "../../context/UserContext";



function Login(props) {
  var classes = useStyles();
  // local
  return (
    <Grid container className={classes.container}>
      <div className={classes.logotypeContainer}>
        <img src={logo} alt="logo" className={classes.logotypeImage} />
      
      </div>
         <div className={classes.formContainer}>
           <div className={classes.form}>
            <div className="logoitem">
             <img src={logo} alt="logo" className={classes.logotypeImage} />
              <Typography className={classes.tagline}> Sell.Manage.Engage </Typography>
          </div>
             <Button className="loginbutton" block onClick={() =>
  movetologin('/login', props.history)}> Log in</Button>

             <Button className="registerbutton" block  onClick={() =>
  movetologin('/register', props.history)}> Create Account </Button>
          </div>
            
          <Typography className="orloginwith"> OR LOG IN WITH </Typography>
          <div className="loginarea">
            <div className="loginwithfb"> 
                <img src={fb} alt="logo" className={classes.logotypeImage} />
            </div>
            <div className="loginwithg"> 
              <img src={glogin} alt="logo" className={classes.logotypeImage} />
            </div>
          </div>
          <div class="teensarey">
            <Typography className="terms"> By creating an account you agree with the Terms of uses and  Privacy Policy </Typography>
          </div>
      </div>
     
    </Grid>
  );
}

export default withRouter(Login);
