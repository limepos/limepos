import { makeStyles } from "@material-ui/styles";
import bg from "../../images/index-jumbotron@2x.png";
export default makeStyles(theme => ({
  container: {
    height: "100vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: 0,
    left: 0,
  },
  themecolor: {
    backgroundColor: "#141549",
  },
  tagline : {
    color: '#141549',
    fontWeight: 600,
  },
  logotypeContainer: {
   // background: theme.palette.primary.main,
   backgroundImage: `url(${bg})`,
   backgroundPosition: 'center',
   backgroundRepeat: 'no-repeat',
   backgroundSize: 'cover',
    width: "70%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "50%",
    },
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  logotypeImage: {
    width: 100,
    marginBottom: theme.spacing(2),
  },
  logotypeText: {
    color: "#141549",
    fontWeight: 500,
    fontSize: 54,
    [theme.breakpoints.down("md")]: {
      fontSize: 38,
    },
  },
  formContainer: {
    width: "30%",
    height: "100%",
    backgroundColor: '#fff',
    padding: 20,
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "50%",
    },
  },
  form: {
    textAlign: 'center',
  },
  loginbutton:{
    backgroundColor: 'transparent',
    color: '#525252',
    height: 49,
    fontWeight: 'bold',
    borderWidth: 2,
    borderColor: '#bfbbbb'
  },
  registerbutton:{
    backgroundColor: 'rgb(102, 254, 203)',
    color: '#bfbbbb',
    height: 49,
    fontWeight: 'bold',
    borderWidth: 2,
    borderColor: 'rgb(102, 254, 203)',
    marginTop: 50,
  },
  tab: {
    fontWeight: 400,
    fontSize: 15,
  },
  greeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
  subGreeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
  googleButton: {
    marginTop: theme.spacing(3),
    boxShadow: theme.customShadows.widget,
    backgroundColor: "white",
    width: "100%",
    textTransform: "none",
  },
  googleButtonCreating: {
    marginTop: 0,
  },
  googleIcon: {
    width: 25,
    marginRight: theme.spacing(2),
  },
  creatingButtonContainer: {
    marginTop: theme.spacing(2.5),
    height: 30,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  createAccountButton: {
    height: 30,
    textTransform: "none",
  },
  formDividerContainer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
  formDividerWord: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  formDivider: {
    flexGrow: 1,
    height: 0.5,
    backgroundColor: theme.palette.text.hint + "40",
  },
  errorMessage: {
    textAlign: "center",
  },
  textFieldUnderline: {
    "&:before": {
      borderBottomColor: theme.palette.primary.light,
    },
    "&:after": {
      borderBottomColor: theme.palette.primary.main,
    },
    "&:hover:before": {
      borderBottomColor: `${theme.palette.primary.light} !important`,
    },
  },
  textField: {
    borderBottomColor: theme.palette.background.light,
  },
  formButtons: {
    width: "100%",
    marginTop: theme.spacing(2),
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  forgetButton: {
    textTransform: "none",
    fontWeight: 400,
  },
  loginLoader: {
    marginLeft: theme.spacing(2),
  },
  copyright: {
    marginTop: theme.spacing(2),
    whiteSpace: "nowrap",
    [theme.breakpoints.up("md")]: {
      position: "absolute",
      bottom: theme.spacing(2),
    },
  },
}));
