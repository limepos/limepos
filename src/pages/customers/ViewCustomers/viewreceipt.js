import React from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Typography,
  message,
} from 'antd';

const { Text } = Typography;
const { Title } = Typography;


class Viewreceipt extends React.Component {

  state = {
    visible: false
  };

  constructor(props) {
    super(props);
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  goback = () => {
     this.props.history.goBack();
  };
 
  render() {
   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}> <span onClick={ this.goback }> Receipts  </span>  / DE10  </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
           
           <Title level={4}>Receipt Details</Title>
           <Text>Prepared by Demo</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>on Feb 13, 2020 7:31 PM</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>Main Register Register</Text>
           <div style={{ margin: '24px 0' }} />
           <Text>#DE-1UI-2005-17 - Table 3 </Text>
        
        </Col>
        <Col span={16} className="pepar">
           
           <div className="reciept_header">
                 <span className="statusTitle"> Status </span> <span className='fullfilestatus statu'>fulfilled</span> <span className='paid statu'>Paid</span> <a className="rightitem cancel" onClick={this.showModal}>Cancel</a> <a className="rightitem print">Print</a>
           </div>
           <div className="line"/>

           <table className="billview">
               <thead>
                   <tr>
                       <th>Items</th> 
                       <th>Quantity</th>
                       <th>Tax %</th>
                       <th>	Price</th>
                     </tr>
               </thead>
               <tbody>
                  <tr>
                       <td>Burger</td> 
                       <td>1</td>
                       <td>-</td>
                       <td>	₹100.00 </td>
                     </tr>
                     <tr>
                       <td>Burger</td> 
                       <td>1</td>
                       <td>-</td>
                       <td>	₹100.00 </td>
                     </tr>
                     <tr className="total">
                       <td></td> 
                       <td></td>
                       <td>Subtotal</td>
                       <td>	₹200.00 </td>
                     </tr>
                     <tr className="total">
                       <td></td> 
                       <td></td>
                       <th> <storage >Total </storage></th>
                       <th>	<storage> ₹200.00 </storage></th>
                     </tr>
               </tbody>
           </table>
           <div className="line"/>
           <span className="statusTitle"> Status </span> <a> #7 </a>
           <div className="line"/>
           <span className="statusTitle"> Customer Details </span> &nbsp; <a> Customer | 9713265840 </a>
               <br/>
               <br/>
               <span> ₹100.00 on Cash </span>
                <br/>
                <br/>
               <span> Feb 13, 2020, 7:32 PM </span>
            
      </Col>
    </Col>
      </Row>
      <Modal
          title="Confirm Cancel"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          okText = "Cencel Receipt"
          cancelText=  'Go Back'
        > 
            <div style={{ marginBottom: 16, marginTop: 16 }}>
                            <label>Enter Refund Amount</label>
                            <Input type="number" placeholder="Refund Amount" />
                          </div>
                          <div style={{ marginBottom: 1, marginTop: 16 }}>
                            <label> Refund Payment Type </label>
                          </div>
                          <div style={{ marginBottom: 16, marginTop: 0 }}>
                            <Button className="payment_type"> Cash</Button>
                            <Button className="payment_type"> Credit / Debit Card </Button>
                            <Button className="payment_type"> Other </Button>
                            
                          </div>
                          <div style={{ marginBottom: 16, marginTop: 16 }}>
                            <label>Cancellation Notes</label>
                            <Input type="text" placeholder="Cancel Note." />
                          </div>
                   
        </Modal>
    </div>
    );
  }
}
export default Form.create()(Viewreceipt);