import React from 'react';
import { Table, Input, message } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
const { Search } = Input;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    mobile: `974653250${i}`,
    name : 'Customer',
    ordercount: `5`,
    orderprice: `₹100.00`,
    lastseen : `Feb 13, 2020, 7:32 PM`,
    
  });
}



class AllReceipts extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
      {
        title: 'Mobile',
        dataIndex: 'mobile',
        key: 'mobile'  
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Order Count',
        dataIndex: 'ordercount',
        key: 'ordercount',
      },
      {
        title: 'Order Value',
        dataIndex: 'orderprice',
        key: 'orderprice',
      },
      {
        title: 'Last Seen',
        dataIndex: 'lastseen',
        key: 'lastseen',
      }
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
     
  start = () => {
    this.setState({ loading: true });
    // ajax request after empty completing
    setTimeout(() => {
      this.setState({
        selectedRowKeys: [],
        loading: false,
        actiontitle : 'Action'

      });
      message.success('Record Deleted Successfull.');
    }, 1000);
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });

    console.log("SElectd=>",selectedRowKeys.length)
    if (selectedRowKeys.length > 0) {
         this.setState({ actiontitle : 'Delete Selected ' });
    } else {
          this.setState({ actiontitle : 'Action' });
    }

  };

  handleButtonClick = (e) => {
    message.info('Click on left button.');
    console.log('click left button', e);
  }
  
  handleMenuClick = (e) => {
    message.info('Click on menu item.');
    console.log('click', e);
  }
   
  addnewRegister = (id) => {
    this.props.history.history.push(`/app/view/receipts/${id}`);
  }

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

   changeDate = (date, dateString) =>{
    console.log(date, dateString);
  }

  render() {
    
    const { selectedRowKeys } = this.state;
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    
                    
                     <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" /> 
                  
                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table  columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (AllReceipts);