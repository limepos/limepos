import React from 'react';
import { Modal, Typography, Button, Tabs, Row, Col, Input} from 'antd';
import Grid from '@material-ui/core/Grid';
import AllCustomer from './AllCustomers/allCustomers';
const { Title } = Typography;
const { TabPane } = Tabs;

class Customers extends React.Component {

  constructor(props) {
    super(props);
  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-categories/1'){
     
    this.state.current_tab = '1';

  } 
  if( currantbackurl === '/app/setup/product-categories/2'){
    this.state.current_tab = '2';
    
    
  }
  }

  state = {
     current_tab: '1',
     visible: false
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })

  }
  

  exportProuct = () => {
    this.setState({
      visible: true,
    });
  }
  import = () => {
    this.props.history.push(`/app/Product/import`);
  }

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Customers </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                    <Button type="" onClick={ this.exportProuct }  className=""> Export </Button>
                    &nbsp;
                    <Button type="" onClick={ this.import }  className=""> Import </Button>
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                        <TabPane tab="All Customers"  key="1">
                              <AllCustomer history={this.props}/>
                       </TabPane>
                    </Tabs>
                  </Col> 
                </Col>
                <Modal
                title="Request a Report"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                > 
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Send To Email Address</label>
                      <Input type="email" placeholder="Email" />
                    </div>
        </Modal>
              </Row>     
            </div>
    )
  }
}
export default (Customers);