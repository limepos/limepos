import React from 'react';
import { Modal, Typography, Button, Tabs, Row, Col, Select, Input, DatePicker,Checkbox} from 'antd';
import Grid from '@material-ui/core/Grid';
import AllReceipts from './AllReceipts/allReceipts';
const { Title } = Typography;
const { TabPane } = Tabs;
const { Option } = Select;
const { RangePicker } = DatePicker;

class Receipts extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-categories/1'){
     
    this.state.current_tab = '1';
  
     console.log("current tab", this.state.current_tab)

  } 
  if( currantbackurl === '/app/setup/product-categories/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
    
  }
  }

  state = {
     current_tab: '1',
     visible: false,
     datetype : '',
     sendonEmail : false,
     mailcheck: false,
     download : false
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,

    })

  }
   
  addnewRegister = (tab) => {
  }

  import = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
      sendonEmail : false,
      mailcheck: false,
      download : false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  dataRange = (v) => {
    console.log(v)
    this.setState({
      datetype : v 
    })
  }
  ondownload=(e)=>{
   this.setState({
     download : !this.state.download
   })
 }

  sendOnemail=(e)=>{
        this.setState({
          sendonEmail : e.target.checked,
          mailcheck: !this.state.mailcheck
        })
  }
  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={6} className="pageTitle">
                    <Title level={4}> Receipts </Title>  
                </Grid>
                <Grid item xs={6} className="rightButton">
                    <Button type="" onClick={ this.import }  className=""> Export </Button>
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                        <TabPane tab="All  "  key="1">
                              <AllReceipts history={this.props}/>
                       </TabPane>
                       <TabPane tab="Unfulfilled" key="2">
                              <AllReceipts history={this.props}/>
                           </TabPane>
                      <TabPane tab="Unpaid"  key="3">
                           <AllReceipts history={this.props}/>
                      </TabPane>
                    </Tabs>
                  </Col> 
                </Col>
                <Modal
          title="Request a Report"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
                   <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Choose Report Type</label>
                      <Select
                          showSearch
                          className="fullfidth"
                          optionFilterProp="children"
                          placeholder="Choose Report Type"
                          onChange={ this.tabelselect}
                          filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="Sales report"> Sales report </Option>
                          <Option value=" Payment report"> Payment report </Option>
                          <Option value=" Daily sales and payment report"> Daily sales and payment report</Option>
                          <Option value="Product wish sales report"> Product wish sales report </Option>
                          <Option value="Shift open / Close report"> Shift open / Close report </Option>
                          <Option value="Order ticket report"> Order ticket report  </Option>

                        </Select>
                    </div>

                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Date Range</label>
                      <Select
                          showSearch
                          className="fullfidth"
                          optionFilterProp="children"
                          placeholder="Choose Date Range"
                          onChange={ this.dataRange}
                          filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="Today"> Today </Option>
                          <Option value="Yesterday"> Yesterday </Option>
                          <Option value="This Month "> This Month </Option>
                          <Option value="Last Month"> Last Month </Option>
                          <Option value="CustomSelection"> Custom Selection </Option>
                         </Select>
                    </div>

                    {this.state.datetype === "CustomSelection" ? <div style={{ marginBottom: 16, marginTop: 16 }}>
                       <RangePicker />
                         </div> : ''}

                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                      <label>Choose A Register</label>
                      <Select
                          showSearch
                          className="fullfidth"
                          optionFilterProp="children"
                          placeholder="Choose A Register"
                          onChange={ this.tabelselect}
                          filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          <Option value="All"> All </Option>
                          <Option value="Main Register"> Main Register </Option>
                          <Option value="Test"> Test </Option>
                         </Select>
                    </div>
                    <div style={{ marginBottom: 16, marginTop: 16 }}>
                       <Checkbox  onChange={this.ondownload}  checked={ this.state.download } >Download report on local drive </Checkbox>
                    </div>
                     <div style={{ marginBottom: 16, marginTop: 16 }}>
                     <Checkbox onChange={this.sendOnemail} checked={ this.state.mailcheck } value="email">Send To Email Address  </Checkbox>
                    
                      {this.state.sendonEmail ? <Input type="email" placeholder="Email*" /> : '' }
                    </div>
        </Modal>
              </Row>     
            </div>
    )
  }
}
export default (Receipts);