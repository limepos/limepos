import React from 'react';
import { Typography, Row, Col} from 'antd';
import Grid from '@material-ui/core/Grid';
import MoneyIcon from '@material-ui/icons/Money';
import ReceiptIcon from '@material-ui/icons/Receipt';
import PeopleIcon from '@material-ui/icons/People';
const { Title } = Typography;
class Dashboard extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
     current_tab: '1',
     visible: false,
     showExpenseform : false,
  }

  componentDidMount(){
    console.log("did" )  
  }
  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={12} className="pageTitle">
                    <Title level={4}> Dashboard </Title>  
                </Grid>
                
              </Grid>  
              <Row gutter={[15, 15]}>
                      <Col span={24}>
                         <Title className="welcome">Hello Demo, here's how today is.</Title>
                      </Col>
                      <Col span={8} >
                            <div className="widget"> <MoneyIcon /> <br/>  $ 0.00 <br/> Total sales </div> 
                      </Col> 
                      <Col span={8} >
                      <div className="widget"> <ReceiptIcon /> <br/> 0 <br/> Total bills </div> 
                      </Col> 
                      <Col span={8}>
                      <div className="widget"> <PeopleIcon/> <br/>  0 <br/> New customers </div> 
                      </Col> 
              </Row>
            </div>
    )
  }
}
export default (Dashboard);