import React, { useState } from "react";
import {
  Grid,
  CircularProgress,
  Typography,
  Fade,
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

import { Button} from 'antd';

import ArrowBackIcon from '@material-ui/icons/ArrowBack';
// styles
import useStyles from "./styles";
import logo from "../../images/logo.png";
// context
import { useUserDispatch, loginUser, back } from "../../context/UserContext";

function Login(props) {
  var classes = useStyles();
  // global
  var userDispatch = useUserDispatch();

  // local
  var [isLoading, setIsLoading] = useState(false);
  var [error, setError] = useState(null);
  var [loginValue, setLoginValue] = useState("");
  var [passwordValue, setPasswordValue] = useState("");
  return (
    <Grid container className={classes.container}>
       
      <div className={classes.logotypeContainer}>
        <img src={logo} alt="logo" className={classes.logotypeImage} />
       
      </div>
      <div className={classes.formContainer}>
        <div className="back" onClick={() =>
                      back(props.history) }> <ArrowBackIcon/></div>
        <div className={classes.form}>
       
              <Typography variant="h3" className={classes.greeting}>
                Welcome to The POS
              </Typography>
              
              <Fade in={error}>
                <Typography color="secondary" className={classes.errorMessage}>
                  Something is wrong with your login or password
                </Typography>
              </Fade>
              <ValidatorForm>   
                                    <TextValidator
                                    label="Email"
                                    onChange={e => {setLoginValue(e.target.value)} }
                                    name="email"
                                    value={loginValue}
                                    validators={['required', 'isEmail']}
                                    errorMessages={['this field is required', 'email is not valid']}
                                    fullWidth
                                    InputProps={{
                                        classes: {
                                          underline: classes.textFieldUnderline,
                                          input: classes.textField,
                                        },
                                      }}
                                    />
                                    <TextValidator
                                    label="Password"
                                    onChange={e => setPasswordValue(e.target.value)}
                                    name="password"
                                    value={passwordValue}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                    fullWidth
                                     type="password"
                                    InputProps={{
                                        classes: {
                                          underline: classes.textFieldUnderline,
                                          input: classes.textField,
                                        },
                                      }}
                                    />
              <div>
                {isLoading ? (
                  <CircularProgress size={26} className={classes.loginLoader} />
                ) : (
                  <Button
                   block
                   
                    onClick={() =>
                      loginUser(
                        userDispatch,
                        loginValue,
                        passwordValue,
                        props.history,
                        setIsLoading,
                        setError,
                      )
                    }
                    variant="contained"
                    className ="logintransparent"
                    size="large"
                  >
                    Login 
                  </Button>
                )}
               
              
               <Typography className="tagline">
               Forgot Password ?
              </Typography>
              </div>
              </ValidatorForm>
        </div>
      </div>
    </Grid>
  );
}

export default withRouter(Login);
