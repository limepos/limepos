import React, { useState } from 'react';

import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import {
    Grid,
    CircularProgress,
    Typography,
    Tabs,
    Tab,
    TextField,
    Fade,
  } from "@material-ui/core";
  import { Button} from 'antd';
  import classnames from "classnames";
  import ArrowBackIcon from '@material-ui/icons/ArrowBack';
  import { withStyles } from '@material-ui/styles';
// styles
//import useStyles from "./styles";

import logo from "./logo.svg";
import google from "../../images/google.svg";
import bg from "../../images/index-jumbotron@2x.png";
// context
import { useUserDispatch, loginUser, RegisterUser, back } from "../../context/UserContext";

const styles = theme => ({
    container: {
        height: "100vh",
        width: "100vw",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        top: 0,
        left: 0,
      },
      themecolor: {
        backgroundColor: "#141549",
      },
      logotypeContainer: {
       // background: theme.palette.primary.main,
       backgroundImage: `url(${bg})`,
       backgroundPosition: 'center',
       backgroundRepeat: 'no-repeat',
       backgroundSize: 'cover',
        width: "70%",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down("md")]: {
          width: "50%",
        },
        [theme.breakpoints.down("md")]: {
          display: "none",
        },
      },
      logotypeImage: {
        width: 100,
        marginBottom: theme.spacing(2),
      },
      logotypeText: {
        color: "#141549",
        fontWeight: 500,
        fontSize: 54,
        [theme.breakpoints.down("md")]: {
          fontSize: 38,
        },
      },
      formContainer: {
        width: "30%",
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        [theme.breakpoints.down("md")]: {
          width: "50%",
        },
      },
      form: {
        width: 320,
      },
      tab: {
        fontWeight: 400,
        fontSize: 15,
      },
      greeting: {
        fontWeight: 500,
        textAlign: "center",
        marginTop: theme.spacing(2),
      },
      subGreeting: {
        fontWeight: 500,
        textAlign: "center",
        marginTop: theme.spacing(2),
      },
      googleButton: {
        marginTop: theme.spacing(3),
        boxShadow: theme.customShadows.widget,
        backgroundColor: "white",
        width: "100%",
        textTransform: "none",
      },
      googleButtonCreating: {
        marginTop: 0,
      },
      googleIcon: {
        width: 25,
        marginRight: theme.spacing(2),
      },
      creatingButtonContainer: {
        marginTop: theme.spacing(2.5),
        height: 30,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
      createAccountButton: {
        height: 30,
        textTransform: "none",
      },
      formDividerContainer: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        display: "flex",
        alignItems: "center",
      },
      formDividerWord: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
      },
      formDivider: {
        flexGrow: 1,
        height: 0.5,
        backgroundColor: theme.palette.text.hint + "40",
      },
      errorMessage: {
        textAlign: "center",
      },
      textFieldUnderline: {
        "&:before": {
          borderBottomColor: theme.palette.primary.light,
        },
        "&:after": {
          borderBottomColor: theme.palette.primary.main,
        },
        "&:hover:before": {
          borderBottomColor: `${theme.palette.primary.light} !important`,
        },
      },
      textField: {
        borderBottomColor: theme.palette.background.light,
        width:'100%',
      },
      formButtons: {
        width: "100%",
        marginTop: theme.spacing(2),
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
      },
      forgetButton: {
        textTransform: "none",
        fontWeight: 400,
      },
      loginLoader: {
        marginLeft: theme.spacing(2),
      },
      copyright: {
        marginTop: theme.spacing(2),
        whiteSpace: "nowrap",
        [theme.breakpoints.up("md")]: {
          position: "absolute",
          bottom: theme.spacing(2),
        },
      },
  });
  
class Login extends React.Component {
    state = {
        formData: {
            email: '',
            password: ''
          },
        submitted: false,
        error: false,
        loginValue: '',
        passwordValue : '',
        setIsLoading : false,
       // setError : false,
        isLoading: false,
      
    }

    constructor(props) {
        super(props);
        
      }

    handleChange = (event) => {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }

    handleSubmit = () => {
        this.setState({ submitted: true }, () => {
            setTimeout(() => this.setState({ submitted: false }), 5000);
        });
    }

    render() {
        const { formData, submitted } = this.state;
        const { classes } = this.props;
       
        //console.log(classes);
        return (
            <Grid container className={classes.container}>
                        <div className={classes.logotypeContainer}>
                            <img src={logo} alt="logo" className={classes.logotypeImage} />
                            <Typography className={classes.logotypeText}> Lime POS </Typography>
                        </div>
                        <div className={classes.formContainer}>
                            <div className="back" onClick={() =>
                                        back(this.props.history) }> <ArrowBackIcon/></div>
                            <div className={classes.form}>
                        
                                <Typography variant="h3" className={classes.greeting}>
                                    Welcome to Lime POS
                                </Typography>
                                
                                <Fade in={this.state.error}>
                                    <Typography color="secondary" className={classes.errorMessage}>
                                    Something is wrong with your login or password :(
                                    </Typography>
                                </Fade>
                                <ValidatorForm   ref="form"  onSubmit={this.handleSubmit} >   
                                    <TextValidator
                                    label="Email"
                                    onChange={this.handleChange}
                                    name="email"
                                    value={formData.email}
                                    validators={['required', 'isEmail']}
                                    errorMessages={['this field is required', 'email is not valid']}
                                    fullWidth
                                    InputProps={{
                                        classes: {
                                          underline: classes.textFieldUnderline,
                                          input: classes.textField,
                                        },
                                      }}
                                    />
                                    <TextValidator
                                    label="Password"
                                    onChange={this.handleChange}
                                    name="password"
                                    value={formData.password}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                    fullWidth
                                     type="password"
                                    InputProps={{
                                        classes: {
                                          underline: classes.textFieldUnderline,
                                          input: classes.textField,
                                        },
                                      }}
                                    />
                                <div>
                                    { this.state.isLoading ? (
                                    <CircularProgress size={26} className={classes.loginLoader} />
                                    ) : (
                                    <Button
                                    block
                                    
                                        onClick={() =>
                                        loginUser(
                                            useUserDispatch,
                                            this.state.formData.email,
                                            this.state.formData.password,
                                            this.props.history,
                                         )
                                        }
                                        variant="contained"
                                        className ="logintransparent"
                                        size="large"
                                    >
                                        Login 
                                    </Button>
                                    )}
                                
                                
                                <Typography className="tagline">
                                Forget Password
                                </Typography>
                                </div>
                                </ValidatorForm>
                            </div>
                        </div>
            </Grid>
        );
    }
}
export default withStyles(styles)(Login);