import React, { useState } from "react";
import {
  Grid,
  Typography,
  Button,
  Tabs,
  Tab
} from "@material-ui/core";
import { Radio } from 'antd';
import { withRouter } from "react-router-dom";
import useStyles from "./styles";

// logo
// import logo from "../../images/logo_dummy.png";
import logo from "../../images/logo.png";
import retail from "../../images/retail-icon-3.png";
import restaurent from "../../images/retail-icon-4.png";
import ArrowForwardIosIcon from '@material-ui/icons/ArrowRight';

// context
import { useModeDispatch, checkMode } from "../../context/ModeContext";

function Account(props) {
  var classes = useStyles();

  // global
  var modeDispatch = useModeDispatch();

  // local
  var [modeValue, setModeValue] = useState("retaile_mode");

  var [activeTabId, setActiveTabId] = useState(0);

  return (
    <Grid container className={classes.container}>
      <div className={classes.logotypeContainer}>
        <img src={logo} alt="logo" className={classes.logotypeImage} />
      </div>
      <div className={classes.formContainer}>
        <div className={classes.form}>
       
         <Typography variant="h3" className={classes.greeting}>
           MODE SELECTION
         </Typography>
         <Typography variant="h1" className={classes.greeting}>
           &nbsp;
         </Typography>
          <Tabs
            value={activeTabId}
            onChange={(e, id) => setActiveTabId(id)}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab  icon={<img src={retail} alt="logo" className={classes.logotypeImage} />}  label="RETAIL MODE" classes={{ root: classes.tab }} />
            <Tab  icon={<img src={restaurent} alt="logo" className={classes.logotypeImage} />}  label="RESTAURANT MODE" classes={{ root: classes.tab }} />
          </Tabs>
          {activeTabId === 0 && (
            <React.Fragment>
            <div className="pad">
            <Radio.Group defaultValue="retaile_mode" onChange={onChange} buttonStyle="solid">
              <Radio.Button value="retaile_mode"> Inventory Mode </Radio.Button>
            </Radio.Group>
              
              <Button size="large" color="theme" className={classes.googleButton} onClick={ () => switchMode() } >
               ENTER <ArrowForwardIosIcon/>
              </Button>
              </div>
            </React.Fragment>
          )}
          {activeTabId === 1 && (
            <React.Fragment>
            <div className="pad">
            <Radio.Group onChange={onChange} buttonStyle="solid">
              <Radio.Button value="kitchen_mode"> KITCHEN DISPLAY SYSTEM</Radio.Button>
              <Radio.Button value="waiter_mode"> WAITER MODE</Radio.Button>
              <Radio.Button value="cashier_mode"> CASHIER MODE</Radio.Button>
          </Radio.Group>
              
              <Button size="large" color="theme" className={classes.googleButton}  onClick={ () => switchMode() } >
                ENTER <ArrowForwardIosIcon/>
              </Button>
          </div>
              </React.Fragment>
          )}
        </div>
        <Typography color="primary" className={classes.copyright}>
          © 2020 All rights reserved.
        </Typography>
      </div>
    </Grid>
  );
  
  function onChange(e) {

    setModeValue(e.target.value);
      console.log(`radio checked:${e.target.value}`);
  }

  function switchMode() {

      console.log("modevalue", modeValue);
      checkMode(modeValue, modeDispatch, props.history);
     
    //props.history.push('/app/dashboard')
  }


}

export default withRouter(Account);
