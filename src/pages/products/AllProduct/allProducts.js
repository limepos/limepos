import React from 'react';
import { Table, Input, DatePicker, Modal } from 'antd';
import tableSearch from '../../../components/TableSearch/tableSearch';
import burger from '../../../images/products/burger.jpg';
const { Search } = Input;
const { RangePicker} = DatePicker;
const { confirm } = Modal;

const data = [];
for (let i = 0; i < 20; i++) {
  data.push({
    key: i,
    name: `Burger`,
    variantgroups : '-',
    addongroups: `-`,
    productcategory: "General",
    taxgroup : "Zero Tax Group ",
    productprice : '₹100.00'
  });
}



class Allproducts extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    selectedRowKeys: [], // Check here to configure the default column
    loading: false,
    actiontitle : "Action",
    columns : [
       {
        title: '',
        dataIndex: 'pic',
        key: 'pic',
        render: (text, record) => (
          <span>
            <img src={ burger } className="table_image" onClick={() => this.editProudct(record.key) }/> 
          </span>
        ),
      },
      {
        title: 'Product Name',
        dataIndex: 'name',
        key: 'name',
        render: (text, record) => (
          <span>
            <a  onClick={() => this.editProudct(record.key) }> {record.name} </a>
          </span>
        ),
      },
      {
        title: 'Variant Groups',
        dataIndex: 'variantgroups',
        key: 'variantgroups',
      },
      {
        title: 'Addon Groups',
        dataIndex: 'addongroups',
        key: 'addongroups',
      },
      {
        title: 'Product Category',
        dataIndex: 'productcategory',
        key: 'productcategory',
      },
      {
        title: '	Tax Group',
        dataIndex: 'taxgroup',
        key: 'taxgroup',
      },
      {
        title: 'Product Price',
        dataIndex: 'productprice',
        key: 'productprice',
      }
  
    ],
    bookdata : data,
    displaybookdata: data, 
    sortName: '',
    sortValue: ''
  };
     

  finder = (e) =>{

    let searchtext = e.target.value
    let columns = this.state.columns
    let bookdata = this.state.bookdata
    
   let r = tableSearch(searchtext,bookdata, columns);
    //console.log("Response", r)
    this.setState({
      displaybookdata: r
    })

  }

   changeDate = (date, dateString) =>{
    console.log(date, dateString);
  }

  deleteexpences =() =>{

    confirm({
      title: 'Confirm Cancellation',
      content: 'Are you sure you want to cancel this expense?',
      okText: 'Cancel Expenses',
      okType: 'danger',
      cancelText: 'Back',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });

  }
  
  editProudct = (id) => {
    this.props.history.history.push(`/app/Product/Edit/${id}`);
  }
  


  render() {
    
    const { selectedRowKeys } = this.state;
 
    const hasSelected = selectedRowKeys.length > 0;

  
    return (
            <div>
                <div style={{ marginBottom: 16 }}>
                    
                     <RangePicker onChange={this.changeDate} className="searchBox " />
                     <Search placeholder="input search text" onChange={e => this.finder(e) }  className="searchBox" /> 
                  
                    <span style={{ marginLeft: 8 }}>
                     {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                    </span>
                </div>
                 <Table  columns={this.state.columns} dataSource={this.state.displaybookdata} />
            </div>
    )
  }
}
export default (Allproducts);