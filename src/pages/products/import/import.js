import React from 'react';
import {Typography, Row, Col, Upload, message,Icon} from 'antd';
const { Title } = Typography;
const { Dragger } = Upload;

const props = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

class ProductOption extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
     current_tab: '1',
     visible: false,
     confirmLoading: false,
  }

  componentDidMount(){

   // this.props.history
   
    console.log("did" )  

  }
  
  goback = () => {
    this.props.history.goBack();
 };
  render() {
    
    return (
            <div hi="s">
                <Row gutter={[25, 25]}>
                    <Col span={24}>
                        <Title level={4}> <span onClick={ this.goback }> Products </span> / Import </Title>  
                    </Col>
                </Row>
              
            
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Dragger {...props}>
                        <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                        </p>
                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                        
                    </Dragger>
                  </Col> 
                </Col>
                
              </Row>     
            </div>
    )
  }
}
export default (ProductOption);