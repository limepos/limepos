import React from 'react';
import {
  Form,
  Input,

  Select,
  Row,
  Col,
  Button,
  Typography,
  Upload,
  message, 
   
} from 'antd';
import dummy from "../../../images/4728.png";

const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;



function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
 

class EditProduct extends React.Component {

  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    loading: false,
    value: '',
  };

  constructor(props) {
    super(props);
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  handleWebsiteChange = value => {
    let autoCompleteResult;
    if (!value) {
      autoCompleteResult = [];
    } else {
      autoCompleteResult = ['.com', '.org', '.net'].map(domain => `${value}${domain}`);
    }
    this.setState({ autoCompleteResult });
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  goback = () => {
     this.props.history.goBack();
  };
  onChange = ({ target: { value } }) => {
    this.setState({ value });
  };

   handleChange = (value) => {
    console.log(`selected ${value}`);
  }

  render() {
    const { imageUrl } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
    };
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={24}>
             <Title level={4}>  <span onClick={ this.goback }> Products  </span> / New </Title>  
          </Col>
        </Row>

      <Row gutter={[50, 50]}>
      <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Your Product Details</Title>
           <Text>Edit your product details here. Product name should be unique.</Text>
        </Col>
        <Col span={16} className="pepar">

        <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={this.handleChange}
      >
        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : <img src={dummy} alt="avatar" style={{ width: '100%' }} />}
      </Upload>   
       <Text> Upload Product Image. It should be a square image.</Text>
       <br/>
       <Text> &nbsp; </Text>

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
        <Form.Item label="Product Name" {...formItemLayout}>
            {getFieldDecorator('name', {
            rules: [
              {
                required: true,
                message: 'Please enter name',
              },
            ],
          })(<Input  placeholder="Product Category Name" />)}
        </Form.Item>

        <Form.Item label="Product Category" {...formItemLayout}>
        { getFieldDecorator('category', {
            rules: [
              {
                required: true,
                message: 'Please Select Category.',
              },
            ],
          })(<Select nstyle={{ width: '100%' }}  placeholder="Product Category">
            <Option value="general"> General </Option>
          </Select>) }
        </Form.Item>

        <Form.Item label="Tax Group" {...formItemLayout}>
        { getFieldDecorator('taxgroup', {
            rules: [
              {
                required: true,
                message: 'Please select tax group.',
              },
            ],
          })(<Select nstyle={{ width: '100%' }}  placeholder="Tax Group">
            <Option value="general"> Zero tax group </Option>
          </Select>) }
        </Form.Item>

        <Form.Item label="Product Price" {...formItemLayout}>
            {getFieldDecorator('price', {
            rules: [
              {
                required: true,
                message: 'Please enter price',
              },
            ],
          })(<Input type="number" placeholder="Price" />)}
        </Form.Item>

        <Form.Item >
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>
    <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Product Options</Title>
           <Text>You can add one ore more variant groups and an add-on group to the product.</Text>
        </Col>
        <Col span={16} className="pepar">

      <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
       
      <Form.Item label="Add variant group" {...formItemLayout}>
        { getFieldDecorator('varintgroup', {
            rules: [
              {
                required: true,
                message: 'Please Select variant group.',
              },
            ],
          })(<Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Please select"
            defaultValue={['a10', 'c12']}
            onChange={this.handleChange}
          >
            {children}
          </Select>) }
        </Form.Item>

        <Form.Item label="Add addon group" {...formItemLayout}>
        { getFieldDecorator('addongroup', {
            rules: [
              {
                required: true,
                message: 'Please Select addon group.',
              },
            ],
          })(<Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Please select"
            defaultValue={['a10', 'c12']}
            onChange={this.handleChange}
          >
            {children}
          </Select>) }
        </Form.Item>

        <Form.Item >
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>

    <Col span={24}>
        <Col span={8}>
          
           <Title level={4}>Product Additional Details</Title>
           <Text>Additional details about the product.</Text>
        </Col>
        <Col span={16} className="pepar">

       <Form layout="vertical" {...formItemLayout} onSubmit={this.handleSubmit} >
       
         <Form.Item label="Unit of Measure" {...formItemLayout}>
              <Input type="text" placeholder="Unit of Measure" />
        </Form.Item>

        <Form.Item label="Notes " {...formItemLayout}>
              <Input type="text" placeholder="Note." />
        </Form.Item>

        <Form.Item label="Limit to Register " {...formItemLayout}>
          <Select
            style={{ width: '100%' }}
            placeholder="Please select"
             onChange={this.handleChange}
          >
            <Option value="All">All Register</Option>
            <Option value="Main">Main Register</Option>
          </Select>
        </Form.Item>

        <Form.Item label="Sort Order " {...formItemLayout}>
              <Input type="text" placeholder="Sort Order" />
        </Form.Item>

        <Form.Item >
           <Button type="default" onClick={ this.goback }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </Form.Item>
      </Form>
      </Col>
    </Col>

      </Row>
    </div>
    );
  }
}
export default Form.create()(EditProduct);