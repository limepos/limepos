import React from 'react';
import { Modal, Typography, Button, Tabs, Row, Col, Input } from 'antd';
import Grid from '@material-ui/core/Grid';
import AllProduct from './AllProduct/allProducts';
const { Title } = Typography;
const { TabPane } = Tabs;
class Receipts extends React.Component {

  constructor(props) {
    super(props);

  console.log("constr")

  console.log("History -->", this.props.history.location.pathname)

  const currantbackurl = this.props.history.location.pathname;

  if( currantbackurl === '/app/setup/product-categories/1'){
     
    this.state.current_tab = '1';
  
     console.log("current tab", this.state.current_tab)

  } 
  if( currantbackurl === '/app/setup/product-categories/2'){
    this.state.current_tab = '2';
    console.log("current tab", this.state.current_tab)
    
  }
  }

  state = {
     current_tab: '1',
     visible: false,
     showExpenseform : false,
  }

  componentDidMount(){

   // this.props.history
    console.log("did" )  
  }

  callback = key => {
   //  console.log(key)

    this.setState({
      current_tab: key,
    })

  }
   
  addnewRegister = (tab) => {

  }

  import = () => {
  
    this.props.history.push("/app/Product/import");

  }

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
      showExpenseform :false
    });
  }
  openexpenseform = () => {
    this.setState({
      showExpenseform :true
    })
    }

    addProuducts = () =>{
      this.props.history.push("/app/add-products");
    }

   addExpense =() =>{
     this.setState({
      showExpenseform :false
     })
   } 

   newProudct = () => {
    this.props.history.push(`/app/product/new`);
   }

  render() {
    
    return (
            <div hi="s">
              <Grid container spacing={3}>
                <Grid item xs={3} className="pageTitle">
                    <Title level={4}> Products </Title>  
                </Grid>
                <Grid item xs={9} className="rightButton">
                   <Button type="" onClick={ this.openexpenseform }  className=""> Export </Button>  <Button type="" onClick={ this.import }  className=""> Import </Button> <Button type="primary" onClick={() =>  this.addProuducts()}  className="primarybutton"> Quick Add Products  </Button> <Button type="primary" onClick={() =>  this.newProudct()}  className="primarybutton">Add Single Product  </Button>
                </Grid>
              </Grid>  
              <Row gutter={[50, 50]}>
               <Col span={24}>
                 <Col span={24} className="pepar">
                    <Tabs defaultActiveKey={this.state.current_tab} onChange={this.callback}>
                        <TabPane tab="All Products"  key="1">
                              <AllProduct history={this.props}/>
                       </TabPane>
                      </Tabs>
                  </Col> 
                </Col>
                  <Modal
                  title="Export Products"
                  visible={this.state.showExpenseform }
                  onOk={this.addExpense}
                  onCancel={this.handleCancel}
                  okText = "Submit"
                  >
                  <div style={{ marginBottom: 16, marginTop: 16 }}>
                    <label>Send To Email Address</label>
                      <Input type="Email" placeholder="Email" />
                  </div>
                  </Modal>
              </Row>     
            </div>
    )
  }
}
export default (Receipts);