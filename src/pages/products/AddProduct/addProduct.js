import React from 'react';
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Icon,
  Typography
} from 'antd';

let id = 0;
const { Text } = Typography;
const { Title } = Typography;
const { Option } = Select;

class Addproduct extends React.Component {

  state = {
    value: '',
    name : '',
    price : '',
    tax: '',
    category : '',
    error :false
  };

  constructor(props) {
    super(props);
  }
 
  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');

    this.props.form.validateFields((err, values) => {
        if (!err) {
                const nextKeys = keys.concat(id++);
                // can use data-binding to set
                // important! notify form to detect changes
                form.setFieldsValue({
                keys: nextKeys,
                });
        }else{

            console.log("Error");
        }
      });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { keys, names } = values;
        console.log('Received values of form: ', values);
        console.log('Merged values:', keys.map(key => names[key]));
      }
    });
  };

  selectCategory = (value) => {
    console.log(`selected ${value}`);
  }

  setupProductCategory =()=> {
    this.props.history.push("/app/setup/product-categorie/new-category");
  }
  setupTaxGroup = () => {
    this.props.history.push("/app/setup/tax/addtaxgroup");
  }

  render() {

    
    const { goBack } = this.props.history;
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 4 },
      },
    };

     getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <Row gutter={[10,10]}> 
      <Col span={6}> 
          <Form.Item
          {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
          required={false}
          key={k}
        >
          {getFieldDecorator(`prouctname[${k}]`, {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Please enter product name",
              },
            ],
          })(<Input placeholder="Product Name" />)}
        </Form.Item> 
     </Col>
      <Col span={6}> 
      <Form.Item
        {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        required={false}
        key={k}
      >
        {getFieldDecorator(`category[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
                required: true,
                whitespace: true,
                message: "Please select category",
            },
          ],
        })(<Select
            showSearch
            
            placeholder="Select a Category"
            optionFilterProp="children"
            
          filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="General">General</Option>
            <Option value="Hot">Hot</Option>
            <Option value="Regular">Regular</Option>
          </Select>)}
      
      </Form.Item>
      </Col>
      <Col span={6}>
        <Form.Item
        {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
        required={false}
        key={k}
      >
        {getFieldDecorator(`taxgroup[${k}]`, {
          validateTrigger: ['onChange', 'onBlur'],
          rules: [
            {
              required: true,
              whitespace: false,
              message: "Please select tax Group",
            },
          ],
        })(<Select
            showSearch
            
            placeholder="Select a Tax Group"
            optionFilterProp="children"
           
          filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="General">Zero Tax Group </Option>
            <Option value="Hot"> 5 %</Option>
            <Option value="Regular"> 10 % </Option>
          </Select>)}
      </Form.Item>
      </Col>
      <Col span={4}> <Form.Item
      {...(index >= 0 ? formItemLayout : formItemLayoutWithOutLabel)}
      required={false}
      key={k}
    >
      {getFieldDecorator(`price[${k}]`, {
        validateTrigger: ['onChange', 'onBlur'],
        rules: [
            {
              required: true,
              whitespace: false,
              message: "Please Enter Price"
            }]
      })(<Input placeholder="Price" />)}
    </Form.Item> </Col>
      <Col span={2}> 
      {keys.length > 1 ? (
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => this.remove(k)}
        />
      ) : null}
   </Col>
   </Row>    
    ));
   
    return (
      <div>
       
       <Row gutter={[25, 25]}>
          <Col span={12} className="pageTitle">
             <Title level={4}>  <span onClick={ () => goBack() }>   Products </span> / Quick Add </Title>  
          </Col>
          <Col span={12} className="rightButton">
              <Button type="" onClick={this.setupProductCategory}  className=""> Setup Product Category  </Button> <Button type="" onClick={ this.setupTaxGroup}  className=""> Setup Tax Group </Button>   
          </Col>
        </Row>

       <Row gutter={[50, 50]}>
         <Col span={24}>
          <Form onSubmit={this.handleSubmit}>
          <Row gutter={[10,10]}> 
            <Col span={6}><Text strong> Product Name </Text></Col>
            <Col span={6}><Text strong> Product Category </Text></Col>
            <Col span={6}><Text strong> Tax Group </Text></Col>
            <Col span={6}><Text strong> Price </Text></Col>
          </Row>  
           {formItems}
        <Form.Item {...formItemLayoutWithOutLabel}>
          <Button type="dashed"  onClick={this.add} style={{ width: '60%' }}>
            <Icon type="plus" /> Add field
          </Button>
        </Form.Item>
        <Form.Item {...formItemLayoutWithOutLabel}>
        <Button type="default" onClick={ () => goBack() }>
            Go Back
          </Button>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
        </Col>
      </Row>
    </div>
    );
  }
}
export default Form.create()(Addproduct);