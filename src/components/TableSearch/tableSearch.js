import React from "react";


function tableSearch( searchtext, bookdata, columns) {

    let sortName = '';
    let sortValue = '';
    let name = {};
   
    let fil = [];

      const listItems = columns.map((col) => {

         name[col.dataIndex]  = 'string' ;
          fil.push(col.dataIndex)
      })
   
      let customerSearchLower = searchtext.toLowerCase()

      const filterFunc = (item : name ) => {

        
        let cond = [];

         fil.map((c) => {
          
             cond.push(item[c].toLowerCase().indexOf(customerSearchLower) !== -1);
         })

         console.log(cond)
         if(cond.includes(true)){
          return true; 
         }else{
           return false;
         }

         
      }

    const data = bookdata.filter((item: name) => filterFunc(item))

     //  console.log(data) 

     let displaybookdata = data.sort((a, b) =>
      sortValue === 'end'
        ? a[sortName] < b[sortName]
          ? -1
          : 1
        : b[sortName] < a[sortName]
        ? -1
        : 1
    ) 
    
    if(customerSearchLower.length == 0) {

          return bookdata
      }

   
      return data
    
  }

 export default  tableSearch ;
