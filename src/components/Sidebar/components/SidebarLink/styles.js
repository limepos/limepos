import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({
  link: {
    textDecoration: "none",
    "&:hover, &:focus": {
     // backgroundColor: theme.palette.background.light,
     backgroundColor: '#3a4350',
    },
  },
  linkActive: {
   // backgroundColor: theme.palette.background.light,
   backgroundColor : '#3a4350',
  },
  linkNested: {
    paddingLeft: 0,
    "&:hover, &:focus": {
      backgroundColor: "#3a4350",
    },
  },
  linkIcon: {
    marginRight: theme.spacing(1),
   // color: theme.palette.text.secondary + "99",
    transition: theme.transitions.create("color"),
    width: 24,
    display: "flex",
    justifyContent: "center",
    color:"#fff"
  },
  linkIconActive: {
   // color: theme.palette.primary.main,
   color:"#fff"
  },
  linkText: {
    padding: 0,
   // color: theme.palette.text.secondary + "CC",
    transition: theme.transitions.create(["opacity", "color"]),
    fontSize: 16,
    color: "#fff",
  },
  linkTextActive: {
    //color: theme.palette.text.primary,
    color: "#fff"
  },
  linkTextHidden: {
    opacity: 0,
  },
  nestedList: {
   // paddingLeft: theme.spacing(2) + 30,
     paddingLeft: 0,
     background: "#3b4452",
    // position: 'absolute',
    //  top: 0,
    //  width: 230,
    //  left:68,
    // height: '92vh !important',
  },
  sectionTitle: {
    marginLeft: theme.spacing(4.5),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
    height: 1,
    backgroundColor: "#D8D8D880",
  }
}));
