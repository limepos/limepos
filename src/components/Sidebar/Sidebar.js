import React, { useState, useEffect } from "react";
import { Drawer, IconButton } from "@material-ui/core";
import {
  ArrowBack as ArrowBackIcon
} from "@material-ui/icons";
import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import classNames from "classnames";

// styles
import useStyles from "./styles";

import SideMenu from 'react-sidemenu';
import './Sidebar.css'; 

// components



// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "../../context/LayoutContext";

import { useModeState } from "../../context/ModeContext";

  var kitchen_mode  = [
    {label: 'Dashboard', value: '/app/dashboard', icon: 'fa-tachometer' },
    {label: 'Sell', value: '/app/sell', icon: 'fa-shopping-cart'},
    {label: 'KDS', value: '/app/kds', icon: 'fa-shopping-cart'},
    {label: 'Receipts', value: '/app/receipts', icon: 'fa-list-alt'},
    {label: 'Expenses', value: '/app/expenses', icon: 'fa-cubes'},
    {label: 'Products', value: '/app/products', icon: 'fa-tags'},
    {label: 'Customers', value: '/app/customers', icon: 'fa-users'},
    {label: 'Setup', value: '/app/Setup', icon: 'fa-cogs',
    children: [
        { label: "Shop", value: "/app/setup/shop" },
        { label: "Registers", value: "/app/setup/registers" },
        { label: "Product Categories", value: "/app/setup/product-categories" },
        { label: "Product Options", value: "/admin/app/setup/product-options" },
        { label: "Taxes", value: "/app/setup/taxes" },
        { label: "Users", value: "/app/setup/users" },
        { label: "Discount Rules", value: "/app/setup/discount-rules" },
        // { label: "Additional Charges", value: "/app/additonalcharges" },
        // { label: "Custom Fields", value: "/app/setup/customields" },
        // { label: "Preferences", value: "/app/setup/preferences" }
      ]},
    
  ];


  // var kitchen_mode = [
  //    {label: 'Sell', value: '/app/sell', icon: 'fa-shopping-cart'},
  // ];

  var retaile_mode = [
    {label: 'Dashboard', value: '/app/dashboard', icon: 'fa-tachometer'},
    {label: 'Checkout', value: '/app/retail/checkout', icon: 'fa-shopping-cart'},
    {label: 'Orders', value: '/app/retail/orders', icon: 'fa-cubes'},
    {label: 'Products', value: '/app/retail/products', icon: 'fa-tags'},
    {label: 'Customers', value: '/app/retail/customers', icon: 'fa-address-book-o'},
    {label: 'Transactions', value: '/app/retail/transactions', icon: 'fa-credit-card-alt'},
    {label: 'Users', value: '/app/retail/users', icon: 'fa-users'},
    {label: 'Settings', value: '/app/Setup', icon: 'fa-cogs',
    children: [
        { label: "General", value: "/app/retail/settings/general" },
        { label: "Business", value: "/app/retail/settings/business" },
        { label: "Sales Tax", value: "/app/retail/settings/tax" },
        { label: "Export Reports", value: "/app/retail/settings/expot" }
        ]
     }
  ];

  var waiter_mode = [
      {label: 'Sell', value: '/app/sell', icon: 'fa-shopping-cart'}
  ];

  var cashier_mode = [
    {label: 'Sell', value: '/app/sell', icon: 'fa-shopping-cart'},
    {label: 'Receipts', value: '/app/receipts', icon: 'fa-list-alt'},
    {label: 'Expenses', value: '/app/expenses', icon: 'fa-cubes'},
  ];

function Sidebar({ location }) {
  var classes = useStyles();
  var theme = useTheme();
 
  var  isuseModeState  = useModeState();

  var items = [];

  console.log("-------->", isuseModeState.isMode);

        if(isuseModeState.isMode === "retaile_mode"){

            items = retaile_mode;

        }
        if(isuseModeState.isMode === "kitchen_mode"){

          items = kitchen_mode;

        }
        if(isuseModeState.isMode === "cashier_mode"){
          items = cashier_mode;
        }
        if(isuseModeState.isMode === "waiter_mode"){
          items = waiter_mode;
        }

  // global
  var { isSidebarOpened } = useLayoutState();
  var layoutDispatch = useLayoutDispatch();

  // local
  var [isPermanent, setPermanent] = useState(true);

  useEffect(function() {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup() {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <Drawer
      variant={isPermanent ? "permanent" : "temporary"}
      className={classNames(classes.drawer, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened,
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        }),
      }}
      open={isSidebarOpened}
      onClick={() => toggleSidebar(layoutDispatch)}
    >
      <div className={classes.toolbar} />
      <div className={classes.mobileBackButton}>
        <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
          <ArrowBackIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        </IconButton>
      </div>
      {/* <List className={classes.sidebarList}>
        {structure.map(link => (
          <SidebarLink
            key={link.id}
            location={location}
            isSidebarOpened={isSidebarOpened}
            {...link}
          />
        ))}
      </List> */}
      <SideMenu items={items}/>
    </Drawer>
  );

  // ##################################################################
  function handleWindowWidthChange() {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);
