import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Header";
import Sidebar from "../Sidebar";

// pages
import Dashboard from "../../pages/dashboard";
import Sell from "../../pages/sell";
import Customers from "../../pages/customers";
import ImportCustomer from "../../pages/customers/import";

import Expenses from "../../pages/expenses";
import Products from "../../pages/products";
import Addproduct from "../../pages/products/AddProduct";
import ImportProduct from "../../pages/products/import";
import EditProduct from "../../pages/products/EditProduct";
import addProduct from "../../pages/products/addSingleProduct";

import receipts from "../../pages/receipts";
import Viewreceipt from "../../pages/receipts/ViewReceipt";

// Setup Componets
import additionalCharges from "../../pages/additional-charges";
import customefields from "../../pages/setup/custom-fields";
import discountRules from "../../pages/setup/discountRules/discountRules";
import Addnewdiscout from "../../pages/setup/discountRules/AddDiscountRule";
import preferences from "../../pages/setup/preferences";

// Products Categories.
import productCategories from "../../pages/setup/productCategories/AllCategories";
import AddNewCategory from "../../pages/setup/productCategories/AddNewCategory";
import AddNewOrderTicketGroup from "../../pages/setup/productCategories/AddNewOrderTicketGroup";
import Shop from "../../pages/setup/shop";

// inner Register page
import allregisters from "../../pages/setup/registers/AllRegister/allregisters";
import Addregister from "../../pages/setup/registers/AddRegister";

import users from "../../pages/setup/users/allusers";
import Addcashier from "../../pages/setup/users/AddCashiers";
import Addappuser from "../../pages/setup/users/AddAppUser";
import Addwaiter from "../../pages/setup/users/AddWaiter";
import Addkitchenuser from "../../pages/setup/users/AddkitchenUser";

import taxes from "../../pages/setup/taxes/taxe";
import Addtaxegroup from "../../pages/setup/taxes/addtaxegroup";
import Addtax from "../../pages/setup/taxes/addtaxe";

import productOptions from "../../pages/setup/productOptions/AllproductOptins";
import AddNewVariant from "../../pages/setup/productOptions/Add_variant";
import AddNewaddon from "../../pages/setup/productOptions/Add_addons";
import AddonGroup from "../../pages/setup/productOptions/Addon_Groups";
import AddNewVariantGroup from "../../pages/setup/productOptions/Add_variant_Groups/AddNewVariantGroup";
import AddItemGroup from "../../pages/setup/productOptions/Add_item_group";
import Editvariant from "../../pages/setup/productOptions/Edit_variant";
import Editaddon from "../../pages/setup/productOptions/Edit_addon";
import Import from "../../pages/setup/productOptions/AllproductOptins/import";

// Retail mode 
import Icons from "../../pages/x_icons";
import checkout from "../../retailMode/checkout/checkout";
import retaileProducts from "../../retailMode/products/products";
import addRetailProduct from "../../retailMode/products/addSingleProduct";
import editRetailProduct from "../../retailMode/products/EditProduct";
import ImportRetailProduct from "../../retailMode/products/import";
import orderRetail from "../../retailMode/Order/Orders/orders";
import orderView from "../../retailMode/Order/OrderView/orderView";
import customerRetail from "../../retailMode/Customer/Customers/customers";
import customerView from "../../retailMode/Customer/CustomerView/customerView";
import addCustomer from "../../retailMode/Customer/addCustomer/addCustomer";
import transactions from "../../retailMode/transactions/transaction";
import viewTransactions from "../../retailMode/transactions/viewTransactions/viewTransactions";
import usersRetail from "../../retailMode/users/users";
import userView from "../../retailMode/users/userView/userView";
import retailAddUser from "../../retailMode/users/addUser/adduser";

import GeneralSettings from "../../retailMode/settings/general";
import BusinessSetting from "../../retailMode/settings/business";
import ExportSetttings from "../../retailMode/settings/exportReport";
import SalesTaxSettings from "../../retailMode/settings/salesAndTax";

import { useLayoutState } from "../../context/LayoutContext";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <Header history={props.history} />
      <Sidebar />
      <div
        className={classnames(classes.content, {
          [classes.contentShift]: layoutState.isSidebarOpened,
        })}
      >
        <div className={classes.fakeToolbar} />
        <Switch>
          <Route path="/app/dashboard" component={Dashboard} />
          <Route path="/app/sell" component={Sell} />
          <Route path="/app/customers" component={Customers} />
          <Route path="/app/customer/import" component={ImportCustomer} />
          <Route path="/app/expenses" component={Expenses} />
          <Route path="/app/products" component={Products} />
          <Route path="/app/add-products" component={Addproduct} />
          <Route path="/app/Product/import" component={ImportProduct} />
          <Route path="/app/Product/Edit" component={EditProduct} />
          <Route path="/app/product/new" component={addProduct} />
          <Route path="/app/receipts" component={receipts} />
          <Route path="/app/view/receipts" component={Viewreceipt} />
          <Route path="/app/additonalcharges" component={additionalCharges} />
          <Route path="/admin/app/setup/customields" component={customefields} />
          <Route path="/admin/app/setup/customers" component={Customers} />
          <Route path="/admin/app/setup/discount-rules" component={discountRules} />
          <Route path="/admin/app/setup/discount-rule/new-rule" component={Addnewdiscout}/>
          <Route path="/admin/app/setup/preferences" component={preferences} />
          <Route path="/admin/app/setup/product-categories"  component={productCategories} />
          <Route path="/admin/app/setup/product-categorie/new-category" component={AddNewCategory} />
          <Route path="/admin/app/setup/new-ticket-group" component={AddNewOrderTicketGroup} />
          <Route path="/admin/app/setup/shop" component={Shop} />
          <Route path="/admin/app/setup/registers" component={allregisters} />
          <Route path="/admin/app/setup/register/new-register" component={Addregister} />
          <Route path="/admin/app/setup/users" component={users} />
          <Route path="/admin/app/setup/user/new-cashier" component={Addcashier} />
          <Route path="/admin/app/setup/user/new-kitchenuser" component={Addkitchenuser} />
          <Route path="/admin/app/setup/user/new-waiter" component={Addwaiter} />
          <Route path="/admin/app/setup/user/new-appuser" component={Addappuser} />
          <Route path="/admin/app/setup/taxes" component={taxes} />
          <Route path="/admin/app/setup/tax/addtaxgroup" component={Addtaxegroup} />
          <Route path="/admin/app/setup/tax/addtax" component={Addtax} />
          <Route path="/admin/app/setup/product-options" component={productOptions} />
          <Route path="/admin/app/setup/product-option/new-variant" component={AddNewVariant} />
          <Route path="/admin/app/setup/product-option/new-addon"  component={AddNewaddon}  />
          <Route path="/admin/app/setup/product-option/new-addon-group" component={AddonGroup} />
          <Route path="/admin/app/setup/product-option/new-variant-group" component={AddNewVariantGroup} />
          <Route path="/admin/app/setup/product-option/new-item-group" component={AddItemGroup} />
          <Route path="/admin/app/setup/product-option/edit-variant"   component={Editvariant}  />
          <Route path="/admin/app/setup/product-option/edit-addon" component={Editaddon} />
          <Route path="/admin/app/setup/product-option/import" component={Import} />
            
          <Route path="/app/retail/checkout" component={checkout} />
          <Route path="/app/retail/products" component={retaileProducts} />
          <Route path="/app/retail/add-product" component={addRetailProduct} />
          <Route path="/app/retail/product/edit" component={editRetailProduct} />
          <Route path="/app/retail/orders" component={orderRetail} />
          <Route path="/app/retail/order/detail" component={orderView} />
          <Route path="/app/retail/customers" component={customerRetail} />
          <Route path="/app/retail/customer/detail" component={customerView} />
          <Route path="/app/retail/customer/new" component={addCustomer} />
          
          <Route path="/app/retail/transactions" component={transactions} />
          <Route path="/app/retail/transaction/detail" component={viewTransactions} />
          <Route path="/app/retail/users" component={usersRetail} />
          <Route path="/app/retail/user/detail" component={userView} />
          <Route path="/app/retail/user/new" component={retailAddUser} />

          <Route path="/app/retail/settings/general" component={GeneralSettings} />
          <Route path="/app/retail/settings/business" component={BusinessSetting} />
          <Route path="/app/retail/settings/expot" component={ExportSetttings} />
          <Route path="/app/retail/settings/tax" component={SalesTaxSettings} />


        </Switch>
      </div>
    </div>
  );
}

export default withRouter(Layout);
