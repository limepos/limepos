import React from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";

// components
import Layout from "./Layout";

// pages
import Error from "../pages/error";
import Login from "../pages/login/Login";
import Register from "../pages/registration";
import Mode from "../pages/mode";
import Landingpage from "../pages/landingpage";

// context
import { useUserState } from "../context/UserContext";  

export default function App() {
  // global
  var { isAuthenticated, role } = useUserState();

  return (
    <HashRouter>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/mode" />} />
        <Route
          exact
          path="/app"
          render={() => <Redirect to="/app/dashboard" />}
        />
        <AdminPrivateRoute path="/admin/app" component={Layout} />
        <CashierPrivateRoute path="/user/app" component={Layout} />
        <PrivateRoute path="/app" component={Layout} />
        <PrivateRoute path="/mode" component={Mode} />
        <PublicRoute path="/login" component={Login} />
        <PublicRoute path="/register" component={Register} />
        <PublicRoute path="/account" component={Landingpage} />
        <Route component={Error} />
      </Switch>
    </HashRouter>
  );

  // #######################################################################

  function PrivateRoute({ component, ...rest }) {
    console.log("role =>", role)
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated || role === "admin" || role === "cashier" || role === "waiter" ? (
            React.createElement(component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/account",
                state: {
                  from: props.location,
                },
              }}
            />
          )
        }
      />
    );
  }

  function AdminPrivateRoute({ component, ...rest }) {

    console.log("AdminPrivateRoute Role =>", role)

    return (
      <Route
        {...rest}
        render={ props =>
          role === "admin" ? (
            React.createElement(component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/account",
                state: {
                  from: props.location,
                },
              }}
            />
          )
        }
      />
    );
  }

  function CashierPrivateRoute({ component, ...rest }) {

    console.log("AdminPrivateRoute Role =>", role)

    return (
      <Route
        {...rest}
        render={ props =>
          role === "admin" || role === "cashier" ? (
            React.createElement(component, props)
          ) : (
            <Redirect
              to={{
                pathname: "/account",
                state: {
                  from: props.location,
                },
              }}
            />
          )
        }
      />
    );
  }
  function PublicRoute({ component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated ? (
            <Redirect
              to={{
                pathname: "/",
              }}
            />
          ) : (
            React.createElement(component, props)
          )
        }
      />
    );
  }
}
